<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@welcome');
Route::get('/gioi-thieu', 'HomeController@customer');
Route::get('/doi-tac', 'HomeController@merchant');
Route::get('/dieu-khoan-chung', 'HomeController@terms');
Route::get('/chinh-sach-bao-mat', 'HomeController@privacy');
Route::get('/cam-ket-voi-khach-hang', 'HomeController@policy');
Route::get('/ve-chung-toi', 'HomeController@about');
Route::get('/ho-tro-doi-tac', 'HomeController@merchantSupport');
Route::get('/ho-tro-khach-hang', 'HomeController@customerService');


Route::post('payments', 'PaymentController@store');
Route::get('payment/email/{token}', 'PaymentController@oneClick')->name('payment.oneClick');
Route::get('payment/callback/alepay', 'PaymentController@callbackAlePay');
Route::get('payment/callback/nganluong', 'PaymentController@callbackNganLuong');
Route::get('payment/callback/cash', 'PaymentController@callbackCash');

Auth::routes(['verify' => true]);
Route::group(['middleware' => 'auth'], function () {
    Route::get('dashboards', 'DashboardController@index')->name('dashboards');
    Route::resource('orders', 'OrderController');
    Route::resource('invoices', 'InvoiceController');
    Route::get('users', 'UserController@index');
    Route::get('supports', 'SupportController@index');
});


Route::group(['middleware' => 'throttle:5,1'], function () {
    Route::post('/contacts', 'HomeController@contact')
        ->name('contacts');
    Route::post('/partner-contacts', 'HomeController@partnerContact')
        ->name('partnerContact');

    Route::post('/subscriptions', 'HomeController@subscription')
        ->name('subscription');

});


use Vsch\TranslationManager\Translator;

Route::group(['middleware' => 'auth', 'prefix' => 'translations'], function () {
    Translator::routes();
});

Route::get('locale/{locale}', 'HomeController@locale');

Route::get('/redirect', 'SocialAuthFacebookController@redirect');
Route::get('/callback', 'SocialAuthFacebookController@callback');
