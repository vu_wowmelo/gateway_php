<?php

namespace App\Rules;

use App\Order;
use Illuminate\Contracts\Validation\Rule;

class OrderCode implements Rule
{
    protected $clientId;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($clientId)
    {
        $this->clientId =  $clientId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(empty($this->clientId)){
            return false;
        }

        $order = Order::where('order_code', $value)
                    ->where('client_id' , $this->clientId);
        if($order->exists()){
                return false;

        }
        return true;

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Order code is duplicated.';
    }
}
