<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use PhpParser\Node\Expr\Array_;

class PaymentMethod implements Rule
{
    protected $paymentMethod;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(empty($this->paymentMethod)){
            return false;
        }

        if($this->paymentMethod == 'domestic' && empty($value)){

            return false;

        }

        return true;

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Vui lòng chọn 1 ngân hàng.';
    }
}
