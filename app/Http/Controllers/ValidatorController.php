<?php

namespace App\Http\Controllers;

use App\Interaction;
use App\Services\Score\ScoreBaseService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Score\AccountService;
use App\Score\HistoricalService;

class ValidatorController extends Controller
{

    protected $historicalService, $accountService, $scoreBaseService;

    public function __construct(ScoreBaseService $scoreBaseService, Request $request)
    {
        parent::__construct($request);
        $this->scoreBaseService = $scoreBaseService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->validate([
            'shopper_id' => 'required',
            'registered_months' => 'required'
        ],
            [
                'shopper_id.required' => 'Please provide us shopper id. ',
                'registered_months.required' => 'Please provide us registered_at. ',
            ]);

        //

        $interaction = Interaction::firstOrCreate([
            'shopper_id', $request->get('shopper_id'),
            'client_id', $request->client
        ], [

        ]);

        return $request->client;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
                'shopper_id' => 'required',
                'registration_date' => 'required',

            ],
            [
                'shopper_id.required' => 'Please provide us shopper id. ',
                'registration_date.required' => 'Please provide us registered_months. ',
            ]);

            return $this->scoreBaseService->index($request);


    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
