<?php

namespace App\Http\Controllers;

use App\Client;
use App\DHLProvince;
use App\Domestic;
use App\Notifications\NewAccount;
use App\Order;
use App\Rules\NID;
use App\Rules\OrderCode;
use App\Services\OrderService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use Illuminate\Validation\Rule;

class OrderController extends Controller
{

    protected $orderService;
    protected $cityMapping = [
        'TP Hồ Chí Minh' => 'Hồ Chí Minh',
        'Hồ Chí Minh' => 'Hồ Chí Minh'
    ];

    public function __construct(OrderService $orderService, Request $request)
    {
        parent::__construct($request);
        $this->orderService = $orderService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $orders = Order::whereIn('status', [1])
            ->where('user_id',Auth::user()->id)
            ->with('client')
            ->get();

        $selectedProvince = null;


        $order = array_get($orders,0,null);

        $provinces = DHLProvince::distinct('province')->select('province')->get();
        $provinceAll = DHLProvince::all();

        $dhlProvinceAll = [];
        foreach ($provinceAll as $province){
            $dhlProvinceAll[$province->province][] = $province->district;
        }
        $selectedDistrict =  null;
        if(!empty($order->billing_city)){
            if(!empty($this->cityMapping[$order->billing_city])){
                $selectedProvince  = $this->cityMapping[$order->billing_city];
            }
            $selectedDistrict  = $order->billing_district;

        }


        if(!empty($selectedProvince)){
            $districts = DHLProvince::distinct('district')->where(
                'province',$selectedProvince
            )->select('district')->get();
        }else{
            $districts = DHLProvince::distinct('district')->select('district')->get();
        }




        $domestics = Domestic::all();

        return view('order.index',[
            'pageTitle' => 'Đơn hàng',
            'orders' => $orders,
            'order' => $order,
            'provinces' => $provinces,
            'districts' => $districts,
            'selectedProvince' =>$selectedProvince,
            'selectedDistrict' =>$selectedDistrict,
            'dhlProvinceAll' =>$dhlProvinceAll,
            'domestics' =>$domestics,
            'pageDescription' =>'Đơn hàng chờ thanh toán'
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'order_code' => ['required',new OrderCode($request->client->id)],
            'shopper_id' => ['required',
                Rule::exists('interactions')->where(function ($query) use ($request) {
                    $query->where('client_id', $request->client->id);
                }),
            ],
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'nid' => ['required',new NID($request->get('email',''))
            ],
            'total' => 'required',
            'billing_address.address' => 'required',
            'billing_address.address_note' => 'required',
            'billing_address.contact_name' => 'required',
            'billing_address.contact_phone' => 'required',
            'billing_address.ward' => 'required',
            'billing_address.district' => 'required',
            'billing_address.city' => 'required',
            'items.*.id' =>'required',
            'items.*.sku' =>'required',
            'items.*.product' =>'required',
            'items.*.price' =>'required',
            'items.*.subtotal' =>'required',
        ]);



        return $this->orderService->store($request);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        $user = User::find(7);
        $client = Client::where('id',7)->first();
        $token = Password::getRepository()->create($user);
        $user->notify(new NewAccount($token, $user, $client));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
