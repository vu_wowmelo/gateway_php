<?php

namespace App\Http\Controllers;

use App\Containers\NganLuongPayment\Lib\AlePay\Alepay;
use App\Containers\NganLuongPayment\Lib\NganLuong\NL_Checkoutv3;
use App\DHLProvince;
use App\Domestic;
use App\Invoice;
use App\InvoiceOrderRelation;
use App\InvoiceRelation;
use App\Order;
use App\OrderItem;
use App\Rules\PaymentMethod;
use App\Services\PaymentService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use Webpatser\Uuid\Uuid;

class PaymentController extends Controller
{

    protected $paymentService;

    protected $cityMapping = [
        'TP Hồ Chí Minh' => 'Hồ Chí Minh',
        'Hồ Chí Minh' => 'Hồ Chí Minh'

    ];

    public function __construct(PaymentService  $paymentService)
    {
        $this->paymentService = $paymentService;
    }


    public function oneClick($token)
    {
        $order = Order::where('status', 1)
                ->where('payment_token', $token);



        if (!$order->exists()) {
            abort(404);
        }
        $order = $order->first();
        $order->total= 0;
        foreach ($order->items as $item){
            $order->total +=$item->subtotal;
        }

        $domestics = Domestic::all();
        $selectedProvince = null;


        $provinces = DHLProvince::distinct('province')->select('province')->get();
        $provinceAll = DHLProvince::all();

        $dhlProvinceAll = [];
        foreach ($provinceAll as $province){
            $dhlProvinceAll[$province->province][] = $province->district;
        }

        if(!empty($this->cityMapping[$order->billing_city])){
            $selectedProvince  = $this->cityMapping[$order->billing_city];
        }

        if(!empty($selectedProvince)){
            $districts = DHLProvince::distinct('district')->where(
                'province',$selectedProvince
            )->select('district')->get();
        }else{
            $districts = DHLProvince::distinct('district')->select('district')->get();
        }

        $selectedDistrict  = $order->billing_district;

        return view('payment.index', [
            'pageTitle' => 'Thông tin chung',
            'token' => $token,
            'order' => $order,
            'provinces' => $provinces,
            'districts' => $districts,
            'selectedProvince' =>$selectedProvince,
            'selectedDistrict' =>$selectedDistrict,
            'dhlProvinceAll' =>$dhlProvinceAll,
            'domestics' =>$domestics,
            'pageDescription' => '',
        ]);


    }


    public function store(Request $request){

        $request->validate([
            'payment_token'=> ['required'],
            'payment_method' => [
                'required',
                Rule::in(['domestic', 'international_card','cash'])
            ],
            'bank_code' =>[
                'exists:domestics,code',
                new PaymentMethod($request->get('payment_method',''))
            ]
            ]);


        if($request->payment_method == 'cash'){

            $this->getValidationFactory()->make(
                $request->all(),
                [
                    'billing_address'=> ['required'],
                    'billing_city'=> ['required'],
                    'billing_district'=> ['required'],
                ],
                [
                    'billing_address.required'=> 'Vui lòng nhập địa chỉ để Wowmelo liên hệ thu tiền ',
                    'billing_city.required'=> 'Vui lòng chọn tỉnh/thành phố ',
                    'billing_district.required'=> 'Vui lòng chọn quận/huyện ',
                ]
            )->validate();
        }

        $user = new User();
        $order = new Order();
        $items  =[];
        DB::beginTransaction();
        try{
            // create invoice data
            $paymentToken = Uuid::generate()->string;
            $invoice = new Invoice();

            $invoice->payment_token = $paymentToken;
            $invoice->status = 1;
            $invoice->billing_address = $request->get('billing_address','');
            $invoice->billing_city = $request->get('billing_city','');
            $invoice->billing_district = $request->get('billing_district','');
            $description = 'Trả tiền cho đơn hàng ';

            foreach ($request->get('payment_token',[]) as $key => $token) {
                $order = Order::with('user')->where('payment_token',$token)->first();
                $invoice->user_id  = $order->user_id;
                $invoice->billing_contact_name  = $order->billing_contact_name;
                $invoice->billing_contact_phone  = $order->billing_contact_phone;
                $invoice->billing_contact_email  = $order->user->email;
                $invoice->billing_contact_email  = $order->user->email;
                $user = $order->user;
            }

            $invoice->total = $order->payment_amount;
            $invoice->save();
            $i =0;
            foreach ($request->get('payment_token',[]) as $key => $token) {
                $orderItems = OrderItem::where('payment_token',$token)->get();
                foreach ($orderItems as $item) {
                    $i++;
                    $items[] =[
                        "item_name{$i}" => $item->product,
                        "item_quantity{$i}" => $item->qty,
                        "item_amount{$i}" => $item->price,

                    ];
                    $invoiceRelation = new InvoiceRelation();
                    $invoiceRelation->invoice()->associate($invoice);
                    $invoiceRelation->orderItem()->associate($item);
                    $invoiceRelation->save();
                }

            }

            foreach ($request->get('payment_token',[]) as $key => $token) {
                $i++;
                $orders = Order::where('payment_token',$token)->get();
                foreach ($orders as $or){
                    $order = $or;
                    $description .="#{$or->order_code_text} ";
                    $invoiceOrderRelation = new InvoiceOrderRelation();
                    $invoiceOrderRelation->invoice()->associate($invoice);
                    $invoiceOrderRelation->order()->associate($order);
                    $invoiceOrderRelation->save();
                }
            }
            $invoice->description =$description;
            $invoice->save();

            if($request->get('payment_method') =='cash') {

                // update invoice
                $invoice->payment_method = $request->get('payment_method');
                $invoice->save();
                // call back
                $this->paymentService->pending($invoice);

            }

        }catch (\Exception $e){

            DB::rollback();
            return $e;
        }
        DB::commit();

        if($request->get('payment_method') =='cash') {

            return response()->json([
                'payment_method' =>$request->get('payment_method'),
                'checkoutUrl' => url('/payment/callback/cash')

            ]);


        }



        if($request->get('payment_method') =='domestic'){

            $nganLuong = new NL_Checkoutv3(
                env('NGANLUONG_MERCHANT_ID'),
                env('NGANLUONG_MERCHANT_PASS'),
                env('NGANLUONG_RECEIVER'),
                env('NGANLUONG_ENDPOINT')
            );
            //$order_code, $total_amount, $bank_code, $payment_type, $order_description, $tax_amount,
            //                          $fee_shipping, $discount_amount, $return_url, $cancel_url, $buyer_fullname,
            // $buyer_email, $buyer_mobile,
            //                          $buyer_address, $array_items
            $nlResult = $nganLuong->BankCheckout(
                $invoice->id,
                $invoice->total,
                $request->get('bank_code'),
                1,
                null,
                null,
                null,
                null,
                env('NGANLUONG_CALLBACK_URL'),
                url('/'),
                $user->name,
                $user->email,
                $user->phone,
                $order->billing_address,
                $items
            );
            $nlResult->payment_method = $request->get('payment_method');
            $invoice->gateway_token = $nlResult->token;
            $invoice->payment_method = $request->get('payment_method');
            $invoice->checkout_url = $nlResult->checkout_url;
            $invoice->save();
            return response()->json($nlResult);

        }

        if($request->get('payment_method') =='international_card'){
            $opts = [
                'apiKey' => env('ALEPAY_KEY'),
                'encryptKey' => env('ALEPAY_ENCRYPT_KEY'),
                'checksumKey' => env('ALEPAY_CHECKSUM_KEY'),
                'callbackUrl' => env('ALEPAY_CALLBACK_URL'),
                'env' => env('ALEPAY_ENV'),

            ];
            $alepay = new Alepay($opts);
            $data = [
              'orderCode' => $invoice->id,
                'amount' => $invoice->total,
                'currency' =>'VND',
                'totalItem' =>count($items),
                'orderDescription' =>'Thanh Toán hoá đơn số #'.$invoice->id,
                'checkoutType' => 1,
                'returnUrl' => url('/payment/checkout/'),
                'cancelUrl' => url('/payment/cancel/'),
                'buyerName' => $user->name,
                'buyerEmail' => $user->email,
                'buyerPhone' => $user->phone,
                'buyerAddress' => $order->billing_address,
                'buyerCity'  => $order->billing_city,
                'buyerCountry'  => 'Vietnam',
            ];
          //  return $data;
            $nlResult = $alepay->sendOrderToAlepay($data);
            Log::info($data);
            Log::info(json_encode($nlResult));
            $nlResult->payment_method = $request->get('payment_method');
            $invoice->gateway_token = $nlResult->token;
            $invoice->payment_method = $request->get('payment_method');
            $invoice->checkout_url = $nlResult->checkoutUrl;
            $invoice->save();

            return response()->json($nlResult);

        }


        return [];

    }

    public function callbackAlePay(Request $request){

        // get transaction info
        $opts = [
            'apiKey' => env('ALEPAY_KEY'),
            'encryptKey' => env('ALEPAY_ENCRYPT_KEY'),
            'checksumKey' => env('ALEPAY_CHECKSUM_KEY'),
            'callbackUrl' => env('ALEPAY_CALLBACK_URL'),
            'env' => env('ALEPAY_ENV'),

        ];
        $alepay = new Alepay($opts);
        $data = $alepay->decryptCallbackData($request->get('data'));
        $data = json_decode($data,true);
        if($data['errorCode'] !=='000'){

            return $data;
        }
        $result = $alepay->getTransactionInfo($data['data']);
        $result= json_decode($result,true);
        try{
            // get transaction info
            $opts = [
                'apiKey' => env('ALEPAY_KEY'),
                'encryptKey' => env('ALEPAY_ENCRYPT_KEY'),
                'checksumKey' => env('ALEPAY_CHECKSUM_KEY'),
                'callbackUrl' => env('ALEPAY_CALLBACK_URL'),
                'env' => env('ALEPAY_ENV'),
            ];
            $alepay = new Alepay($opts);
            $data = $alepay->decryptCallbackData($request->get('data'));
            $data = json_decode($data,true);
            if($data['errorCode'] !=='000'){

                return $data;
            }
            $result = $alepay->getTransactionInfo($data['data']);
            $result= json_decode($result,true);

            $invoice = Invoice::where('gateway_token',$result['transactionCode'])
                ->where('status',1)
                ->with('relations')
                ->with('relations.orderItem');
            Log::info($result);
            if($invoice->exists()){
                $invoice= $invoice->first();
                $this->paymentService->complete($invoice);
            }
        }catch (\Exception $e){
            return $e;
        }
        return view('payment.callback', [
            'pageTitle' => 'Thông tin chung',
            'pageDescription' => '',
        ]);
    }
    public function callbackNganLuong(Request $request){

        $nganLuong = new NL_Checkoutv3(
            env('NGANLUONG_MERCHANT_ID'),
            env('NGANLUONG_MERCHANT_PASS'),
            env('NGANLUONG_RECEIVER'),
            env('NGANLUONG_ENDPOINT')
        );
        $data = $nganLuong->GetTransactionDetail($request->get('token'));

        if($data->transaction_status = '00'){
            $invoice = Invoice::where('gateway_token',$request->get('token'))
                ->where('status',1)
                ->with('relations')
                ->with('relations.orderItem');
            if($invoice->exists()){
                $invoice= $invoice->first();

                $this->paymentService->complete($invoice);
            }
        }
        return view('payment.callback', [
            'pageTitle' => 'Thông tin chung',
            'pageDescription' => '',
        ]);

    }


    public function callbackCash(Request $request){

        return view('payment.callback', [
            'pageTitle' => 'Thông tin chung',
            'pageDescription' => '',
        ]);

    }

}
