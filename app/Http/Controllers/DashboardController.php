<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $orders = Order::whereHas('items', function ($query) {
            $query->where('status', 1);
        })
            ->where('user_id',Auth::user()->id)
            ->with('client')
            ->get();

        $orderHasPaid = Order::whereHas('items', function ($query) {
            $query->where('status', 2);
        })
            ->where('user_id',Auth::user()->id)
            ->with('client')
            ->orderBy('updated_at','desc')
            ->get();

        $date =date('Y-m-d');
        $totalPending = 0;
        $totalLate = 0;
        if(!empty($orders)){
            foreach ($orders as &$order){
                $totalPending += $order->total;
                $order->late = false;
                if($date >= $order->due_date){
                    $order->late = true;
                    $totalLate += $order->total;
                }
            }
        }

        return view('dashboard',[
            'pageTitle' => 'Thông tin chung',
            'orders' => $orders,
            'orderHasPaid' =>$orderHasPaid,
            'totalPending' => $totalPending,
            'totalLate' => $totalLate,
            'pageDescription' =>'',
            ]);
    }
}
