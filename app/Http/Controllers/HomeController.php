<?php

namespace App\Http\Controllers;

use App\Mail\ConfirmContactMail;
use App\Mail\ContactMail;
use App\Mail\PartnerConfirmContactMail;
use App\Mail\PartnerContactMail;
use App\Subscription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{

    public function index()
    {
        return view('home');
    }

    public function contact(Request $request){

        $request->validate([
            'email' => ['required','email'],
            'name' => 'required',
            'phone' => 'required',
            'message' => 'required',
        ]);

        $mail = Mail::to(env('MAIL_USERNAME'))->queue(new ContactMail($request->only('email','name','phone','message')));
        $mail = Mail::to($request->get('email'))->queue(new ConfirmContactMail($request->only('email','name','phone','message')));





        return [
            'title' => __('contact.title'),
            'text' => __('contact.text')
        ];

    }


    public function partnerContact(Request $request)
    {

        $request->validate([
            'email' => ['required', 'email'],
            'firstname' => 'required',
            'lastname' => 'required',
            'phone' => 'required',
            'website' => 'required',
            'annual_sales' => 'required',
            'average_order' => 'required',
        ]);

        $mail = Mail::to('partners@wowmelo.com')->queue(new PartnerContactMail($request->only('firstname', 'lastname', 'email', 'phone', 'website',  'average_order', 'annual_sales')));

        $params = $request->only('firstname', 'lastname', 'email', 'phone', 'website',  'average_order', 'annual_sales');
        $params['subject'] = __('contact.subject');
        $params['content'] = __('contact.content');
        $params['hello'] = __('contact.hello');
        $params['regards'] = __('contact.regards');

        $mail = Mail::to($request->get('email'))->queue(new PartnerConfirmContactMail($params));

        return [
            'title' => __('contact.title'),
            'text' => __('contact.text')
        ];

    }

    public function subscription(Request $request)
    {

        $request->validate([
            'email' => ['required', 'email'],
        ]);


        Subscription::firstOrCreate(['email' => $request->get('email')]);

        return ['thông tin đã đăng ký thành công'];
    }

    public function welcome(){
        return view('welcome');

    }

    public function customer(){
        return view('customer');
    }

    public function merchant(){
        return view('merchant');
    }

    public function terms(){
        return view('terms');
    }

    public function privacy(){
        return view('privacy');
    }

    public function policy(){
        return view('policy');
    }

    public function about(){
        return view('about');
    }

    public function merchantSupport(){
        return view('merchant_support');
    }

    public function customerService(){
        return view('customer_service');
    }

    public function locale($locale){
        Session::put('locale', $locale);

        return redirect()->back();

    }









}
