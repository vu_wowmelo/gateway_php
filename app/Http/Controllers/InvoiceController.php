<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invoice;
use Illuminate\Support\Facades\Auth;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoices = Invoice::where('user_id', Auth::user()->id)
            ->where(function ($query) {
                $query->whereRaw("(invoices.status = 2 and invoices.payment_method in ('domestic','bank_transfer','international_card')) or (payment_method = 'cash' and invoices.status in(-1,1,2,3,-2)) ");
            })
            ->orderBy('created_at','desc')
            ->paginate(10);


        return view('invoice.index',[
            'pageTitle' => 'Hoá đơn',
            'invoices' => $invoices,
            'pageDescription' =>'Các hoá đơn'
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $from =[
            'name' =>'Wow Melo',
            'address_line1' =>'795 Folsom Ave, Suite 600',
            'address_line2' =>'San Francisco, CA 94107',
            'phone' =>'(804) 123-5432',
            'email' =>'info@almasaeedstudio.com',
        ];
        $to =[
            'name' =>'Wow Melo',
            'address_line1' =>'795 Folsom Ave, Suite 600',
            'address_line2' =>'San Francisco, CA 94107',
            'phone' =>'(804) 123-5432',
            'email' =>'info@almasaeedstudio.com',
        ];

        $invoice =[
            'id' => $id,
            'order_id' => '8765',
            'invoice_date' => '2018-11-12',
            'payment_due_date' => '2018-11-12',
            'from' => $from,
            'to' => $to,
            'sub_total' =>'3000000',
            'total' =>'3000000',
            'items' =>[
                [
                    'id' =>1,
                    'qty' =>1,
                    'product' => 'Tàu hộ vệ tên lửa 15 nòng',
                    'price' => '3000000',
                    'total' => '3000000'
                ]
            ]

        ];


        //
        return view('invoice.edit',['invoice'=>$invoice, 'pageTitle' => 'Hoá đơn','pageDescription' =>'Chi tiết hoá đơn']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
