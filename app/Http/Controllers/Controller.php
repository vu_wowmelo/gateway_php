<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\Token;
use Lcobucci\JWT\Parser;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct(Request $request)
    {
        $request->client =[];
        $bearerToken = ($request->bearerToken());
        if(!empty($bearerToken)){
            $tokenId= (new Parser())->parse($bearerToken)->getHeader('jti');
            $client = Token::find($tokenId)->client;
            $request->client = $client;
        }

    }
}
