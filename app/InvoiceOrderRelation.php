<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceOrderRelation extends Model
{
    protected $guarded = [];

    public function invoice()
    {
        return $this->belongsTo('App\Invoice');
    }

    public function order()
    {
        return $this->belongsTo('App\Order');
    }



}
