<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PartnerConfirmContactMail extends Mailable
{
    use Queueable, SerializesModels;


    private $params;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        $this->params = $params;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $params =$this->params;
        return $this->markdown('emails.contacts.partner.confirm')
            ->subject($params['subject'])
            ->with(['params' => $this->params]);
    }
}
