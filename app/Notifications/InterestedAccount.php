<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Lang;

class InterestedAccount extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * The password reset token.
     *
     * @var string
     */
    public $token, $user, $client ;

    /**
     * The callback that should be used to build the mail message.
     *
     * @var \Closure|null
     */
    public static $toMailCallback;

    /**
     * Create a notification instance.
     *
     * @param  string  $token
     * @return void
     */

    /**
     * Create a notification instance.
     *
     * @param  string  $token
     * @return void
     */
    public function __construct($token, $user)
    {
        $this->token = $token;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if (static::$toMailCallback) {
            return call_user_func(static::$toMailCallback, $notifiable, $this->token);
        }

        return (new MailMessage)
            ->greeting('Xin chào '. $notifiable->name.'!')
            ->subject(Lang::getFromJson('Welcome to Wowmelo'))
            ->line(Lang::getFromJson("
            Cảm ơn bạn đã đăng ký Wowmelo"))
            ->line(Lang::getFromJson("
            Tài khoản Wowmelo của bạn đã được tạo với tài khoản email này. Bạn có thể đăng nhập vào tài khoản này để sử dụng dịch vụ pay later từ wowmelo.
            "))

            ->line(Lang::getFromJson("
            Vui lòng nhấp vào nút bên dưới để đăng nhập vào wowmelo:
            "))
            ->action(
                Lang::getFromJson('Đăng nhập tài khoản'),
                url(config('app.url').'/login'));

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
