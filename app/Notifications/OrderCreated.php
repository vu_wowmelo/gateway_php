<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Lang;

class OrderCreated extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * The password reset token.
     *
     * @var string
     */
    public $user, $client, $order;

    /**
     * The callback that should be used to build the mail message.
     *
     * @var \Closure|null
     */
    public static $toMailCallback;

    /**
     * Create a notification instance.
     *
     * @param  string  $token
     * @return void
     */

    /**
     * Create a notification instance.
     *
     * @param  string  $token
     * @return void
     */
    public function __construct($user, $client, $order)
    {
        $this->user = $user;
        $this->client = $client;
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if (static::$toMailCallback) {
            return call_user_func(static::$toMailCallback, $notifiable, $this->token);
        }

        $introLines = [
            "Đơn hàng #{$this->order->order_code} đặt tại {$this->client->name} đã xác nhận sử dụng dịch vụ Pay Later của WowMelo.",
            "Đơn hàng của bạn sẽ được giao trong thời gian sớm nhất, còn bạn chỉ cần thảnh thơi và thanh toán sau 14 ngày sau.",
            "Để hoàn tất việc thanh toán, bạn chỉ cần nhấp vào link bên dưới hoặc bất cứ email nào liên quan đến đơn hàng này mà chúng tôi sẽ gửi khi hạn thanh toán đến gần. Chi tiết hạn thanh toán được đính kèm cuối thư này."
        ];

        $url = url(config('app.url').route('payment.oneClick',
                $this->order->payment_token,
                false));

        return (new MailMessage)
            ->markdown('emails.orders.created',['order' => $this->order,'introLines' =>$introLines,'url' => $url])
            ->greeting('Xin chào '. $notifiable->name.'!')
            ->subject(Lang::getFromJson('Xác nhận sử dụng WowMelo -  Cảm ơn bạn đã mua sắm với dịch vụ Pay Later'))

            ;

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
