<?php
/**
 * Created by PhpStorm.
 * User: vunguyen
 * Date: 11/23/18
 * Time: 5:48 PM
 */

namespace App\Services;


class Service
{

    public function __construct()
    {

    }


    public function compareValue($row, $value){


        if($row->data_type == 'T/F'){

            if($value ==  $row->max_value){
                return $row->point;
            }
            return 0;
        }

        if($row->data_type == 'Min/Max'){
            if(empty($row->max_value)){
                if(($value) >=  ($row->min_value) ){
                    return $row->point;
                }

            }

            if(empty($row->min_value)){
                if(($value) <=  ($row->max_value )){
                    return $row->point;
                }

            }
            if(($value) >=  ($row->min_value) && ($value) <=  ($row->max_value )){
                return $row->point;
            }
            return 0;
        }

    }


}