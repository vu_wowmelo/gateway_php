<?php
/**
 * Created by PhpStorm.
 * User: vunguyen
 * Date: 11/23/18
 * Time: 5:48 PM
 */

namespace App\Services;



use App\Interaction;
use App\Notifications\NewAccount;
use App\Notifications\OrderCreated;
use App\Order;
use App\OrderItem;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Password;
use Webpatser\Uuid\Uuid;

class OrderService
{

    protected $defaultRole = 4;

    public function __construct()
    {
    }


    /**
     * @param Request $request
     */
    public function store(Request $request){

        $user = User::where('email',$request->email);
        $billingAddress = $request->get('billing_address');
        $firsOrder = false;
        if(!$user->exists()){
            // create account
            $user = User::create([
                'email' =>$request->email,
                'nid' =>$request->nid,
                'phone' =>$request->phone,
                'name' =>$request->first_name.' '.$request->last_name,
                'password' => '-1',
                'address' => array_get($billingAddress,'address'),
                'address_note' => array_get($billingAddress,'address_note'),
                'ward' => array_get($billingAddress,'ward'),
                'district' => array_get($billingAddress,'district'),
                'city' => array_get($billingAddress,'city'),

            ]);

            $user->roles()->attach($this->defaultRole);


            // sending email welcome
            $token = Password::getRepository()->create($user);
            $user->notify(new NewAccount($token, $user, $request->client));
            $firsOrder = true;
        }else{
            $user =  $user->first();
            Interaction::where('client_id',$request->client->id)
                ->where('shopper_id',$request->shopper_id)
                ->update(['user_id' => $user->id]);

            $user->update(['balance' => $user->balance*1-$request->total*1 ]);

        }


        $items = $request->get('items');
        $paymentToken = Uuid::generate()->string;
        $order =  Order::create([
            'total' => $request->total,
            'payment_amount' =>$request->total,
            'payment_token' => $paymentToken,
            'order_code' => $request->order_code,
            'client_id' => $request->client->id,
            'tax'  => $request->get('tax',0),
            'discount'  => $request->get('discount',0),
            'fee'  => $request->get('fee',0),
            'billing_address' => array_get($billingAddress,'address'),
            'billing_address_note' => array_get($billingAddress,'address_note'),
            'billing_contact_name' => array_get($billingAddress,'contact_name'),
            'billing_contact_phone' => array_get($billingAddress,'contact_phone'),
            'billing_ward' => array_get($billingAddress,'ward'),
            'billing_district' => array_get($billingAddress,'district'),
            'billing_city' => array_get($billingAddress,'city'),
            'due_date' => date('Y-m-d',strtotime('+14days'))
        ]);
        $order->user()->associate($user);
        $order->save();
        foreach ($items as $item){
            $orderItem = new OrderItem([
                'sku' => $item['sku'],
                'payment_token' =>$paymentToken,
                'product' => $item['product'],
                'price' =>$item['price'],
                'subtotal' => $item['subtotal'],
                'qty' => $item['qty'],
            ]);
            $orderItem->order()->associate($order);
            $orderItem->save();
        }
        $user->notify(new OrderCreated($user, $request->client, $order->load('items')));


        return $order;


    }



}