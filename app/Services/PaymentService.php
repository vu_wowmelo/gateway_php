<?php
/**
 * Created by PhpStorm.
 * User: vunguyen
 * Date: 11/23/18
 * Time: 5:48 PM
 */

namespace App\Services;



use App\Interaction;
use App\Invoice;
use App\Notifications\NewAccount;
use App\Notifications\OrderCreated;
use App\Order;
use App\OrderItem;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Password;

class PaymentService
{


    public function __construct()
    {
    }


    public function complete(Invoice $invoice){
        DB::beginTransaction();

        try{

            $invoice->update(['status' =>2]);
            foreach ($invoice->relations as $relation){
                OrderItem::where('id',$relation->order_item_id)
                    ->update(['status' => 2,'payment_token' => $invoice->payment_token]);
               // $relation->orderItem->update(['status' => 2,'payment_token' => $invoice->payment_token]);
            }

            foreach ($invoice->orderRelations as $relation){
                Order::where('id',$relation->id)
                    ->update(['status' => 2,'payment_token' => $invoice->payment_token]);
                // $relation->orderItem->update(['status' => 2,'payment_token' => $invoice->payment_token]);
            }



        }catch (\Exception $e){
            Log::info($e);
            DB::rollback();


        }

        DB::commit();


    }


    public function pending(Invoice $invoice){
        DB::beginTransaction();

        try{

            $invoice->update(['status' =>-1]);
            foreach ($invoice->relations as $relation){
                OrderItem::where('id',$relation->order_item_id)
                    ->update(['status' => -1,'payment_token' => $invoice->payment_token]);
                // $relation->orderItem->update(['status' => 2,'payment_token' => $invoice->payment_token]);
            }

            foreach ($invoice->orderRelations as $relation){
                Order::where('id',$relation->id)
                    ->update(['status' => -1,'payment_token' => $invoice->payment_token]);
                // $relation->orderItem->update(['status' => 2,'payment_token' => $invoice->payment_token]);
            }



        }catch (\Exception $e){
            Log::info($e);
            DB::rollback();


        }

        DB::commit();


    }



}