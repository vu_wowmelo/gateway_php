<?php
/**
 * Created by PhpStorm.
 * User: vunguyen
 * Date: 11/23/18
 * Time: 5:48 PM
 */

namespace App\Services\Score;


use App\Services\Service;

class AccountService extends Service
{


    public function __construct()
    {

    }

    public function getScore($factors, $data){
        $ageFactors =  [];
        $informationFactors =  [];

        if(!empty($factors)){
            foreach ($factors as $factor ){

                switch ($factor->sub_factor) {
                    case "Age":
                        $ageFactors[] = $factor;
                        break;
                    case "Information":
                        $informationFactors[] = $factor;
                        break;
                    default:
                }

            }
        }

        $ageScore = $this->Age($ageFactors, $data);
        $informationScore = $this->Information($informationFactors, $data);
        return [
            'Age' => $ageScore,
            'informationScore' => $informationScore
        ];
    }


    private function Age($ageFactors, $data){
        foreach ($ageFactors as &$ageFactor){
            $ageFactor->value_input = $data[$ageFactor->input_key];
            $ageFactor->user_score = $this->compareValue($ageFactor, $ageFactor->value_input);

        }
        return $ageFactors;
    }

    private function Information($informationFactors, $data){


        foreach ($informationFactors as &$informationFactor){
            $informationFactor->value_input = $data[$informationFactor->input_key];
            $informationFactor->user_score = $this->compareValue($informationFactor, $informationFactor->value_input);

        }

        return $informationFactors;
    }



}