<?php
/**
 * Created by PhpStorm.
 * User: vunguyen
 * Date: 11/23/18
 * Time: 5:48 PM
 */

namespace App\Services\Score;


use App\Interaction;
use App\Order;
use App\OrderItem;
use App\Score;
use App\Services\Service;
use App\Shopping;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Webpatser\Uuid\Uuid;

class ScoreBaseService extends Service
{

    protected $defaultRole = 4;

    protected $accountService, $historicalService;

    public function __construct(AccountService $accountService, HistoricalService $historicalService)
    {
        $this->accountService = $accountService;
        $this->historicalService = $historicalService;
    }

    public function index(Request $request){

        $shopperId = $request->get('shopper_id', 0);

        $interaction  = Interaction::where('client_id',$request->client->id)
            ->where(function ($query) use($request) {
                $query->where('email_address', '=', $request->get('email_address',''))
                    ->orWhere('phone_number', '=', $request->get('phone_number',''));
            });


        if(($interaction->count() ==0)){
            $inter =[
                'shopper_id'        => $request->get('shopper_id'),
                'client_id'         => $request->client->id,
                'first_name'        => $request->get('first_name',''),
                'first_name'        => $request->get('first_name',''),
                'last_name'         => $request->get('last_name',''),
                'email_address'     => $request->get('email_address',''),
                'phone_number'      => $request->get('phone_number',''),
                'registration_date' => $request->get('registration_date',''),
            ];
            $interaction = Interaction::create($inter);
        }else{
            $interaction = $interaction->first();
        }

        if(empty($interaction->email_address)){
            if(!empty($request->get('email_address',''))){
                Interaction::where('id',$interaction->id)->update([
                    'email_address' =>$request->get('email_address','')
                ]);
            }

        }
        // found data
        if(!empty($interaction->shopper_id)){
            $shopperId =  $interaction->shopper_id;
        }


        $data = $request->all();

        $hasNewOrder = false;
        // insert into log table
        if(!empty($data['orders'])) {
            foreach ($data['orders'] as $row) {
                $endDate = array_get($row, 'order_end_date', '');
                if(!empty($endDate)){
                    $endDate =  substr($endDate, 0, 10);
                }
                if(!Shopping::where('order_code', $row['order_code'])
                    ->where('interaction_id',$interaction->id)
                    ->exists()){
                    $hasNewOrder = true;
                }

                Shopping::firstOrCreate([
                    'order_code'    => $row['order_code'],
                    'interaction_id'  =>  $interaction->id,
                ], [
                    'order_amount'              => $row['order_amount'],
                    'is_paid'                   => array_get($row, 'is_paid', false),
                    'is_canceled'               => array_get($row, 'is_canceled', false),
                    'is_cod'                    => array_get($row, 'is_cod', false),
                    'is_purchased_visa'         => array_get($row, 'is_purchased_visa', false),
                    'is_purchased_by_atm'       => array_get($row, 'is_purchased_by_atm', false),
                    'is_purchased_by_paypal'    => array_get($row, 'is_purchased_by_paypal', false),
                    'order_end_date'            => $endDate,
                    'shopper_id'    => $shopperId,
                    'client_id'     => $request->client->id


                ]);
            }
        }else{
            $data['orders'] = [];
        }


        if(!empty($interaction->user_id) && !$hasNewOrder){
            // return
            $credit = Order::where('user_id','=', $interaction->user_id)
                ->where('status',1)
                ->sum('total');

            $data = $interaction->toArray();
            $data['nid_verified'] = 1;

            $user = User::find($interaction->user_id);

            return [
                'verified'          => 'PASSED',
                'current_credit'    => $credit,
                'balance'           => $user->balance,
                'limitation'        => $user->balance
            ];
        }


        $configs = DB::table('configs')
            ->get();

        $decisions = DB::table('decisions')->get();

        $ranges = DB::table('ranges')->get();

        $currentCredit = 0;


        $data['orders'] = Shopping::where('interaction_id',$interaction->id)
            ->get()
            ->toArray();

        $data['current_credit'] = $currentCredit;

        $calculate =  $this->calculate($configs,$decisions,$ranges,$data);
        $hash = Uuid::generate()->string;

        $score = Score::create([
            'interaction_id'        => $interaction->id,
            'shopper_id'        => $request->get('shopper_id'),
            'client_id'         => $request->client->id,
            'inputs'            => json_encode($data),
            'data'              => json_encode(@$calculate['data']),
            'decisions'         => json_encode($decisions),
            'factors'           => json_encode(@$calculate['metaData']['factors']),
            'ranges'            => json_encode($ranges),
            'M_score'           => @$calculate['metaData']['M_score'],
            'results'           => (@$calculate['result']["verified"]),
            'max_credit'        => (@$calculate['result']["max_credit"]),
            'current_credit'    => (@$calculate['result']["current_credit"]),
            'balance'           => (@$calculate['result']["balance"]),
            'limitation'        => (@$calculate['result']["limitation"]),
            'checksum'          => $hash,
        ]);

        if($calculate['result']["verified"] == 'PASSED'){
            // update balance
            $interaction  = Interaction::where('client_id',$request->client->id)
                ->where('shopper_id',$shopperId)
                ->first();

            if(empty($interaction->user_id) && !empty($interaction->email_address)){

                $user = User::firstOrCreate([
                    'email' =>$interaction->email_address,
                    'phone' =>$interaction->phone_number,
                ],[
                    'email' =>$interaction->email_address,
                    'nid' =>$request->get('nid',''),
                    'phone' =>$interaction->phone_number,
                    'name' =>$request->first_name.' '.$request->last_name,
                    'password' => '-1',
                    'address' =>'',
                    'address_note' =>'',
                    'ward' => '',
                    'district' => '',
                    'city' => '',
                    'balance' => $calculate['result']['limitation']

                ]);
                $user->roles()->attach($this->defaultRole);

                $interaction->update(['user_id' => $user->id]);


            }
            if(!empty($interaction->user_id)){
                User::where('id',$interaction->user_id)->update(['balance' =>$calculate['result']['limitation'] ]);
            }

        }



        @$calculate['result']['checksum'] = $hash;
        return $calculate['result'];
    }


    private function calculate($configs,$decisions,$ranges, $data){

        $date1 = new \DateTime($data['registration_date']);
        $date2 = new \DateTime(date('Y-m-d'));
        $interval = date_diff($date1, $date2);

        $accountFactors = [];
        $historicalFactors = [];
        foreach ($configs as $config) {
            if ($config->status == 'Enable') {
                switch ($config->factor) {
                    case "ACCOUNT":
                        $accountFactors[] = $config;
                        break;
                    case "HISTORICAL":
                        $historicalFactors[] = $config;
                        break;
                    default:
                }
            }
        }

        $result =[];
        $data['registered_months'] =$interval->m + ($interval->y * 12);
        $data['customer_registration_date'] =$interval->days;
        // update data needed to resolve
        $data['total_order_by_atm'] = 0;
        $data['total_order_by_visa'] = 0;
        $data['total_order_by_cod'] = 0;
        $last12Month = date('Y-m-d 00:00:00',strtotime('-12month'));
        $last6Month = date('Y-m-d 00:00:00',strtotime('-6month'));
        $data['total_order_last_12_month'] = 0;
        $data['total_order_paid_last_12_month'] = 0;

        $data['total_order_last_6_month']= 0;
        $data['minimum_order_value'] = 0;
        $data['maximum_order_value'] = 0;
        $data['total_order_canceled'] = 0;
        $data['total_orders'] = 0;
        $data['nid_verified'] = 0;
        $data['has_canceled_order'] = 0;
        $data['total_amount'] = 0;

        foreach ($data['orders'] as $order){
            $data['total_orders']++;
            if(array_get($order,'is_purchased_by_atm', 0)){
                $data['total_order_by_atm'] += 1;
            }
            if(array_get($order,'is_purchased_visa', 0)){
                $data['total_order_by_visa'] += 1;
            }
            if(array_get($order,'is_cod', 0)){
                $data['total_order_by_cod'] += 1;
            }
            if(array_get($order,'is_canceled', 1)){
                $data['total_order_canceled'] += 1;
                $data['has_canceled_order'] = 1;
            }
            if($order['is_paid']){
                $data['total_amount'] += $order['order_amount'];
            }
            if($order['order_end_date'] >= $last12Month && $order['is_paid']){
                $data['total_order_paid_last_12_month'] += 1;
            }
            if($order['order_end_date'] >= $last12Month){
                $data['total_order_last_12_month'] += 1;
            }
            if($order['order_end_date'] >= $last6Month && $order['is_paid']){
                $data['total_order_last_6_month']+=1;
            }
            if($data['minimum_order_value']  == 0){
                $data['minimum_order_value'] = $order['order_amount'];
            }
            if($data['maximum_order_value']  == 0){
                $data['maximum_order_value'] = $order['order_amount'];
            }
            if($data['minimum_order_value']  >= $order['order_amount']){
                $data['minimum_order_value'] = $order['order_amount'];
            }
            if($data['maximum_order_value']  <= $order['order_amount']){
                $data['maximum_order_value'] = $order['order_amount'];
            }
        }
        $data['ratio_canceled'] = 0;
        $data['ratio_cod'] = 0;

        if($data['total_orders'] >0){
            $data['ratio_canceled'] = $data['total_order_canceled']/$data['total_orders']*100;
            $data['ratio_cod'] = $data['total_order_by_cod']/$data['total_orders']*100;
        }


        if (!empty($accountFactors)) {
            $result['accountScore'] = $this->accountService->getScore($accountFactors, $data);

        }
        if (!empty($historicalFactors)) {
            $result['historicalScore'] = $this->historicalService->getScore($historicalFactors, $data);

        }

        $metaData = [];
        $metaData['factors'] = [];
        $metaData['max_point'] = 0;
        $metaData['user_point'] = 0;
        $metaData['ratio'] = 0;
        if(!empty($result)){
            foreach ($result as $factors){
                foreach ($factors as $subFactors){
                    foreach ($subFactors as $factor){
                        $metaData['factors'][]  = $factor;
                        $metaData['max_point'] += $factor->point;
                        $metaData['user_point'] += $factor->user_score;
                    }
                }
            }
        }

        if($metaData['max_point'] > 0){
            $metaData['M_score'] = $metaData['user_point'] / $metaData['max_point'];
        }

        // load decision
        $sumWeightPoint = 0;
        if(!empty($decisions)){
            foreach ($decisions as &$decision){
                if($decision->source == 'M_score'){
                    $decision->points = $metaData['M_score'];
                    $decision->weight_points = $decision->points * $decision->weight / 100;
                    $sumWeightPoint += $decision->weight_points;
                }
            }
        }

        $selectedRange = null;
        foreach ($ranges as $range){
            if($range->from <=$sumWeightPoint && $range->to >= $sumWeightPoint){
                $selectedRange = $range;
            }
        }

        if(empty($selectedRange)){
            return [
                'metaData' => $metaData ,
                'data' => $data,
                'result'=> [
                    'verified'          => 'FAILED',
                    'max_credit'        => 0,
                    'current_credit'    => $data['current_credit'],
                    'limitation'        => 0
                ]
            ];

        }

        $avgAmount = 0;
        $limitation = $selectedRange->limitation;
        if($data['total_orders'] >0){
            $avgAmount = $data['total_amount']/$data['total_order_paid_last_12_month'];
            $limitation = $avgAmount > $selectedRange->limitation?$selectedRange->limitation:$avgAmount;
        }

        $currentBalance  = $selectedRange->limitation;
        if(!empty($data['current_credit'])){
            $currentBalance  = $selectedRange->limitation - $data['current_credit'];
            $limitation = $limitation < $currentBalance ? $limitation : $currentBalance;
        }



        return [
            'metaData'  =>   $metaData,
            'data'      =>   $data,
            'result'    =>  [
                'verified'          => 'PASSED',
                'max_credit'        => $selectedRange->limitation,
                'current_credit'    => $data['current_credit'],
                'balance'           => $currentBalance,
                'limitation'        => $limitation
            ]
        ];
    }
}
