<?php
/**
 * Created by PhpStorm.
 * User: vunguyen
 * Date: 11/23/18
 * Time: 5:48 PM
 */

namespace App\Services\Score;


use App\Services\Service;

class HistoricalService extends Service
{


    public function __construct()
    {
    }


    public function getScore($factors, $data){
        $orderFactors =  [];
        $productFactors =  [];
        $paymentMethodFactors =  [];
        $orderHistoryFactors =  [];
        $deliveryFactors =  [];

        if(!empty($factors)){
            foreach ($factors as $factor ){

                switch ($factor->sub_factor) {
                    case "Order":
                        $orderFactors[] = $factor;
                        break;
                    case "Product":
                        $productFactors[] = $factor;
                        break;
                    case "Payment Method":
                        $paymentMethodFactors[] = $factor;
                        break;
                    case "Order History":
                        $orderHistoryFactors[] = $factor;
                        break;
                    case "Delivery":
                        $deliveryFactors[] = $factor;
                        break;

                    default:
                }

            }
        }
        $result = [];
        if(!empty($productFactors)){
            $result['productScore'] = $this->product($productFactors, $data);
        }
        if(!empty($orderFactors)){
            $result['orderScore']  = $this->order($orderFactors, $data);
        }

        if(!empty($paymentMethodFactors)){
            $result['paymentMethodScore']   = $this->paymentMethod($paymentMethodFactors, $data);
        }

        if(!empty($orderHistoryFactors)){
            $result['orderHistoryScore']   = $this->orderHistory($orderHistoryFactors, $data);
        }

        if(!empty($deliveryFactors)){
            $result['deliveryScore']  = $this->delivery($deliveryFactors, $data);
        }



        return $result;
    }


    private function product($productFactors, $data){

        foreach ($productFactors as &$productFactor){
            $productFactor->value_input = $data[$productFactor->input_key];
            $productFactor->user_score = $this->compareValue($productFactor, $productFactor->value_input);

        }
        return $productFactors;
    }

    private function order($orderFactors, $data){

        foreach ($orderFactors as &$orderFactor){
            $orderFactor->value_input = $data[$orderFactor->input_key];
            $orderFactor->user_score = $this->compareValue($orderFactor, $orderFactor->value_input);


        }
        return $orderFactors;
    }


    private function paymentMethod($paymentMethodFactors, $data){


        foreach ($paymentMethodFactors as &$paymentMethodFactor){
            $paymentMethodFactor->value_input = $data[$paymentMethodFactor->input_key];
            $paymentMethodFactor->user_score = $this->compareValue($paymentMethodFactor,$paymentMethodFactor->value_input);
        }
        return $paymentMethodFactors;
    }


    private function orderHistory($oderHistoryFactors, $data){

        foreach ($oderHistoryFactors as &$oderHistoryFactor){
            $oderHistoryFactor->value_input = $data[$oderHistoryFactor->input_key];
            $oderHistoryFactor->user_score = $this->compareValue($oderHistoryFactor, $oderHistoryFactor->value_input);
        }
        return $oderHistoryFactors;
    }


    private function delivery($deliveryFactors, $data){

        foreach ($deliveryFactors as &$deliveryFactor){
            $deliveryFactor->value_input = $data[$deliveryFactor->input_key];
            $deliveryFactor->user_score = $this->compareValue($deliveryFactor, $deliveryFactor->value_input);

        }
        return $deliveryFactors;
    }



}