<?php

namespace App\Services;
use App\Notifications\InterestedAccount;
use App\SocialFacebookAccount;
use App\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Password;
use Laravel\Socialite\Contracts\User as ProviderUser;

class SocialFacebookAccountService
{
    public function createOrGetUser(ProviderUser $providerUser)
    {
        Log::info(json_encode($providerUser));
        $account = SocialFacebookAccount::whereProvider('facebook')
            ->whereProviderUserId($providerUser->getId())
            ->first();
        Log::info($account);
        // call api directlly
        if ($account) {
            return $account->user;
        } else {

            $account = new SocialFacebookAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => 'facebook'
            ]);

            Log::info($providerUser->getEmail());

            $user = User::whereEmail($providerUser->getEmail())->first();

            if (!$user) {

                $user = User::create([
                    'email' => $providerUser->getEmail(),
                    'name' => $providerUser->getName(),
                    'password' => md5(rand(1,10000)),
                ]);
                $user->roles()->attach(4);

                $token = Password::getRepository()->create($user);
                $user->notify(new InterestedAccount($token, $user));

            }

            $account->user()->associate($user);
            $account->save();

            return $user;
        }
    }
}
