<?php
/**
 * Created by PhpStorm.
 * User: vunguyen
 * Date: 2019-04-16
 * Time: 13:51
 */

namespace App\Observers;


use App\Order;
use Illuminate\Support\Facades\DB;

class OrderObserver
{


    public function created(Order $order)
    {
        // sync to merchant


        $orderMerchantId  = DB::connection('order_merchant')->table('orders')
            ->insertGetId([
                'order_code' => $order->order_code,
                'total' => $order->total,
                'fee' => $order->fee,
                'discount' => $order->discount,
                'tax' => $order->tax,
                'status' => $order->status,
                'billing_address' => $order->billing_address,
                'billing_address' => $order->billing_address,
                'billing_address_note' => $order->billing_address_note,
                'billing_contact_name' => $order->billing_contact_name,
                'billing_contact_phone' => $order->billing_contact_phone,
                'user_id' => $order->user_id,
                'client_id' => $order->client_id,
                'created_by_team_id' => $order->client_id,
                'billing_ward'  => $order->billing_ward,
                'billing_district' => $order->billing_district,
                'payment_token' => $order->payment_token,
                'due_date'  => $order->due_date
            ]);

        foreach ($order->items as $item){
               DB::connection('order_merchant')->table('order_items')
                ->insert([
                    'sku' => $item->sku,
                    'product' => $item->product,
                    'price' => $item->price,
                    'qty' => $item->qty,
                    'subtotal' => $item->subtotal,
                    'status' => $item->status,
                    'order_id' => $orderMerchantId,
                    'created_by_team_id' => $order->client_id,

                ]);

        }
    }

    public function updated(Order $order)
    {
        DB::connection('order_merchant')->table('orders')
            ->where('order_code', $order->order_code)
            ->where('created_by_team_id',$order->client_id )
            ->update([
                'status' =>$order->status
            ]);
    }



}