<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $guarded = [];

    protected $appends = ['status_text','payment_method_text'];

    public static $statistics = [
        1 => ['text' => 'Khởi tạo','class' =>'label-danger', 'code' => 'init', 'value' => '1'],
        2 => ['text' => 'Đã thanh toán','class' =>'label-success', 'code' => 'paid', 'value' => '2'],
        3 => ['text' => 'Thanh toán 1 phần','class' =>'label-warning', 'code' => 'partial', 'value' => '3'],
        -1 => ['text' => 'Chờ thanh toán','class' =>'label-warning', 'code' => 'pending', 'value' => '-1'],
        -2 => ['text' => 'Đã gửi thông tin qua dịch vụ thu hộ','class' =>'label-warning', 'code' => 'pending', 'value' => '-2'],
        -3 => ['text' => 'TIỀN THU HỘ ĐÃ ĐƯỢC THU','class' =>'label-warning', 'code' => 'pending', 'value' => '-3'],
        -4 => ['text' => 'TIỀN THU HỘ ĐÃ ĐƯỢC CHUYỂN KHOẢN','class' =>'label-warning', 'code' => 'pending', 'value' => '-4'],
    ];

    public static $methods = [
        'cash' => ['text' => 'Thu tiền mặt','class' =>'text-primary', 'code' => 'init', 'value' => 'cash'],
        'domestic' => ['text' => 'Thẻ nội địa/ATM','class' =>'text-success', 'code' => 'paid', 'value' => 'domestic'],
        'international_card' => ['text' => 'Thẻ quốc tế','class' =>'text-info', 'code' => 'partial', 'value' => 'international_card'],
        'bank_transfer' => ['text' => 'Chuyển khoản','class' =>'text-info', 'code' => 'partial', 'value' => 'bank_transfer'],
    ];


    public function relations()
    {
        return $this->hasMany('App\InvoiceRelation');
    }

    public function orderRelations()
    {
        return $this->hasMany('App\InvoiceOrderRelation');
    }

    public function events()
    {
        return $this->hasMany('App\InvoiceEvent');
    }


    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getPaymentMethodTextAttribute(){
        if(!empty(self::$methods[$this->payment_method])){
            return self::$methods[$this->payment_method];
        }
    }



    public function getStatusTextAttribute(){
        if(!empty($this->status)){
            foreach (self::$statistics as $status){
                if($status['value'] == $this->status){
                    return $status;
                }
            }
        }else{
            return '';
        }
    }


}
