<?php

namespace App\Console\Commands;

use App\ShopperCriterias;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ExportReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:exportReport';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $data = DB::table('scores')
            ->join('interactions', 'scores.shopper_id', '=', 'interactions.shopper_id')
            ->where('scores.results','FAILED')
            ->get();
        $reports = [];
        foreach ($data as $row){

            $tmp = [
                'email' => $row->email_address,
                'date' => $row->created_at,
                'shopper_id' => $row->shopper_id,
            ];

            $factors  = (json_decode($row->factors,true));
            foreach ($factors as $fac){

                $r = $tmp;
                $r['criteria_id'] =  $fac['id'];
                $r['criteria_name'] =  $fac['input_key'];
                $r['score_id'] =  $row->id;
                $r['value_input'] = $fac['value_input'];
                $r['user_score'] = $fac['user_score'];

                $flight = ShopperCriterias::firstOrCreate(
                    [
                        'criteria_id' => $fac['id'],
                        'score_id' => $row->id,
                        ],
                    $r
                );

            }


        }


    }
}
