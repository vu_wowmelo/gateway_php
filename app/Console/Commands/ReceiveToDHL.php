<?php

namespace App\Console\Commands;

use App\Invoice;
use App\InvoiceEvent;
use App\Order;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ReceiveToDHL extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'receive:dhl';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $invoices = Invoice::where('status', -2)
            ->where('payment_method', 'cash')
            ->get();

        $trackingReferenceNumber = [];

        foreach ($invoices  as $invoice){
            $trackingReferenceNumber[]  = 'WOWMELO_'.$invoice->id;
        }

        if(empty($trackingReferenceNumber)){
            return;
        }
        try {
            // send a shipment to DHL
            $client = new \GuzzleHttp\Client();
            $res = $client->request('GET', env('DHL_ENDPOINT', '') . '/v1/OAuth/AccessToken', [
                'query' => [
                    'clientId' => env('DHL_CLIENT_ID', ''),
                    'password' => env('DHL_PASSWORD', ''),
                ]
            ]);


            $body = $res->getBody();
            $dhlToken = (json_decode($body->getContents())->accessTokenResponse->token);

            $params = array(
                'trackItemRequest' =>
                    array(
                        'hdr' =>
                            array(
                                'messageType' => 'TRACKITEM',
                                'accessToken' => $dhlToken,
                                'messageDateTime' => date('Y-m-d').'T'.date('H:i:sP'),
                                'messageVersion' => '1.0',
                                'messageLanguage' => 'en',
                            ),
                        'bd' =>
                            array(
                                'pickupAccountId' => env('DHL_PICKUP_ACCT'),
                                'soldToAccountId' => env('DHL_SOLD_TO_ACCT'),
                                'ePODRequired' => 'Y',
                                'trackingReferenceNumber' =>$trackingReferenceNumber

                            ),
                    ),
            );


            $res = $client->request('POST', env('DHL_ENDPOINT', '') . '/v3/Tracking', [
                'json' => $params
            ]);

            $body = $res->getBody();
            $dhlResponse = (json_decode($body->getContents()));



            if($dhlResponse->trackItemResponse->bd->responseStatus->code == 200){
                $shipmentItems  = $dhlResponse->trackItemResponse->bd->shipmentItems;
                foreach ($shipmentItems as $shipmentItem){
                    $events = ($shipmentItem->events);
                    $invoiceId = explode('_',$shipmentItem->shipmentID);
                    $invoiceId = $invoiceId[1];

                    foreach ($events as $event){

                        $row = [
                            'status' => $event->status,
                            'description'  => $event->description,
                            'tracking_id' => $shipmentItem->trackingID,
                            'invoice_id' =>$invoiceId
                        ];

                        InvoiceEvent::firstOrCreate($row,$row);
                        // update order and invoice
                        if($event->status  == 77223){
                            $invoice = Invoice::find($invoiceId);
                            $invoice->update(['status' => -3]);
                            Order::where('payment_token', $invoice->payment_token)->update(['status' => -3]);
                        }

                        if($event->status  == 77222){
                            $invoice = Invoice::find($invoiceId);
                            $invoice->update(['status' => -4]);
                            Order::where('payment_token', $invoice->payment_token)->update(['status' => -4]);
                        }

                    }
                }

                exit;
            }
        } catch (\Exception $e) {

            print_r($e);
        }
        exit;

    }
}
