<?php

namespace App\Console\Commands;

use App\Order;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SyncOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:sync-order';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $orders = Order::with('items')->get();
        foreach ($orders as $order){
            if(DB::connection('order_merchant')->table('orders')
                ->where('order_code',$order->order_code )
                ->where('created_by_team_id' , $order->client_id)
                ->exists()){
                continue;

            }

            $orderMerchantId  = DB::connection('order_merchant')->table('orders')
                ->insertGetId([
                    'order_code' => $order->order_code,
                    'total' => $order->total,
                    'fee' => $order->fee,
                    'discount' => $order->discount,
                    'tax' => $order->tax,
                    'status' => $order->status,
                    'billing_address' => $order->billing_address,
                    'billing_address' => $order->billing_address,
                    'billing_address_note' => $order->billing_address_note,
                    'billing_contact_name' => $order->billing_contact_name,
                    'billing_contact_phone' => $order->billing_contact_phone,
                    'user_id' => $order->user_id,
                    'client_id' => $order->client_id,
                    'created_by_team_id' => $order->client_id,
                    'billing_ward'  => $order->billing_ward,
                    'billing_district' => $order->billing_district,
                    'payment_token' => $order->payment_token,
                    'due_date'  => $order->due_date
                ]);

            foreach ($order->items as $item){
                $orderMerchantId  = DB::connection('order_merchant')->table('order_items')
                    ->insertGetId([
                        'sku' => $item->sku,
                        'product' => $item->product,
                        'price' => $item->price,
                        'qty' => $item->qty,
                        'subtotal' => $item->subtotal,
                        'status' => $item->status,
                        'order_id' => $orderMerchantId,
                        'created_by_team_id' => $order->client_id,

                    ]);

            }
        }

    }
}
