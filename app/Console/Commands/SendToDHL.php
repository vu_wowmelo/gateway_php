<?php

namespace App\Console\Commands;

use App\DHLProvince;
use App\Invoice;
use App\Order;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class SendToDHL extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:dhl';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $invoice = Invoice::where('status',-1)
            ->where('payment_method','cash')
            ->first();


        print_r($invoice->toArray());

        try{
            // send a shipment to DHL
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', env('DHL_ENDPOINT','').'/v1/OAuth/AccessToken', [
            'query' => [
                'clientId' => env('DHL_CLIENT_ID',''),
                'password' => env('DHL_PASSWORD',''),
            ]
        ]);

        $body = $res->getBody();
        $dhlToken = (json_decode($body->getContents())->accessTokenResponse->token);


        $params =array (
            'manifestRequest' =>
                array (
                    'hdr' =>
                        array (
                            'messageType' => 'SHIPMENT',
                            'messageDateTime' => date('Y-m-d').'T'.date('H:i:sP'),
                            'accessToken' => $dhlToken,
                            'messageVersion' => '1.0',
                            'messageLanguage' => 'en',
                        ),
                    'bd' =>
                        array (
                            'pickupAccountId' => env('DHL_PICKUP_ACCT'),
                            'soldToAccountId' => env('DHL_SOLD_TO_ACCT'),
                            'pickupDateTime' => date('Y-m-d',strtotime("+45 days")).'T'.date('H:i:sP',strtotime("+45 days")),
                            'handoverMethod' => 1,
                            'shipperAddress' =>
                                array (
                                    'name' => $invoice->billing_contact_name,
                                    'address1' => $invoice->billing_address,
                                    'address2' => '',
                                    'state' => $invoice->billing_city,
                                    'district' => $invoice->billing_district,
                                    'country' => 'VN',
                                    'phone' => $invoice->billing_contact_phone,
                                    'email' => $invoice->billing_contact_email ,
                                ),
                            'shipmentItems' =>
                                array (
                                    0 =>
                                        array (
                                            'consigneeAddress' =>
                                                array (
                                                    'name' => "Wowmelo Trading Ltd",
                                                    'address1' => '180-192 Nguyen Cong Tru',
                                                    'address2' => 'Nguyen Thai Binh Ward',
                                                    'state' => 'Hồ Chí Minh',
                                                    'district' => 'Quận 1',
                                                    'country' => 'VN',
                                                    'phone' => '090 9011 869',
                                                    'email' => 'info@wowmelo.com',
                                                ),
                                            'shipmentID' => 'WOW_'.$invoice->id,
                                            'packageDesc' => $invoice->description,
                                            'totalWeight' => 10,
                                            'totalWeightUOM' => 'g',
                                            'dimensionUOM' => 'CM',
                                            'height' => 10.0,
                                            'length' => 30.0,
                                            'width' => 20.0,
                                            'productCode' => 'PDO',
                                            'contentIndicator' => null,
                                            'codValue' => $invoice->total,
                                            'insuranceValue' => NULL,
                                            'totalValue' => $invoice->total,
                                            'currency' => 'VND',
                                            'remarks' => 'WOWMELO_'.$invoice->id,
                                            'returnMode' => '05'
                                        ),
                                ),
                        ),
                ),
        );

            Log::info($params);
            Log::info(json_encode($params));
            $res = $client->request('POST', env('DHL_ENDPOINT','').'/v3/Shipment', [
                'json' => $params
            ]);

            $body = $res->getBody();
            $dhlResponse = (json_decode($body->getContents()));

            print_r($dhlResponse);

            if($dhlResponse->manifestResponse->bd->responseStatus->code == 200){
                $shipmentItems = $dhlResponse->manifestResponse->bd->shipmentItems;
                $invoice->update(['status' => -2,'shipment_id' => $shipmentItems[0]->shipmentID]);
                Order::where('payment_token',$invoice->payment_token)
                    ->update(['status' => -2]);
            }

        }catch (\Exception $e){

        }
        exit;

    }
}
