<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceRelation extends Model
{
    protected $guarded = [];

    public function invoice()
    {
        return $this->belongsTo('App\Invoice');
    }

    public function orderItem()
    {
        return $this->belongsTo('App\OrderItem');
    }



}
