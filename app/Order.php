<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    public static $statistics = [
        1 => ['text' => 'Khởi tạo','class' =>'label-danger', 'code' => 'init', 'value' => '1'],
        2 => ['text' => 'Đã thanh toán','class' =>'label-success', 'code' => 'paid', 'value' => '2'],
        3 => ['text' => 'Thanh toán 1 phần','class' =>'label-warning', 'code' => 'partial', 'value' => '3'],
        -1 => ['text' => 'Chờ thanh toán','class' =>'label-warning', 'code' => 'pending', 'value' => '-1'],
        -2 => ['text' => 'Đã gửi thông tin qua dịch vụ thu hộ ','class' =>'label-warning', 'code' => 'pending', 'value' => '-1'],
        -3 => ['text' => 'TIỀN THU HỘ ĐÃ ĐƯỢC THU','class' =>'label-warning', 'code' => 'pending', 'value' => '-3'],
        -4 => ['text' => 'TIỀN THU HỘ ĐÃ ĐƯỢC CHUYỂN KHOẢN','class' =>'label-warning', 'code' => 'pending', 'value' => '-4'],
    ];

    protected $guarded = [];

    protected $appends = ['status_text','order_code_text'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }


    public function client()
    {
        return $this->belongsTo('App\Client','client_id','id');
    }


    public function items()
    {
        return $this->hasMany('App\OrderItem');
    }

    public function getOrderCodeTextAttribute(){
        if(!empty($this->order_code)){
            return substr($this->order_code,0,6);
        }

    }




    public function getStatusTextAttribute(){
        if(!empty(self::$statistics[$this->status])){
            return self::$statistics[$this->status];
        }
    }


}
