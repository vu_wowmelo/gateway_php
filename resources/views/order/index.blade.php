@extends('layouts.app')

@section('content')

<div class="row">
    <form class="submit-form" id="formOrder">

    <div class="col-md-9">

            <div class="box box-success">
            <div class="box-header with-border">
                <h4 class="box-title">Chọn sản phẩm bạn muốn trả tiền</h4>
            </div>
            <div class="box-body">
                <!-- the events -->

                <table id="table-orders" class="table table-hover">
                    <thead>
                    <tr>
                        <th>Mã đơn hàng</th>
                        <th>Mua tại</th>
                        <th>Số tiền</th>
                        <th class="visible-lg">Ngày đến hạn</th>
                        <th class="visible-lg">Chọn</th>
                    </tr>
                    </thead>
                    <tbody>
                    @unless (count($orders))
                        <tr>
                            <td colspan="5"> <p>Bạn chưa đơn hàng nào cần trả tiền.</p></td>
                        </tr>

                    @endunless

                    @if(!empty($orders))
                        @foreach($orders as $order)
                    <tr data-total="{{$order->total}}"   class="info order{{$order->id}}" >
                        <td class="text-bold">#{{$order->order_code_text}}</td>
                        <td class="text-bold">{{$order->client->name}}</td>
                        <td>{{number_format($order->total,0,',','.')}}</td>
                        <td  class="visible-lg">{{$order->due_date}}</td>
                        <td  class="visible-lg">
                            <div class="icheck-primary icheck-inline">
                                <input
                                        data-total="{{$order->payment_amount}}"
                                        data-payment_token="{{$order->payment_token}}"
                                        data-discount="{{$order->discount}}"
                                        data-fee="{{$order->fee}}"
                                        checked  data-id={{$order->id}} class="checkbox_order" type="radio" name="checkbox_"
                                       id="checkbox_{{$order->id}}" >
                                <label for="checkbox_{{$order->id}}"></label>
                            </div>
                        </td>

                    </tr>
                        @foreach($order->items as $item)
                        <tr data-subtotal="{{$item->subtotal}}" data-id="{{$item->id}}" class=" text-green detail{{$order->id}}">
                            <td colspan="2" class="text-center">{{$item->product}}. Số lượng: {{$item->qty}}</td>
                            <td>{{number_format($item->subtotal,0,',','.')}}</td>
                            <td  class="visible-lg">{{$order->due_date}}</td>
                            <td  class="visible-lg"></td>
                        </tr>
                        @endforeach
                    @endforeach
                    @endif

                    </tbody>
                </table>

            </div>
            <!-- /.box-body -->
        </div>
    </div>
    <div class="col-md-3">
        <div class="box box-success">
            <div class="box-header with-border">
                <h4 class="box-title">Tổng số phải trả</h4>
            </div>
            <div class="box-body">

                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                        <tr>
                            <th style="width:50%">Tổng tiền:</th>
                            <td id="subTotalAmount"></td>
                        </tr>
                        <tr>
                            <th>Phí trễ hạn</th>
                            <td>---</td>
                        </tr>
                        <tr>
                            <th>Phí dịch vụ:</th>
                            <td>---</td>
                        </tr>
                        <tr>
                            <th>Tổng cộng:</th>
                            <td id="totalAmount"></td>
                        </tr>
                        <tr>
                            <th colspan="2">
                                Chọn phương thức thanh toán:
                            </th>
                        </tr>
                        <tr>
                            <th colspan="2">
                                <select name="payment_method" class="form-control" id="selectPaymentMethod">
                                    <option value="domestic">Thẻ nội địa</option>
                                    <option value="international_card">Thẻ quốc tế(Visa/MasterCard)</option>
                                    <option value="cash">Trả tiền mặt</option>
                                </select>

                            </th>
                        </tr>
                        <tr>
                            <th colspan="2" class="">
                                <select name="bank_code" class="form-control" id="selectBankCode">
                                    @foreach($domestics as $domestic)
                                        <option value="{{$domestic->code}}">{{$domestic->name}}</option>

                                    @endforeach
                                </select>

                            </th>
                        </tr>



                        </tbody>
                    </table>

                    <table class="table hidden" id="address-table">
                        <thead>
                        <tr>
                            <th>
                                Địa chỉ của bạn để wowmelo thu tiền:
                            </th>
                        </tr>
                        </thead>
                        <tr>
                            <th colspan="2" class="">
                                <select  name="billing_city" class="form-control" id="tinh_thanh_pho">
                                    @foreach($provinces as $province)
                                        <option
                                                @if($province->province ==$selectedProvince)
                                                selected
                                                @endif
                                                data-district="{{json_encode($dhlProvinceAll[$province->province])}}" value="{{$province->province}}">{{$province->province}}</option>
                                    @endforeach
                                </select>


                            </th>
                        </tr>

                        <tr>
                            <th colspan="2" class="">
                                <select name="billing_district" class="form-control" id="quan_huyen">
                                    @foreach($districts as $district)
                                        <option
                                                @if($district->district ==$selectedDistrict)
                                                selected
                                                @endif
                                                value="{{$district->district}}">{{$district->district}}</option>
                                    @endforeach
                                </select>


                            </th>
                        </tr>

                        <tr>
                            <th colspan="2" class="">
                                <input name="billing_address"  value="{{@$order->billing_address}}" type="text" class="form-control" id="address">


                            </th>
                        </tr>

                    </table>
                    <button id="btnPayment" type="button" class="btn btn-success btn-block center-block"><i class="fa fa-credit-card"></i>
                        Thanh toán
                    </button>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
    </form>

</div>
@endsection
