@component('mail::message')
    {{-- Greeting --}}
    @if (! empty($greeting))
        # {{ $greeting }}
    @else
        @if ($level === 'error')
            # @lang('Whoops!')
        @else
            # @lang('Xin chào!')
        @endif
    @endif

    {{-- Intro Lines --}}
@foreach ($introLines as $line)
{{$line }}

@endforeach
@component('mail::button', ['url' => $url])
    Thanh toán ngay
@endcomponent

@if(!empty($order))
## Chi tiết đơn hàng:
@component('mail::table')
    | Sản phẩm    | SL   | Giá  | Thành tiền|Hạn thanh toán|
    |:------  | -----------:|--------: |:--------:|:--------:|
    @foreach($order['items'] as $item)
        | {{$item->product}} | {{$item->qty}} | {{number_format($item->price,0,',','.')}} | {{number_format($item->subtotal,0,',','.')}} | {{date('d/m/Y',strtotime('+14 days', strtotime($item->created_at)))}} |
    @endforeach
    | Tổng cộng |  |   | {{number_format($order->total,0,',','.')}} | {{date('d/m/Y',strtotime('+14 days', strtotime($order->created_at)))}} |

@endcomponent
@endif

@component('mail::subcopy')
    @lang(
        "Nếu bạn không thể bấm vào nút \":actionText\", Vui lòng copy link phía dưới \n".
        'và dán vào trình duyệt: [:actionURL](:actionURL)',
        [
            'actionText' => "Thanh toán ngay",
            'actionURL' => $url,
        ]
    )
@endcomponent

@endcomponent
