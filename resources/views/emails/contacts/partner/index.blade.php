@component('mail::message')
# Có 1 liên hệ từ WowMelo Website nội dung chi tiết bên dưới

## Firstname: {{$params['firstname']}}
## Lastname: {{$params['lastname']}}
## Email: {{$params['email']}}
## Phone: {{$params['phone']}}
## Website: {{$params['website']}}
## Average Order Value: {{$params['average_order']}}
## Annual Sales: {{$params['annual_sales']}}

Trân trọng,<br>
{{ config('app.name') }}
@endcomponent
