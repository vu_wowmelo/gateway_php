@component('mail::message')
# Có 1 liên hệ từ WowMelo Website nội dung chi tiết bên dưới

## Tên: {{$params['name']}}
## Email: {{$params['email']}}
## Điện Thoại: {{$params['phone']}}
## Nội dung tin nhắn: {{$params['message']}}

Trân trọng,<br>
{{ config('app.name') }}
@endcomponent
