@extends('layouts.default')

@section('content')

    <div class="section white">
        <div class="container">
            <div class="title">
                @lang('messages.faq.title')
            </div>
            <div class="description">
                @lang('messages.faq.desc')
            </div>
            <h3>1. @lang('messages.faq.1.title')</h3>
            <div class="panel-group" id="faqAccordion1">
                <div class="panel panel-default ">
                    <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse"
                         data-parent="#faqAccordion" data-target="#question1_1">
                        <h4 class="panel-title">
                            <a class="accordion-toggle question-toggle collapsed" data-toggle="collapse"
                               data-parent="#faqAccordion1" href="#question1_1">

                                @lang('messages.faq.1.1.ques')
                            </a>
                        </h4>
                    </div>
                    <div id="question1_1" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">
                            @lang('messages.faq.1.1.ans')
                        </div>
                    </div>
                </div>
                <div class="panel panel-default ">
                    <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse"
                         data-parent="#faqAccordion" data-target="#question1_5">
                        <h4 class="panel-title">
                            <a class="accordion-toggle question-toggle collapsed" data-toggle="collapse"
                               data-parent="#faqAccordion1" href="#question1_5">

                                @lang('messages.faq.1.5.ques')
                            </a>
                        </h4>
                    </div>
                    <div id="question1_5" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">
                            @lang('messages.faq.1.5.ans')
                        </div>
                    </div>
                </div>
                <div class="panel panel-default ">
                    <div class="panel-heading  accordion-toggle question-toggle collapsed" data-toggle="collapse"
                         data-parent="#faqAccordion" data-target="#question1_2">
                        <h4 class="panel-title">
                            <a class="accordion-toggle question-toggle collapsed" data-toggle="collapse"
                               data-parent="#faqAccordion1" href="#question1_2">

                                @lang('messages.faq.1.2.ques')
                            </a>
                        </h4>
                    </div>
                    <div id="question1_2" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">
                            @lang('messages.faq.1.2.ans')
                        </div>
                    </div>
                </div>
                <div class="panel panel-default ">
                    <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse"
                         data-parent="#faqAccordion" data-target="#question1_3">
                        <h4 class="panel-title">
                            <a class="accordion-toggle question-toggle collapsed" data-toggle="collapse"
                               data-parent="#faqAccordion1" href="#question1_3">

                                @lang('messages.faq.1.3.ques')
                            </a>
                        </h4>
                    </div>
                    <div id="question1_3" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">
                            @lang('messages.faq.1.3.ans')
                        </div>
                    </div>
                </div>
                <div class="panel panel-default ">
                    <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse"
                         data-parent="#faqAccordion" data-target="#question1_4">
                        <h4 class="panel-title">
                            <a class="accordion-toggle question-toggle collapsed" data-toggle="collapse"
                               data-parent="#faqAccordion1" href="#question1_4">

                                @lang('messages.faq.1.4.ques')
                            </a>
                        </h4>

                    </div>
                    <div id="question1_4" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">
                            @lang('messages.faq.1.4.ans')
                        </div>
                    </div>
                </div>
            </div>
            <!--/panel-group-->
            <h3>2. @lang('messages.faq.2.title')</h3>
            <div class="panel-group" id="faqAccordion2">
                <div class="panel panel-default  accordion-toggle question-toggle collapsed" data-toggle="collapse"
                     data-parent="#faqAccordion" data-target="#question4">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle question-toggle collapsed" data-toggle="collapse"
                               data-parent="#faqAccordion1" href="#question4">

                                @lang('messages.faq.2.1.ques')
                            </a>
                        </h4>
                    </div>
                    <div id="question4" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">
                            @lang('messages.faq.2.1.ans')
                        </div>
                    </div>
                </div>
                <div class="panel panel-default ">
                    <div class="panel-heading  accordion-toggle question-toggle collapsed" data-toggle="collapse"
                         data-parent="#faqAccordion" data-target="#question5">
                        <h4 class="panel-title">
                            <a class="accordion-toggle question-toggle collapsed" data-toggle="collapse"
                               data-parent="#faqAccordion1" href="#question5">

                                @lang('messages.faq.2.2.ques')
                            </a>
                        </h4>
                    </div>
                    <div id="question5" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">
                            @lang('messages.faq.2.2.ans')
                        </div>
                    </div>
                </div>
                <div class="panel panel-default ">
                    <div class="panel-heading  accordion-toggle question-toggle collapsed" data-toggle="collapse"
                         data-parent="#faqAccordion" data-target="#question6">
                        <h4 class="panel-title">
                            <a class="accordion-toggle question-toggle collapsed" data-toggle="collapse"
                               data-parent="#faqAccordion1" href="#question6">

                                @lang('messages.faq.2.3.ques')
                            </a>
                        </h4>
                    </div>
                    <div id="question6" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">
                            @lang('messages.faq.2.3.ans')
                        </div>
                    </div>
                </div>
                <div class="panel panel-default ">
                    <div class="panel-heading  accordion-toggle question-toggle collapsed" data-toggle="collapse"
                         data-parent="#faqAccordion" data-target="#question7">
                        <h4 class="panel-title">
                            <a class="accordion-toggle question-toggle collapsed" data-toggle="collapse"
                               data-parent="#faqAccordion1" href="#question7">

                                @lang('messages.faq.2.4.ques')
                            </a>
                        </h4>
                    </div>
                    <div id="question7" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">
                            @lang('messages.faq.2.4.ans')
                        </div>
                    </div>
                </div>
                <div class="panel panel-default ">
                    <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse"
                         data-parent="#faqAccordion2" data-target="#question8">
                        <h4 class="panel-title">
                            <a class="accordion-toggle question-toggle collapsed" data-toggle="collapse"
                               data-parent="#faqAccordion1" href="#question8">
                                @lang('messages.faq.2.5.ques')
                            </a>
                        </h4>
                    </div>
                    <div id="question8" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">
                            @lang('messages.faq.2.5.ans')
                        </div>
                    </div>
                </div>
                <div class="panel panel-default ">
                    <div class="panel-heading  accordion-toggle question-toggle collapsed" data-toggle="collapse"
                         data-parent="#faqAccordion" data-target="#question9">
                        <h4 class="panel-title">
                            <a class="accordion-toggle question-toggle collapsed" data-toggle="collapse"
                               data-parent="#faqAccordion1" href="#question9">

                                @lang('messages.faq.2.6.ques')
                            </a>
                        </h4>
                    </div>
                    <div id="question9" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">
                            @lang('messages.faq.2.6.ans')
                        </div>
                    </div>
                </div>
                <div class="panel panel-default ">
                    <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse"
                         data-parent="#faqAccordion" data-target="#question10">
                        <h4 class="panel-title">
                            <a class="accordion-toggle question-toggle collapsed" data-toggle="collapse"
                               data-parent="#faqAccordion1" href="#question10">

                                @lang('messages.faq.2.7.ques')
                            </a>
                        </h4>
                    </div>
                    <div id="question10" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">
                            @lang('messages.faq.2.7.ans')
                        </div>
                    </div>
                </div>
                <div class="panel panel-default ">
                    <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse"
                         data-parent="#faqAccordion" data-target="#question11">
                        <h4 class="panel-title">
                            <a class="accordion-toggle question-toggle collapsed" data-toggle="collapse"
                               data-parent="#faqAccordion1" href="#question11">

                                @lang('messages.faq.2.8.ques')
                            </a>
                        </h4>
                    </div>
                    <div id="question11" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">
                            @lang('messages.faq.2.8.ans')
                        </div>
                    </div>
                </div>
            </div>
            <!--/panel-group-->
            <h3>3. @lang('messages.faq.3.title')</h3>
            <div class="panel-group" id="faqAccordion3">
                <div class="panel panel-default accordion-toggle question-toggle collapsed" data-toggle="collapse"
                     data-parent="#faqAccordion" data-target="#question12">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle question-toggle collapsed" data-toggle="collapse"
                               data-parent="#faqAccordion1" href="#question12">

                                @lang('messages.faq.3.1.ques')
                            </a>
                        </h4>
                    </div>
                    <div id="question12" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">
                            @lang('messages.faq.3.1.ans')
                        </div>
                    </div>
                </div>
                <div class="panel panel-default ">
                    <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse"
                         data-parent="#faqAccordion" data-target="#question13">
                        <h4 class="panel-title">
                            <a class="accordion-toggle question-toggle collapsed" data-toggle="collapse"
                               data-parent="#faqAccordion1" href="#question13">

                                @lang('messages.faq.3.2.ques')
                            </a>
                        </h4>
                    </div>
                    <div id="question13" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">
                            @lang('messages.faq.3.2.ans')
                        </div>
                    </div>
                </div>
                <div class="panel panel-default ">
                    <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse"
                         data-parent="#faqAccordion" data-target="#question14">
                        <h4 class="panel-title">
                            <a class="accordion-toggle question-toggle collapsed" data-toggle="collapse"
                               data-parent="#faqAccordion1" href="#question14">

                                @lang('messages.faq.3.3.ques')
                            </a>
                        </h4>
                    </div>
                    <div id="question14" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">
                            @lang('messages.faq.3.3.ans')
                        </div>
                    </div>
                </div>
                <div class="panel panel-default ">
                    <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse"
                         data-parent="#faqAccordion" data-target="#question15">
                        <h4 class="panel-title">
                            <a class="accordion-toggle question-toggle collapsed" data-toggle="collapse"
                               data-parent="#faqAccordion1" href="#question15">

                                @lang('messages.faq.3.4.ques')
                            </a>
                        </h4>
                    </div>
                    <div id="question15" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">
                            @lang('messages.faq.3.4.ans')
                        </div>
                    </div>
                </div>
                <div class="panel panel-default ">
                    <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse"
                         data-parent="#faqAccordion" data-target="#question16">
                        <h4 class="panel-title">
                            <a class="accordion-toggle question-toggle collapsed" data-toggle="collapse"
                               data-parent="#faqAccordion1" href="#question16">

                                @lang('messages.faq.3.5.ques')
                            </a>
                        </h4>
                    </div>
                    <div id="question16" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">
                            @lang('messages.faq.3.5.ans')
                        </div>
                    </div>
                </div>
                <div class="panel panel-default ">
                    <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse"
                         data-parent="#faqAccordion" data-target="#question17">
                        <h4 class="panel-title">
                            <a class="accordion-toggle question-toggle collapsed" data-toggle="collapse"
                               data-parent="#faqAccordion1" href="#question17">

                                @lang('messages.faq.3.6.ques')
                            </a>
                        </h4>
                    </div>
                    <div id="question17" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">
                            @lang('messages.faq.3.6.ans')
                        </div>
                    </div>
                </div>
            </div>
            <!--/panel-group-->
            <h3>4. @lang('messages.faq.4.title')</h3>
            <div class="panel-group" id="faqAccordion4">
                <div class="panel panel-default accordion-toggle question-toggle collapsed" data-toggle="collapse"
                     data-parent="#faqAccordion" data-target="#question18">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle question-toggle collapsed" data-toggle="collapse"
                               data-parent="#faqAccordion1" href="#question18">

                                @lang('messages.faq.4.1.ques')
                            </a>
                        </h4>
                    </div>
                    <div id="question18" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">
                            @lang('messages.faq.4.1.ans')
                        </div>
                    </div>
                </div>
                <div class="panel panel-default ">
                    <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse"
                         data-parent="#faqAccordion" data-target="#question19">
                        <h4 class="panel-title">
                            <a class="accordion-toggle question-toggle collapsed" data-toggle="collapse"
                               data-parent="#faqAccordion1" href="#question19">

                                @lang('messages.faq.4.2.ques')
                            </a>
                        </h4>
                    </div>
                    <div id="question19" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">
                            @lang('messages.faq.4.2.ans')
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/panel-group-->
    </div>

    @include('layouts.partials.default.contact')

@endsection

@section('banner')
    <div class="row" style="margin: 0">
        <div class="col-md-12 customer" style="background-image: url({{ asset("/img/customer-bg.png") }})">
        </div>
    </div>

    <div class="introduce customer">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="title">@lang('messages.banner.customer_title')</div>

                    <p>@lang('messages.banner.customer_desc')</p>

                    <a href="#contact" class="btn btn-primary">
                        @lang('messages.banner.customer_button')
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
