
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', '') }}</title>

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>

    <!-- Fonts -->
{{--    <link rel="dns-prefetch" href="//fonts.gstatic.com">--}}
{{--    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">--}}

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body class="skin-blue">
<div class="wrapper">
    @include('layouts.partials.header')
    @include('layouts.partials.sidebar')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ @$pageTitle  }}
                <small>{{ @$pageDescription }}</small>
            </h1>
            <!-- You can dynamically generate breadcrumbs here -->
            @if (!empty($breadcrumb))
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> </a></li>
                    <li class="active"></li>
                </ol>
            @endif
        </section>
        <div id="app">

            <!-- Main content -->
            <section class="content">

                <!-- Your Page Content Here -->
                @yield('content')
            </section><!-- /.content -->
        </div>
    </div>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>

@include('layouts.partials.footer')

</body>
</html>
