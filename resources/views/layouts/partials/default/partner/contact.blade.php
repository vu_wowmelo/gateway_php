<div class="section gray" id="contact">
    <div class="container">
        <div class="title">
            @lang('messages.contact.title')
        </div>
        <div class="description">
            @lang('messages.contact.desc')
        </div>
        <a class="btn btn-primary btn-contact">
            <svg width="15" height="24" viewBox="0 0 15 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M12.75 0C13.3594 0 13.875 0.234375 14.3438 0.65625C14.7656 1.125 15 1.64062 15 2.25V21.75C15 22.4062 14.7656 22.9219 14.3438 23.3438C13.875 23.8125 13.3594 24 12.75 24H2.25C1.59375 24 1.07812 23.8125 0.65625 23.3438C0.1875 22.9219 0 22.4062 0 21.75V2.25C0 1.64062 0.1875 1.125 0.65625 0.65625C1.07812 0.234375 1.59375 0 2.25 0H12.75ZM7.5 22.5C7.875 22.5 8.25 22.3594 8.53125 22.0781C8.8125 21.7969 9 21.4219 9 21C9 20.625 8.8125 20.25 8.53125 19.9688C8.25 19.6875 7.875 19.5 7.5 19.5C7.07812 19.5 6.70312 19.6875 6.42188 19.9688C6.14062 20.25 6 20.625 6 21C6 21.4219 6.14062 21.7969 6.42188 22.0781C6.70312 22.3594 7.07812 22.5 7.5 22.5ZM12.75 17.4375V2.8125C12.75 2.67188 12.6562 2.53125 12.5625 2.4375C12.4688 2.34375 12.3281 2.25 12.1875 2.25H2.8125C2.625 2.25 2.48438 2.34375 2.39062 2.4375C2.29688 2.53125 2.25 2.67188 2.25 2.8125V17.4375C2.25 17.625 2.29688 17.7656 2.39062 17.8594C2.48438 17.9531 2.625 18 2.8125 18H12.1875C12.3281 18 12.4688 17.9531 12.5625 17.8594C12.6562 17.7656 12.75 17.625 12.75 17.4375Z" fill="white"/>
            </svg>
            &nbsp;
            &nbsp;
            <span >@lang('messages.contact.hotline')</span>
        </a>
        <form class="contact-form" id="formContact">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 col-sm-12 pos-relative">
                    <input  name="firstname"  class="input-text your-name" id="your_firstname" placeholder="@lang('messages.contact.firstname')" value="">
                </div>
                <div class="col-md-6 col-md-offset-3 col-sm-12 pos-relative">
                    <input  name="lastname"  class="input-text your-name" id="your_lastname" placeholder="@lang('messages.contact.lastname')" value="">
                </div>
                <div class="col-md-6 col-md-offset-3 col-sm-12 pos-relative">
                    <input  name="email"  class="input-text your-name" id="your_email" placeholder="@lang('messages.contact.email')" value="">
                </div>
                <div class="col-md-6 col-md-offset-3 col-sm-12 pos-relative">
                    <input  name="phone"  class="input-text your-name" id="your_phone" placeholder="@lang('messages.contact.phone')" value="">
                </div>
                <div class="col-md-6 col-md-offset-3 col-sm-12 pos-relative">
                    <input  name="website"  class="input-text your-name" id="your_website" placeholder="@lang('messages.contact.website')" value="">
                </div>
                <div class="col-md-6 col-md-offset-3 col-sm-12 pos-relative">
                    <select name="average_order" class="select-box" id="your_average_order" >
                        <option value="">@lang('messages.contact.average_order.text')</option>
                        <option value="<200">@lang('messages.contact.average_order.value.1')</option>
                        <option value="200-500">@lang('messages.contact.average_order.value.2')</option>
                        <option value="500-1000">@lang('messages.contact.average_order.value.3')</option>
                        <option value="1000-2000">@lang('messages.contact.average_order.value.4')</option>
                        <option value=">2000">@lang('messages.contact.average_order.value.5')</option>
                    </select>
                </div>
                <div class="col-md-6 col-md-offset-3 col-sm-12 pos-relative">
                    <select name="annual_sales" class="select-box" id="your_annual_sales" >
                        <option value="">@lang('messages.contact.annual_sales.text')</option>
                        <option value="<0.5">@lang('messages.contact.annual_sales.value.1')</option>
                        <option value="0.5-2">@lang('messages.contact.annual_sales.value.2')</option>
                        <option value="2-10">@lang('messages.contact.annual_sales.value.3')</option>
                        <option value="10-50">@lang('messages.contact.annual_sales.value.4')</option>
                        <option value=">50">@lang('messages.contact.annual_sales.value.5')</option>
                    </select>
                </div>
                <div class="col-md-6 col-md-offset-3 col-sm-12 position-relative">
                    <div class="button-group">
                        <button type="button" id="btnContactPartner" class="btn btn-primary" >
                            @lang('messages.contact.submit')
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
