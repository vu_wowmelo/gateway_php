<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="{{ request()->is('dashboards') ? 'active' : '' }}">
                <a href="{{url('dashboards')}}">
                    <i class="fa fa-line-chart "></i> <span>Dashboard</span>
                </a>
            </li>
            <li class="{{ request()->is('orders') ? 'active' : '' }}">
                <a href="{{url('orders')}}">
                    <i class="fa fa-money"></i> <span>Chờ thanh toán</span>
                    <span class="pull-right-container">
            </span>
                </a>
            </li>
            <li class="{{ request()->is('invoices') ? 'active' : '' }}">
                <a href="{{url('invoices')}}">
                    <i class="fa fa-bar-chart"></i> <span>Lịch sử thanh toán</span>
                    <span class="pull-right-container">
            </span>
                </a>
            </li>
            <li class="{{ request()->is('supports') ? 'active' : '' }}">
                <a href="{{url('supports')}}">
                    <i class="fa fa-support"></i> <span>Hỗ trợ</span>
                </a>
            </li>
            <li class="{{ request()->is('users') ? 'active' : '' }}">
                <a href="{{url('users')}}">
                    <i class="fa fa-user"></i> <span>Thông tin cá nhân</span>
                </a>
            </li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
