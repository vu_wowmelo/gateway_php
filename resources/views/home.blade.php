@extends('layouts.app')

@section('content')

        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <i class="fa fa-edit"></i>
                        <h3 class="box-title">Danh sách đơn hàng</h3>
                    </div>
                    <div class="box-body pad table-responsive">
                        <p>Various types of buttons. Using the base class <code>.btn</code></p>
                        <table class="table table-bordered text-center">
                            <tbody><tr>
                                <th> Mã đơn hàng</th>
                                <th> Mua tại</th>
                                <th>Tổng số tiền</th>
                            </tr>
                            <tr>
                                <td>
                                    xxx
                                </td>
                                <td>
                                    xxx
                                </td>
                                <td>
                                    xxx
                                </td>

                            </tr>

                            </tbody></table>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- ./row -->
        </div>
@endsection
