@extends('layouts.default')

@section('content')

    <div class="section white">
        <div class="container">
            <div class="title">
                @lang('messages.privacy.title')
            </div>
            <div class="description">
                @lang('messages.privacy.effect_form')
            </div>
            <div class="content">
                @lang('messages.privacy.content')
            </div>
        </div>
    </div>

    @include('layouts.partials.default.contact')
@endsection

@section('banner')
    <div class="row" style="margin: 0">
        <div class="col-md-12 customer" style="background-image: url({{ asset("/img/for-merchant-bg.jpg") }})">
        </div>
    </div>
@endsection

