@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Danh sách hoá đơn</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                    <table id="table-invoices" class="table table-hover">
                        <thead>
                            <tr>
                                <th>Mã Hóa đơn</th>
                                <th>Mô tả</th>
                                <th>Phương thức thanh toán</th>
                                <th>Số tiền</th>
                                <th class="visible-lg">Ngày tạo giao dịch</th>
                                <th class="visible-lg">Trạng thái</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (!empty($invoices))
                                @foreach ($invoices as $invoice)
                                    <tr>
                                        <td>#{{$invoice->id}}</td>
                                        <td>{{$invoice->description}}</td>
                                        <td>
                                            <span class="{{$invoice->payment_method_text['class']}}">{{($invoice->payment_method_text['text'])}}</span>

                                        </td>
                                        <td>{{number_format($invoice->total,0,',','.')}}</td>
                                        <td class="visible-lg">{{$invoice->created_at}}</td>
                                        <td class="visible-lg">
                                            <span class="label {{$invoice->status_text['class']}}">{{($invoice->status_text['text'])}}</span>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    {{ $invoices->links() }}
                </div>
            </div>
            <!-- /.box -->


        </div>

    </div>@endsection
