@extends('layouts.app')

@section('content')

    <section class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-globe"></i> {{env('APP_NAME')}} #{{$invoice['id']}}
                    <small class="pull-right">Ngày: {{date('d/m/Y',strtotime($invoice['invoice_date']))}}</small>
                </h2>
            </div>
            <!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">

            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                Khách hàng
                <address>
                    <strong>{{$invoice['to']['name']}}</strong><br>
                    {{$invoice['to']['address_line1']}}<br>
                    {{$invoice['to']['address_line2']}}<br>
                    Điện thoại: {{$invoice['to']['phone']}}<br>
                    Email: {{$invoice['to']['email']}}
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <b>Số hoá đơn #{{$invoice['id']}}</b><br>
                <br>
                <b>Mã số đơn hàng:</b> {{$invoice['order_id']}}<br>
                <b>Ngày đến hạn thanh toán:</b> {{date('d/m/Y',strtotime($invoice['payment_due_date']))}}<br>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- Table row -->
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Sản phẩm</th>
                        <th>Giá</th>
                        <th>Qty</th>
                        <th>Thành tiền</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($invoice['items']))
                        @foreach($invoice['items'] as $item)
                    <tr>
                        <td>{{$item['product']}}</td>
                        <td>{{number_format($item['price'],0,',','.')}}</td>
                        <td>{{$item['qty']}}</td>
                        <td>{{number_format($item['total'],0,',','.')}}</td>
                    </tr>
                    @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <!-- accepted payments column -->
            <div class="col-md-6">
                <p class="lead"><strong>Chọn phương thức thanh toán:</strong></p>
                <form class="bs-example" action="">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <label for='r11' style='width: 350px;'>
                                        <input type='radio' id='r11' name='occupation' value='atm' required />
                                        Thẻ ngân hàng nội địa
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"></a>
                                    </label>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12 ">
                                            <div class="col-md-3 home-brand"><a class="" target="_blank"><span><img src="/img/credit/visa.png"></span></a></div>
                                            <div class="col-md-3 home-brand"><a class="" target="_blank"><span><img src="/img/credit/visa.png"></span></a></div>
                                            <div class="col-md-3 home-brand"><a class="" target="_blank"><span><img src="/img/credit/visa.png"></span></a></div>
                                            <div class="col-md-3 home-brand"><a class="" target="_blank"><span><img src="/img/credit/visa.png"></span></a></div>
                                            <div class="col-md-3 home-brand"><a class="" target="_blank"><span><img src="/img/credit/visa.png"></span></a></div>
                                            <div class="col-md-3 home-brand"><a class="" target="_blank"><span><img src="/img/credit/visa.png"></span></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class=panel-title>
                                    <label for='r12' style='width: 350px;'>
                                        <input type='radio' id='r12' name='occupation' value='visa/master' required />
                                        Thẻ ngân hàng quốc tế
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"></a>
                                    </label>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12 ">
                                            <div><a class="home-brand" target="_blank"><span><img src="/img/credit/visa.png"></span></a></div>
                                            <div><a class="home-brand" target="_blank"><span><img src="/img/credit/mastercard.png"></span></a></div>
                                            <div><a class="home-brand" target="_blank"><span><img src="/img/credit/american-express.png"></span></a></div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>

                    </div>
                </form>
            </div>
            <!-- /.col -->
            <div class="col-md-6">
                <p class="lead"><strong>Tổng tiền đến hạn thanh toán vào ngày {{date('d/m/Y',strtotime($invoice['payment_due_date']))}}</strong></p>

                <div class="table-responsive">
                    <table class="table">
                        <tbody><tr>
                            <th style="width:50%">Tổng cộng:</th>
                            <td>{{number_format($invoice['total'],0,',','.')}}</td>
                        </tr>
                        <tr>
                            <th>Thuế</th>
                            <td>---</td>
                        </tr>

                        <tr>
                            <th>Thành tiền:</th>
                            <td>{{number_format($invoice['total'],0,',','.')}}</td>
                        </tr>
                        </tbody></table>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- this row will not appear when printing -->
        <div class="row no-print">
            <div class="col-xs-12">
                <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Thanh toán
                </button>
            </div>
        </div>
    </section>

@endsection
