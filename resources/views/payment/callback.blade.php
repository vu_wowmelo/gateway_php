@extends('layouts.payment')

@section('content')

    <div class="lockscreen-wrapper">


        <div class="help-block text-center">
            <div class="swal-icon swal-icon--success">
                <span class="swal-icon--success__line swal-icon--success__line--long"></span>
                <span class="swal-icon--success__line swal-icon--success__line--tip"></span>

                <div class="swal-icon--success__ring"></div>
                <div class="swal-icon--success__hide-corners"></div>
            </div>
            <h2>Thanh toán thành công</h2>
            Cảm ơn bạn đã sử dụng WowMelo.
        </div>
        <div class="text-center">
            <a class="btn btn-block btn-primary" href="{{url('/')}}"><i class="fa fa-arrow-left pull-left"></i> Quay về trang chủ</a>
        </div>
        <div class="lockscreen-footer text-center">
            Copyright © 2018 <b><a href="/" class="text-black">WowMelo</a></b><br>
            All rights reserved
        </div>
    </div>

@endsection
