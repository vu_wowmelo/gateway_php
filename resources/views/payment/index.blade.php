@extends('layouts.payment')

@section('content')

    <section class="invoice">
        <form class="submit-form" id="formOneClickPayment">
            <input type="hidden" name="payment_token[]" value="{{$token}}">
            <input type="hidden" name="amount" value="{{$order->total*1}}">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-globe"></i> Trang thanh toán của WowMelo
                    <small class="pull-right">Đơn hàng: #{{$order->order_code}} tại {{$order->client->name}}</small>
                </h2>
            </div>
            <!-- /.col -->
        </div>

        <!-- Table row -->
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Sản phẩm</th>
                        <th>SL</th>
                        <th>Giá</th>
                        <th>Thành tiền</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($order->items as $item)
                    <tr>
                        <td>
                            <input type="hidden" name="id[]" value="{{$item->id}}">
                            {{$item->product}}
                        </td>
                        <td>{{$item->qty}}</td>
                        <td>{{number_format($item->price,0,',','.')}}</td>
                        <td>{{number_format($item->subtotal,0,',','.')}}</td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <!-- accepted payments column -->
            <div class="col-md-6">
                <p class="lead">Chọn 1 phương thức thanh toán:</p>
                <form class="bs-example" action="">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <div class="icheck-primary">
                                        <input value="domestic" checked  id='r11' type="radio" name="payment_method" />
                                        <label for="r11"><strong>Thẻ nội địa, vui lòng chọn một ngân hàng</strong></label>
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"></a>
                                    </div>


                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12 ">
                                            <select name="bank_code" class="select-bank-code" class="image-picker show-html">
                                                @foreach($domestics as $domestic)
                                                    <option data-img-src="/img/domestic/{{$domestic->code}}.png" data-img-alt="Page 2" value="{{$domestic->code}}"> {{$domestic->name}}   </option>
                                                @endforeach
                                            </select>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class=panel-title>
                                    <div class="icheck-primary">
                                        <input value="international_card"  id='r12' type="radio" name="payment_method" />
                                        <label for="r12"><strong>
                                                Thẻ thanh toán quốc tế
                                            </strong></label>
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"></a>
                                    </div>

                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12 ">
                                            <div class="col-md-2 col-xs-12 home-brand "><a class="" target="_blank"><span><img src="/img/credit/visa.png"></span></a></div>
                                            <div class="col-md-2 col-xs-6 home-brand"><a class="" target="_blank"><span><img src="/img/credit/mastercard.png"></span></a></div>
                                            <div class="col-md-2 col-xs-6 home-brand"><a class="" target="_blank"><span><img src="/img/credit/american-express.png"></span></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class=panel-title>
                                    <div class="icheck-primary">
                                        <input value="cash"  id='r13' type="radio" name="payment_method" />
                                        <label for="r13"><strong>
                                                Trả bằng tiền mặt
                                            </strong></label>
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"></a>
                                    </div>

                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12 ">
                                            <h3>Phương thức trả bằng tiền mặt.</h3>
                                            <div class="form-group">
                                                <label for="billing_contact_name">Tên người trả tiền:</label>
                                                <input readonly name="billing_contact_name"  value="{{$order->billing_contact_name}}" type="text" class="form-control" id="billing_contact_name">
                                            </div>
                                            <div class="form-group">
                                                <label for="billing_contact_phone">Số ĐT:</label>
                                                <input readonly name="billing_contact_phone"  value="{{$order->billing_contact_phone}}" type="text" class="form-control" id="billing_contact_phone">



                                            </div>

                                            <div class="form-group">
                                                <label for="address">Địa chỉ của bạn để wowmelo thu tiền:</label>
                                                <input name="billing_address"  value="{{$order->billing_address}}" type="text" class="form-control" id="address">
                                            </div>


                                            <div class="form-group">
                                                <label for="tinh_thanh_pho">Tỉnh thành phố:</label>
                                                <select  name="billing_city" class="form-control" id="tinh_thanh_pho">
                                                    @foreach($provinces as $province)
                                                        <option
                                                        @if($province->province ==$selectedProvince)
                                                            selected
                                                         @endif
                                                          data-district="{{json_encode($dhlProvinceAll[$province->province])}}" value="{{$province->province}}">{{$province->province}}</option>
                                                    @endforeach
                                                </select>


                                            </div>
                                            <div class="form-group">
                                                <label for="quan_huyen">Quận huyện :</label>
                                                <select name="billing_district" class="form-control" id="quan_huyen">
                                                    @foreach($districts as $district)
                                                        <option
                                                                @if($district->district ==$selectedDistrict)
                                                                selected
                                                                @endif
                                                                value="{{$district->district}}">{{$district->district}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>

            </div>
            <!-- /.col -->
            <div class="col-md-6">
                <p class="lead">Ngày đến hạn phải thanh toán {{$order->due_date}}</p>

                <div class="table-responsive">
                    <table class="table">
                        <tbody><tr>
                            <th style="width:50%">Tổng cộng:</th>
                            <td>{{number_format($order->total,0,',','.')}}</td>
                        </tr>
                        <tr>
                            <th>Phí (vận chuyển)</th>
                            <td>{{number_format($order->fee,0,',','.')}}</td>
                        </tr>
                        <tr>
                            <th>Giảm giá</th>
                            <td>{{number_format($order->discount,0,',','.')}}</td>
                        </tr>
                        <tr>
                            <th>Tổng cộng phải trả:</th>
                            <td>{{number_format($order->total,0,',','.')}}</td>
                        </tr>
                        </tbody></table>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- this row will not appear when printing -->
        <div class="row no-print">
            <div class="col-xs-12">
                <button  id="btnOneClickPayment" type="button" class="btn btn-primary pull-right">
                    <i class="fa fa-credit-card"></i>
                    Thanh toán
                </button>
            </div>
        </div>
        </form>
    </section>

@endsection
