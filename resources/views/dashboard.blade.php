@extends('layouts.app')

@section('content')

    @if (count($orders)==0 && count($orderHasPaid)==0)
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <p>Chúng tôi chưa có thông tin mua hàng của bạn sử dụng email {{\Illuminate\Support\Facades\Auth::user()->email}} từ đối tác của chúng tôi.</p>
            <p>Hãy cập nhật các chương mình khuyến mãi hôm nay của các đối tác chúng tôi trên hệ thống trực tuyến hoặc yêu cầu họ thông tin khi bạn mua hàng trực tiếp.</p>
        </div>


    @else
        @if(empty(\Illuminate\Support\Facades\Auth::user()->balance))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <p>Khi bạn mua sắm nhiều hơn với wowmelo, hạn mức của bạn sẽ tăng lên!.</p>
                <p>Hãy kiểm tra ngay những đề xuất từ các đối tác .</p>
            </div>

        @endif

    <div class="row">
        <div class="col-md-8">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h4 class="box-title">Danh sách đơn hàng cần trả tiền:</h4>
                </div>
                <div class="box-body">
                    <!-- the events -->

                    <table id="table-orders" class="table table-hover">
                        <thead>
                        <tr>
                            <th>Mã đơn hàng</th>
                            <th>Mua tại</th>
                            <th>Số tiền</th>
                            <th class="visible-lg">Ngày đến hạn</th>
                        </tr>
                        </thead>
                        <tbody>
                        @unless (count($orders))
                            <tr>
                                <td colspan="4"> <p>Bạn chưa có đơn hàng nào cần trả tiền.</p></td>
                            </tr>

                        @endunless

                        @if(!empty($orders))
                                @foreach($orders as $order)

                                    <tr class="@if($order->late)  text-red @endif" >

                                        <td>
                                            #{{$order->order_code_text}}
                                        </td>
                                        <td>
                                            {{$order->client->name}}

                                        </td>
                                        <td>
                                            {{number_format($order->total,0,',','.')}}

                                        </td>
                                        <td>
                                            {{$order->due_date}}

                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                    @if (count($orders))
                    <a href="{{url('/orders')}}" id="btnPaymentAll" class="btn btn-info btn-block">Thanh toán ngay</a>
                    @endif
                </div>
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h4 class="box-title">Lịch sử thanh toán:</h4>
                    </div>
                    <div class="box-body">
                        <!-- the events -->


                        <table  class="table table-hover">
                            <thead>
                            <tr>
                                <th>Mã đơn hàng</th>
                                <th>Mua tại</th>
                                <th>Số tiền</th>
                                <th class="visible-lg">Ngày đến hạn</th>
                            </tr>
                            </thead>
                            <tbody>
                            @unless (count($orderHasPaid))
                                <tr>
                                    <td colspan="4"> <p>Bạn chưa có lịch sử thanh toán nào.</p></td>
                                </tr>

                            @endunless

                            @if(!empty($orderHasPaid))
                                @foreach($orderHasPaid as $order)
                                    <tr data-toggle="collapse" class="" data-target=".order{{$order->id}}">
                                        <td class="text-bold">#{{$order->order_code_text}}</td>
                                        <td class="text-bold">{{$order->client->name}}</td>
                                        <td>{{number_format($order->total,0,',','.')}}</td>
                                        <td  class="visible-lg">{{$order->due_date}}</td>

                                    </tr>
                                    @foreach($order->items as $item)
                                        <tr data-subtotal="{{$item->subtotal}}" data-id="{{$item->id}}" class="collapse  in order{{$order->id}}">
                                            <td colspan="2" class="text-center">{{$item->product}}. Số lượng: {{$item->qty}}</td>
                                            <td>{{number_format($item->subtotal,0,',','.')}}</td>
                                            <td  class="visible-lg">{{$order->due_date}}</td>
                                        </tr>
                                    @endforeach
                                @endforeach
                            @endif

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
            <div class="col-md-4">
                <!-- Info Boxes Style 2 -->
                <div class="info-box bg-aqua">
                    <span class="info-box-icon"><i class="fa fa-line-chart "></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">SỐ TIỀN CẦN PHẢI TRẢ</span>
                        <span class="info-box-number">{{number_format($totalPending,0,',','.')}}</span>

                        <div class="progress">
                            <div class="progress-bar" style="width: 100%"></div>
                        </div>
                        <span class="progress-description">
                        Vui lòng thanh toán
                  </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
                <div class="info-box bg-red">
                    <span class="info-box-icon"><i class="fa fa-calendar"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Số tiền trễ hạn</span>
                        <span class="info-box-number">{{number_format($totalLate,0,',','.')}}</span>

                        <div class="progress">
                            <div class="progress-bar" style="width: 20%"></div>
                        </div>
                        <span class="progress-description">
                    Chưa thanh toán sau 14 ngày mua hàng
                  </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>



            </div>
        </div>
    </div>
    @endif
@endsection
