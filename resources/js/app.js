
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
$(".select-bank-code").imagepicker()

$('#r11').on('click', function(){
    $(this).parent().find('a').trigger('click')
})
$('#r13').on('click', function(){
    $(this).parent().find('a').trigger('click')
})




$('#selectPaymentMethod').on('change', function() {

    if(this.value =='domestic'){
        $('#selectBankCode').parent().removeClass('hidden');
    }else{
        $('#selectBankCode').parent().addClass('hidden');
    }


    if(this.value =='cash'){
        $('#address-table').removeClass('hidden');
    }else{
        $('#address-table').addClass('hidden');
    }


});

$('#r12').on('click', function(){
    $(this).parent().find('a').trigger('click')
})

$('#table-orders tbody').on( 'click', '.checkbox_order', function () {
    calculate()

} );
calculate()
function calculate(){
    $subTotal = 0;
    $('.checkbox_order').each(function() {
        if ($(this).is(":checked"))
        {
            $subTotal+=$(this).data('total')*1;
        }
    });
    $('#subTotalAmount').number($subTotal,0,',','.');
    $('#totalAmount').number($subTotal,0,',','.');


}

$("#btnPayment").click(function(){
    $('.spinner').removeClass('hidden');
    $('#app').addClass('hidden');

    var data = new FormData($("#formOrder")[0]);
    var amount =0;
    $('.checkbox_order').each(function() {
        if ($(this).is(":checked"))
        {
            amount+= $(this).data('total')*1;
            data.append('payment_token[]',$(this).data('payment_token'));
        }
    });
    data.append('amount',amount);


    $.ajax({
        url:'/payments',
        data: data,
        dataType:'json',
        async:false,
        type:'post',
        processData: false,
        contentType: false,
        success:function(response){
            $('.spinner').addClass('hidden');
            $('#app').removeClass('hidden');
            if(response.payment_method == 'cash') {
              //  window.location.assign(response.checkoutUrl)

            }
            if(response.payment_method == 'international_card'){
                if( typeof response.errorCode =='undefined'){
                 //   window.location.assign(response.checkoutUrl)

                }else{
                    $('.spinner').addClass('hidden');
                    $('#app').removeClass('hidden');

                    swal(
                        {
                            title: "Thông báo!",
                            text: response.errorDescription,
                            icon: "warning"
                        }
                    )
                }
            }
            if(response.payment_method == 'domestic'){
                if(response.error_code =="00"){
                    window.location.assign(response.checkout_url)

                }else{
                    $('.spinner').addClass('hidden');
                    $('#app').removeClass('hidden');

                    swal(
                        {
                            title: "Thông báo!",
                            text: response.error_message,
                            icon: "warning"
                        }
                    )
                }

            }



        },
        error:function(response){
            $('.spinner').addClass('hidden');
            $('#app').removeClass('hidden');

            if(response.status == 422){
                swal(
                    {
                        title: "Thông báo!",
                        text: response.responseJSON.message,
                        icon: "warning"
                    }
                )
                $.each( response.responseJSON.errors, function( i, val ) {
                    $("form#formListingCreate ."+i+"-error").text(val[0]);
                });

            }

        },
    });

});

$("#btnSubscribe").click(function(){

    console.log('subscription')
    var data = new FormData();
    data.append('email', $('#emailSubscribe').val());

    $.ajax({
        url:'/subscriptions',
        data: data,
        dataType:'json',
        async:false,
        type:'post',
        processData: false,
        contentType: false,
        success:function(response){
            $('.spinner').addClass('hidden');
            $('#app').removeClass('hidden');
            swal(
                {
                    title: "Cảm ơn bạn đã đăng ký nhận tin từ WowMelo!",
                    text: "Thông tin của bạn đã được chuyển đến bộ phận hỗ trợ của WowMelo.",
                    icon: "success"
                }
            )

        },
        error:function(response){
            $('.spinner').addClass('hidden');
            $('#app').removeClass('hidden');

            if(response.status == 422){
                let string = "";
                $.each( response.responseJSON.errors, function( i, val ) {
                    string +=val[0]+'\n';
                });
                swal(
                    {
                        title: "Thông báo!",
                        text: string,
                        icon: "warning"
                    }
                )
            }else{
                swal(
                    {
                        title: "Thông báo!",
                        text: " Có lỗi xảy ra chúng thôi không thể đăng ký trong lúc này",
                        icon: "error"
                    }
                )
            }

        },
    });


});
$('#tinh_thanh_pho').on('change', function() {

    var districts = ($(this).children("option:selected").data('district'));

    $('#quan_huyen').empty();
    $.each(districts, function(key,value) {
        console.log(value)
        $('#quan_huyen').append($("<option></option>")
            .attr("value", value).text(value));
    });

});



$("#btnContact").click(function(){
    $('.spinner').removeClass('hidden');
    var data = new FormData($("#formContact")[0]);

    $.ajax({
        url:'/contacts',
        data: data,
        dataType:'json',
        async:false,
        type:'post',
        processData: false,
        contentType: false,
        success:function(response){
            console.log(response)
            $('.spinner').addClass('hidden');
            $('#app').removeClass('hidden');
            fbq('track', 'Contact');

            swal(
                {
                    title: response.title,
                    text: response.text,
                    icon: "success"
                }
            )

        },
        error:function(response){
            $('.spinner').addClass('hidden');
            $('#app').removeClass('hidden');

            console.log(response);
            if(response.status == 422){
                let string = "";
                $.each( response.responseJSON.errors, function( i, val ) {
                    string +=val[0]+'\n';
                });
                swal(
                    {
                        title: response.responseJSON.message,
                        text: string,
                        icon: "warning"
                    }
                )
            }else{
                swal(
                    {
                        title: "Thông báo!",
                        text: " Có lỗi xảy ra chúng thôi không thể gửi tin nhắn trong lúc này",
                        icon: "warning"
                    }
                )
            }

        },
    });


});

$("#btnContactPartner").click(function(){
    $('.spinner').removeClass('hidden');
    var data = new FormData($("#formContact")[0]);

    $.ajax({
        url:'/partner-contacts',
        data: data,
        dataType:'json',
        async:false,
        type:'post',
        processData: false,
        contentType: false,
        success:function(response){
            console.log(response)
            $('.spinner').addClass('hidden');
            $('#app').removeClass('hidden');
            fbq('track', 'Contact');

            swal(
                {
                    title: response.title,
                    text: response.text,
                    icon: "success"
                }
            )

        },
        error:function(response){
            $('.spinner').addClass('hidden');
            $('#app').removeClass('hidden');

            console.log(response);
            if(response.status == 422){
                let string = "";
                $.each( response.responseJSON.errors, function( i, val ) {
                    string +=val[0]+'\n';
                });
                swal(
                    {
                        title: response.responseJSON.message,
                        text: string,
                        icon: "warning"
                    }
                )
            }else{
                swal(
                    {
                        title: "Thông báo!",
                        text: " Có lỗi xảy ra chúng thôi không thể gửi tin nhắn trong lúc này",
                        icon: "warning"
                    }
                )
            }

        },
    });


});


$("#btnOneClickPayment").click(function(){

    $('.spinner').removeClass('hidden');
    $('#app').addClass('hidden');
    var data = new FormData($("#formOneClickPayment")[0]);

    $.ajax({
        url:'/payments',
        data: data,
        dataType:'json',
        async:false,
        type:'post',
        processData: false,
        contentType: false,
        success:function(response){
            $('.spinner').addClass('hidden');
            $('#app').removeClass('hidden');
            if(response.payment_method == 'cash') {
                window.location.assign(response.checkoutUrl)

            }

            if(response.payment_method == 'international_card'){
                if( typeof response.errorCode =='undefined'){
                     window.location.assign(response.checkoutUrl)

                }else{

                    swal(
                        {
                            title: "Thông báo!",
                            text: response.errorDescription,
                            icon: "warning"
                        }
                    )
                }
            }
            if(response.payment_method == 'domestic'){
                if(response.error_code =="00"){
                     window.location.assign(response.checkout_url)

                }else{
                    swal(
                        {
                            title: "Thông báo!",
                            text: response.error_message,
                            icon: "warning"
                        }
                    )
                }

            }



        },
        error:function(response){
            $('.spinner').addClass('hidden');
            $('#app').removeClass('hidden');

            if(response.status == 422){
                swal(
                    {
                        title: "Thông báo!",
                        text: response.responseJSON.message,
                        icon: "warning"
                    }
                )
                $.each( response.responseJSON.errors, function( i, val ) {
                    $("form#formListingCreate ."+i+"-error").text(val[0]);
                });

            }

        },
    });
});

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */


// const files = require.context('./', true, /\.vue$/i)

// files.keys().map(key => {
//     return Vue.component(_.last(key.split('/')).split('.')[0], files(key))
// })

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

