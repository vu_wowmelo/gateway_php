<?php

return [
    'banner'            => [
        'customer_button'       => 'Liên hệ',
        'customer_desc'         => 'WowMelo cung cấp loại hình dịch vụ hỗ trợ mua sắm dành riêng cho các Khách hàng thân thiết nhất của Đối tác bán hàng của WowMelo. Chúng tôi mang đến một trải nghiệm mua sắm mới gọn nhẹ bằng cách loại bỏ quy trình thanh toán truyền thống.<br/><br/>Dịch vụ Hỗ trợ mua sắm của chúng tôi cho phép bạn đặt hàng mọi lúc, mọi nơi - khi ở trên xe buýt, lúc đang  ăn sáng hay trước khi đi ngủ - với vài thao tác đơn giản. Bạn chỉ cần thanh toán cho đơn hàng của mình bất cứ lúc nào có thời gian rỗi trong vòng 14 ngày kể từ  ngày có xác nhận đơn hàng.',
        'customer_title'        => 'Dịch vụ Hỗ trợ mua sắm của WowMelo hoạt động như thế nào?',
        'merchant'              => 'Dành cho Đối tác',
        'merchant_benfit'       => 'Tìm hiều về lợi ích cho Đối tác',
        'merchant_button'       => 'Tới khu vực dành cho Đối tác',
        'merchant_login'        => 'Liên hệ với phòng Kinh Doanh',
        'partner_button'        => 'Liên hệ',
        'partner_desc'          => 'Khách hàng hài lòng thì bạn cũng hài lòng. Dịch vụ tốt là tiền đề của hiệu quả kinh doanh tốt. Hãy cùng nhau biến mua sắm thành niềm vui và cùng thay đổi tư duy về ngành bán lẻ.',
        'partner_title'         => 'Bạn mong muốn thay đổi trải nghiệm mua sắm cho Khách hàng cùng WowMelo?',
        'shopper'               => 'Mua trước trả sau',
        'shopper_benfit'        => 'Tại các cửa hàng yêu thích của bạn, chỉ cần chọn WowMelo khi thanh toán.',
        'shopper_login'         => 'Nhận tín dụng nhanh để mua sắm ngay cả khi không có thẻ tín dụng, với tùy chọn thanh toán 14 hoặc 30 ngày sau đó.',
        'shopper_button'        => 'Tới khu vực dành cho Khách hàng',
        'customer_service'      => 'Wowmelo<br/>Mua hôm nay - Trả sau 14 ngày',
        'customer_service_desc' => 'Chúng tôi có thể giúp gì được cho bạn?',
    ],
    'common'            => [
        'see_more' => 'Xem thêm',
    ],
    'contact'           => [
        'desc' => 'Vui lòng để lại thông tin liên hệ, chúng tôi sẽ liên lạc với bạn sớm.',
        'email' => 'Địa chỉ email',
        'hotline' => '090 9011 869',
        'message' => 'Nội dung tin nhắn',
        'name' => 'Tên',
        'firstname' => 'Tên',
        'lastname' => 'Họ',
        'phone' => 'Số điện thoại',
        'website' => 'Website URL',
        'average_order' => [
            'text' => 'Giá trị trung bình mỗi đơn hàng',
            'value' => [
                '1' => '< 200$',
                '2' => '200$ - 500$',
                '3' => '500$ - 1000$',
                '4' => '1000$ - 2000$',
                '5' => '> 2000$',
            ]
        ],
        'annual_sales' => [
            'text' => 'Doanh thu hằng năm qua kênh Website',
            'value' => [
                '1' => '< 0.5M',
                '2' => '0.5M - 2M',
                '3' => '2M - 10M',
                '4' => '10M - 50M',
                '5' => '> 50M',
            ]
        ],
        'submit' => 'Gửi',
        'title' => 'Liên hệ với chúng tôi',
    ],
    'faq'               => [
        '1' => [
            '1' => [
                'ans' => <<<'TEXT'
<p>Quý khách không cần phải đăng ký để sử dụng dịch vụ – WowMelo sẽ tự động hiển thị tại “Bước thanh toán” cho những Khách hàng được duyệt sử dụng dịch vụ “Trả sau”. Trong trường hợp Quý khách chưa được duyệt sử dụng dịch vụ “Trả sau”, Quý khách có thể đăng ký và cung cấp thông tin đăng nhập cho các trang thương mại điện tử khác (ví dụ: Lazada, Tiki…) để WowMelo có thể tiến hành xét duyệt lịch sử mua sắm của Quý khách.</p>
TEXT
                ,
                'ques' => 'Làm thế nào để đăng ký tài khoản với WowMelo?',
            ],
            '2' => [
                'ans' => <<<'TEXT'
<p>Quý khách có thể thử một trong những phương pháp sau: </p>
                <ul>
                <li>Đảm bảo thanh toán đúng hạn mỗi khi sử dụng dịch vụ WowMelo;</li>
                <li>Giảm giá trị đơn hàng nếu có thể;</li>
                <li>Mua hàng thường xuyên với các Đối tác bán hàng của WowMelo, nhớ đăng nhập vào tài khoản cá nhân của Quý khách để WowMelo nhận diện;</li>
                <li>Sử dụng thông tin cá nhân mua hàng đồng nhất với tất cả các Đối tác bán hàng để WowMelo có thể dễ dàng nhận diện Quý khách.</li>
                </ul>
TEXT
                ,
                'ques' => 'Làm thế nào để tăng cơ hội được duyệt sử dụng dịch vụ Hỗ trợ mua sắm?',
            ],
            '3' => [
                'ans' => '<p>Với WowMelo, Quý khách được phép thanh toán cho đơn hàng của mình trong vòng tối đa 14 ngày kể từ khi có xác nhận đơn hàng (tùy theo Đối tác bán hàng, khung thời gian này có thể ngắn hơn). Chúng tôi sẽ gửi thư hướng dẫn thanh toán ngay sau khi đơn hàng của Quý khách được xác nhận. Để tiến hành thanh toán, Quý khách chỉ cần nhấp vào đường link trong email và làm theo hướng dẫn trên màn hình. Quý khách cũng có thể đăng nhập vào tài khoản cá nhân trên <a href="www.wowmelo.com">www.wowmelo.com</a> để thanh toán.',
                'ques' => 'Cơ chế hoạt động của dịch vụ Hỗ trợ mua sắm như thế nào?',
            ],
            '4' => [
                'ans' => '<p>Khi đã được duyệt để sử dụng dịch vụ Hỗ trợ mua sắm của WowMelo, Quý khách chỉ cần cung cấp số CMND/CCCD để hoàn tất việc đặt hàng. Chúng tôi sẽ tạo tài khoản WowMelo cho Quý khách với địa chỉ email mà Quý khách đăng nhập tại trang web của Đối tác bán hàng mà Quý khách đặt mua hàng, và tất cả các thông tin liên quan tới đơn hàng sẽ được gửi tới địa chỉ email này. Do vậy Quý khách vui lòng đảm bảo thông tin địa chỉ email đăng ký trên trang web của Đối tác bán hàng là chính xác.',
                'ques' => 'Tôi cần cung cấp thông tin gì để sử dụng dịch vụ WowMelo?',
            ],
            '5' => [
                'ans' => <<<'TEXT'
<ul>
<li>Quý khách có thể đăng ký trên trang web của chúng tôi và cung cấp thông tin đăng nhập vào các trang thương mại điện tử của Quý khách (ví dụ: Lazada, Tiki…) qua biểu mẫu Liên hệ (bảo mật) dưới cuối trang này.</li>
<li>Chúng tôi sẽ xét duyệt lịch sử mua sắm của Quý khách trong vòng 24 giờ. Quý khách vui lòng thay đổi mật khẩu ngay sau khi nhận được thông báo kết thúc quá trình xét duyệt.</li>
<li>Quý khách sẽ nhận được email thông báo kết quả ngay khi việc xét duyệt kết thúc. Chúc Quý khách mua sắm vui vẻ với dịch vụ “Trả sau”.</li>
</ul>
TEXT
                ,
                'ques' => 'Tôi chưa từng mua sắm trên trang web của Đối tác bán hàng của WowMelo. Tôi có được phép sử dụng dịch vụ không?',
            ],
            'title' => 'Về Dịch vụ Hỗ trợ Mua sắm của WowMelo',
        ],
        '2' => [
            '1' => [
                'ans' => '<p>Hóa đơn sẽ được gửi tới Quý khách qua email ngay khi Đối tác bán hàng mà Quý khách đặt mua hàng xác nhận đơn hàng. Trên hóa đơn này sẽ ghi rõ các bước tiếp theo Quý khách cần làm, hạn thanh toán và hướng dẫn thanh toán. Tuy nhiên, Quý khách không cần thanh toán trước hạn yêu cầu.',
                'ques' => 'Khi nào thì tôi nhận được hóa đơn and hóa đơn sẽ được gửi tới như thế nào?',
            ],
            '2' => [
                'ans' => <<<'TEXT'
<p>Quý khách có thể thanh toán bất cứ lúc nào, tuy nhiên vui lòng thanh toán không muộn hơn hạn thanh toán được yêu cầu. Quý khách chỉ cần nhấp và đường link trong email của WowMelo gửi tới để tiến hành thanh toán.</p>
                <p>Hoặc Quý khách có thể đăng nhập trực tiếp vào tài khoản WowMelo mà chúng tôi đã tạo cho Quý khách từ lần sử dụng dịch vụ WowMelo lần đầu tiên để tiến hành thanh toán.</p>
                <p>Quý khách cần đảm bảo việc thanh toán đúng hạn. Tất cả các khoản thanh toán quá hạn 14 ngày hoặc hạn khác nếu có thỏa thuận trước sẽ gây ảnh hưởng tới việc Quý khách có được tiếp tục sử dụng dịch vụ của WowMelo trong tương lai.</p>
TEXT
                ,
                'ques' => 'Tôi thanh toán như thế nào?',
            ],
            '3' => [
                'ans' => <<<'TEXT'
<p>Quý khách có thể thanh toán bằng thẻ debit/credit, thẻ ATM nội địa, chuyển khoản ngân hàng và dịch vụ thu hộ tiền mặt của DHL.</p>
                <ul><li>Thanh toán qua thẻ ATM nội địa và thẻ debit/credit sẽ được xử lý qua dịch vụ Alepay của Nganluong.vn. Tất cả các giao dịch sẽ được xử lý bảo mật và được bảo vệ bởi công nghệ bảo mật tiên tiến nhất.</li>
                <li>Với dịch vụ thu hộ tiền mặt của DHL, Quý khách sẽ nhận thư giấy yêu cầu thanh toán trực tiếp từ DHL sau khi có xác nhận đơn hàng. Quý khách vui lòng thanh toán tiền mặt trực tiếp cho nhân viên DHL ngay sau khi nhận thư. Với mỗi một thư yêu cầu thanh toán, trong trường hợp Quý khách không nhận thư hoặc thanh toán trong lần đầu tiên DHL giao hàng, thì nhân viên DHL sẽ chỉ giao thêm 2 lần nữa trước khi gửi thông báo Quý khách không thanh toán cho WowMelo. Quý khách sẽ phải lựa chọn hình thức thanh toán khác để thanh toán cho đơn hàng của mình.</li></ul>
TEXT
                ,
                'ques' => 'Tôi có thể lựa chọn những hình thức thanh toán nào?',
            ],
            '4' => [
                'ans' => <<<'TEXT'
<p>Quý khách có thể thanh toán trong vòng 14 ngày kể từ ngày có xác nhận đơn hàng. Để giúp Quý khách nhớ thanh toán đúng hạn, chúng tôi sẽ gửi thư nhắc thanh toán sau khi có xác nhận đơn hàng.</p>
                <p>Quý khách cũng có thể đăng nhập vào tài khoản WowMelo của mình để kiểm tra thông tin này bất cứ lúc nào.</p>
TEXT
                ,
                'ques' => 'Tôi quên hạn thanh toán.',
            ],
            '5' => [
                'ans' => '<p>Quý khách sẽ không phải thanh toán nếu chưa nhận được đơn hàng của mình. Vui lòng thông báo về việc chưa nhận được hàng với Đối tác bán hàng mà Quý khách đặt mua hàng để được cập nhật thêm thông tin, đồng thời thông báo cho WowMelo về sự chậm trễ này. WowMelo sẽ cố gắng hỗ trợ Quý khách một cách tốt nhất trong thời gian này (tham khảo thêm Chính sách Bảo vệ Khách Hàng).</p>',
                'ques' => 'Tôi nhận được hóa đơn thanh toán, nhưng chưa nhận được đơn hàng của mình.',
            ],
            '6' => [
                'ans' => '<p>Thường chúng tôi sẽ gửi email có thông tin hóa đơn và hướng dẫn thanh toán ngay khi có xác nhận đơn hàng. Tuy nhiên Quý khách cũng có thể đăng nhập vào tài khoản WowMelo của mình để kiểm tra các thông tin này.</p>',
                'ques' => 'Tôi chưa nhận được email có thông tin hóa đơn và hướng dẫn thanh toán.',
            ],
            '7' => [
                'ans' => '<p>Ngay khi chúng tôi nhận được khoản tiền thanh toán của Quý khách, chúng tôi sẽ gửi thư xác nhận tới email Quý khách sử dụng để đặt hàng. Nếu Quý khách không nhận được thư xác nhận này, vui lòng đăng nhập vào tài khoản WowMelo cá nhân để kiểm tra tình trạng thanh toán.</p>',
                'ques' => 'WowMelo đã nhận được khoản thanh toán của tôi chưa?',
            ],
            '8' => [
                'ans' => '<p>Quý khách vui lòng liên hệ dịch vụ Chăm sóc Khách hàng để được hỗ trợ thêm.</p>',
                'ques' => 'Địa chỉ email của tôi không chính xác. Tôi thay đổi hạng mục thông tin này như thế nào?',
            ],
            'title' => 'Hóa đơn & thanh toán',
        ],
        '3' => [
            '1' => [
                'ans' => <<<'TEXT'
<p>Nếu Quý khách muốn hoàn trả một phần hoặc toàn bộ đơn hàng, WowMelo sẽ gửi Quý khách hóa đơn mới ngay khi yêu cầu hoàn trả được Đối tác bán hàng mà Quý khách đặt mua hàng chấp nhận. Quý khách lưu ý rằng Chính sách hoàn trả hàng của Đối tác bán hàng mà Quý khách đặt mua hàng sẽ được áp dụng.</p>
                <p>Trong trường hợp Đối tác bán hàng có sự chậm trễ trong việc xử lý yêu cầu hoàn trả hàng và hạn thanh toán sắp đến gần, Quý khách vui lòng thông báo cho dịch vụ Chăm sóc Khách hàng của WowMelo để được rời hạn thanh toán.</p>
                <p>Lưu ý: nếu Quý khách vừa mới thanh toán cho WowMelo, xin vui lòng chờ 72 tiếng (không tính ngày lễ, thứ bảy, Chủ Nhật) để yêu cầu hoàn trả được tiếp nhận và xử lý.</p>
TEXT
                ,
                'ques' => 'Tôi muốn hoàn trả đơn hàng. Hóa đơn của tôi với WowMelo sẽ được xử lý như thế nào?',
            ],
            '2' => [
                'ans' => '<p>Nếu Quý khách muốn hủy đơn hàng, xin vui lòng liên hệ trực tiếp với Đối tác bán hàng mà Quý khách đặt mua hàng. Việc hủy đơn hàng sẽ áp dụng theo chính sách của Đối tác bán hàng.</p>',
                'ques' => 'Tôi muốn hủy đơn hàng.',
            ],
            '3' => [
                'ans' => '<p>Quý khách vui lòng liên hệ trực tiếp với Đối tác bán hàng mà Quý khách đặt mua hàng để được hỗ trợ.</p>',
                'ques' => 'Đối tác bán hàng đã nhận được hàng hóa cần hoàn trả của tôi chưa?',
            ],
            '4' => [
                'ans' => '<p>Đối tác bán hàng sẽ trực tiếp cung cấp các thông tin cần thiết cho việc hoàn trả cho Quý khách.</p>',
                'ques' => 'Tôi gửi hàng hóa cần hoàn trả tới đâu?',
            ],
            '5' => [
                'ans' => '<p>Thời gian được phép hoàn trả sẽ tùy theo Đối tác bán hàng mà Quý khách đặt mua hàng. Vui lòng liên hệ với Đối tác bán hàng hoặc tham khảo trên website của Đối tác bán hàng để tìm hiểu thêm về chính sách hoàn trả.</p>',
                'ques' => 'Thời gian được phép hoàn trả là bao lâu?',
            ],
            '6' => [
                'ans' => '<p>Nếu bạn đã thanh toán cho WowMelo, thì Đối tác bán hàng mà Quý khách đặt mua hàng sẽ chịu trách nhiệm hoàn trả khoản thanh toán phù hợp bằng các phương tiện thanh toán mà hai bên thỏa thuận.</p>',
                'ques' => 'Tôi sẽ được hoàn tiền như thế nào?',
            ],
            'title' => 'Hoàn trả đơn hàng',
        ],
        '4' => [
            '1' => [
                'ans' => '<p>Quý khách được phép nhận hàng tại địa chỉ khác với địa chỉ thanh toán đăng ký với ngân hàng. Tuy nhiên, nếu Quý khách chưa sử dụng dịch vụ WowMelo bao giờ, chúng tôi khuyên Quý khách nên sử dụng địa chỉ thanh toán đăng ký với ngân hàng để tăng cơ hội được duyệt sử dụng dịch vụ Hỗ trợ mua sắm của WowMelo.</p>',
                'ques' => 'Tôi có thể yêu cầu giao hàng tới địa chỉ khác với địa chỉ thanh toán đăng ký với ngân hàng không?',
            ],
            '2' => [
                'ans' => <<<'TEXT'
<p>Quý khách vui lòng không thanh toán cho nhân viên giao hàng của Đối tác bán hàng. Đối tác bán hàng mà Quý khách đặt mua hàng có thể sử dụng dịch vụ giao hàng khác với dịch vụ thu hộ tiền mặt của WowMelo. Dịch vụ giao hàng của Đối tác bán hàng có thể không được ủy quyền thu hộ tiền mặt cho WowMelo.</p>
                <p>Nếu Quý khách muốn thanh toán bằng tiền mặt, xin vui lòng nhấp vào đường link trong email xác nhận đơn hàng của WowMelo để đăng ký thanh toán bằng tiền mặt. Quý khách vui lòng chỉ thanh toán khi nhận được thư giấy nhắc thanh toán của DHL (dịch vụ thu hộ tiền mặt của WowMelo).</p>
TEXT
                ,
                'ques' => 'Tôi có thể thanh toán tiền mặt trực tiếp cho nhân viên giao hàng không?',
            ],
            'title' => 'Giao hàng',
        ],
        'desc' => 'Tham khảo những câu hỏi thường gặp',
        'title' => 'Câu hỏi thường gặp',
    ],
    'footer'            => [
        'about' => 'Về WowMelo',
        'about_us' => 'Về chúng tôi',
        'careers' => 'Việc làm',
        'connect' => 'Tìm hiểu thêm tại',
        'customer_service' => 'Chăm sóc khách hàng',
        'developers' => 'Lập trình viên',
        'email_address' => 'Nhập địa chỉ email của bạn',
        'fags' => 'Câu hỏi thường gặp',
        'for_merchant' => 'Dành cho đối tác',
        'login' => 'Đăng nhập',
        'merchant' => 'Đối tác',
        'merchant_support' => 'Hổ trợ đối tác',
        'platforms' => 'Platform & plugin',
        'policy' => 'Cam kết với khách hàng',
        'press' => 'Truyền thông',
        'privacy' => 'Chính sách bảo mật',
        'shopper' => 'Khách hàng',
        'subscribe' => 'Theo dõi',
        'subscribe_news' => 'Đăng ký bản tin',
        'terms' => 'Điều khoản chung',
        'update' => 'Nhận thông tin mới',
        'copyright' => 'Bản quyền thuộc về © 2018 WowMelo.',
        'company_info' => 'Công ty TNHH Thương Mại WowMelo. Số đăng ký kinh doanh: 0315447070<br/>
        Địa chỉ: 180, Nguyễn Công Trứ, P. Nguyễn Thái Bình, Quận 1, Thành phố Hồ Chí Minh, Việt Nam'
    ],
    'home'              => [
        'desc' => [
            'melo' => 'Như trái dưa hấu vậy',
            'partner' => '(cùng thêm nhiều các hãng khác trong thời gian tới...)',
            'wow' => 'Chỉ cần vài bước đơn giản',
        ],
        'melo' => [
            'all_desc' => 'Bạn cần kiểm tra thông tin tất cả các giao dịch sử dụng WowMelo của mình? Đăng nhập vào tài khoản WowMelo mà chúng tôi đã tạo cho bạn từ lần đầu tiên sử dụng dịch vụ WowMelo lần đầu tiên.',
            'all_title' => 'Tất cả cùng một chỗ',
            'free_desc' => 'Dịch vụ Hỗ trợ mua sắm của WowMelo hoàn toàn miễn phí - không lãi suất, không phụ phí phát sinh',
            'free_title' => 'Hoàn toàn miễn phí',
            'problem_desc' => 'Bạn nhận được hảng nhưng không vừa ý? Đừng lo lắng, chúng tôi đã bảo vệ bạn bằng chính sách bảo vệ của chúng tôi, nhóm của chúng tôi sẽ hỗ trợ bạn. Giữ liên lạc nhé.',
            'problem_title' => 'Không vấn đề gì',
            'shopper' => 'Sử dụng WowMelo cho của hàng',
            'findout' => 'Tìm hiểu ngay'
        ],
        'title' => [
            'melo' => 'Chúng tôi là MELO: Mua sắm thật ngọt ngào',
            'partner' => 'Danh mục đối tác',
            'wow' => 'Chúng tôi là WOW: Mua sắm thật dễ dàng',
        ],
        'wow' => [
            'browse_desc' => 'Tìm sản phẩm bạn cần gian hàng trực tuyến của Đối tác bán hàng và thêm vào giỏ hàng như bình thường. Đừng quên đăng nhập!',
            'browse_title' => 'Tìm & chọn',
            'item_desc' => 'Thảnh thơi chờ đợi thôi. Đơn hàng của bạn sẽ được giao tận nhà. Nhớ cầm theo CMND/CCCD mà bạn đăng ký khi mua hàng để nhận hàng từ bên giao hàng.',
            'item_title' => 'Nhận hàng',
            'pay_desc' => 'Bạn chỉ cần thanh toán 14 ngày sau khi có xác nhận đơn hàng. Bạn có thể thực hiện thao tác bằng cách nhấp chuột vào đường chúng tôi gửi qua email.',
            'pay_title' => 'Thanh toán 14 ngày sau',
            'select_desc' => 'Chọn WowMelo tại trang thanh toán. Dịch vụ Hỗ trợ mua sắm sẽ tự động hiển thị với các Khách hàng thân thiết của Đối tác bán hàng của WowMelo.',
            'select_title' => 'Chọn WowMelo',
        ],
    ],
    'lang'              => [
        'english' => 'Tiếng Anh',
        'vietnamese' => 'Tiếng Việt',
    ],
    'login'             => 'Đăng nhập',
    'register'          => 'Đăng ký',
    'menu'              => [
        'customer'      => 'Dịch vụ của WowMelo',
        'easy'          => 'Dịch vụ',
        'for_merchant'  => 'Đối tác',
        'for_shopper'   => 'Khách hàng',
        'shop'          => 'Đối tác',
    ],
    'partner'           => [
        '1' => [
            'desc' => 'Thủ tục chốt đơn hàng sẽ được đơn giản hóa và nhanh gọn hơn cho Khách hàng. Các Khách hàng hài lòng sẽ quay lại mua nhiều và thường xuyên hơn, tăng hiệu quả kinh doanh cho bạn.',
            'title' => 'Mua hàng nhanh gọn',
        ],
        '2' => [
            'desc' => 'Đối tác của Wowmelo sẽ được đảm bảo thanh toán 100% cho tất cả đơn hàng được Khách hàng chọn sử dụng Wowmelo, cho dù Khách hàng có chọn thanh toán cho Wowmelo như thế nào đi nữa.',
            'title' => 'An tâm tuyệt đối',
        ],
        '3' => [
            'desc' => 'Hãy đi trước thời đại bằng cách cùng tham gia vào làn sóng công nghệ mới vè trở thành nhà bán lẻ thông minh để mang tới trải nghiệm mua hàng của tiên tiến nhất tới Khách hàng của mình.',
            'title' => 'Tiên phong công nghệ mới',
        ],
        'desc' => 'Hãy cùng khám phá một số lợi ích của việc hợp tác với WowMelo',
        'title' => 'WowMelo có thể mang tới gì cho Đối tác',
    ],
    'mission'           => [
        'desc' => <<<'TEXT'
<ul>
    <li>Khách hàng của bạn trả tiền sau. Bạn được trả tiền vào ngày làm việc tiếp theo.</li>
    <li>Cung cấp cho khách hàng của bạn nhiều sức mua hơn nhờ tự do trả tiền sau khi mua hàng trực tuyến của họ. </li>
    <li>Đánh giá tín dụng là ngay lập tức. Nhiệm vụ của chúng tôi là xóa bỏ rào cản và xây dựng niềm tin vào mua sắm trực tuyến tại Việt Nam.</li>
</ul>
TEXT
        ,
        'title' => 'Tăng doanh số và khách hàng',
        'button' => 'Thử ngay'
    ],
    'different'         => [
        'title' => 'Cách thức hoạt động của WowMelo có gì khác so với các phương thức thanh toán khác?',
        '1' => [
            'title' => 'Phương thức thanh toán <span class="red">COD</span>',
            'step' => [
                '1' => [
                    'title' => 'Bước 1',
                    'desc' => 'Chọn mua hàng và chọn phương thức thanh toán là COD.'
                ],
                '2' => [
                    'title' => 'Bước 2',
                    'desc' => 'Chờ đợi nhân viên giao hàng đến.'
                ],
                '3' => [
                    'title' => 'Bước 3',
                    'desc' => 'Nhận hàng, kiểm tra hàng.'
                ],
                '4' => [
                    'title' => 'Bước 4',
                    'desc' => 'Thanh toán cho ngay sau khi nhận hàng.'
                ],
            ],
        ],
        '2' => [
            'title' => 'Phương thách thanh toán bằng <span class="green">thẻ tín dụng</span>',
            'step' => [
                '1' => [
                    'title' => 'Bước 1',
                    'desc' => 'Chọn mua hàng và phương thức thanh toán COD.'
                ],
                '2' => [
                    'title' => 'Bước 2',
                    'desc' => 'Nhập thông tin thẻ tín dụng'
                ],
                '3' => [
                    'title' => 'Bước 3',
                    'desc' => 'Nhập thông tin người mua'
                ],
                '4' => [
                    'title' => 'Bước 4',
                    'desc' => 'Xác nhận thôn tin thẻ và hoàn tất giao dịch.'
                ],
            ],
        ],
        '3' => [
            'title' => 'Phương thức thanh toán bằng <span class="blue">WowMelo</span>',
            'step' => [
                '1' => [
                    'title' => 'Tìm & chọn',
                    'desc' => 'Tìm sản phẩm bạn cần gian hàng trực tuyến của Đối tác bán hàng và thêm vào giỏ hàng như bình thường. Đừng quên đăng nhập!'
                ],
                '2' => [
                    'title' => 'Chọn WowMelo',
                    'desc' => 'Chọn WowMelo tại trang thanh toán. Dịch vụ Hỗ trợ mua sắm sẽ tự động hiển thị với các Khách hàng thân thiết của Đối tác bán hàng của WowMelo.'
                ],
                '3' => [
                    'title' => 'Nhận hàng',
                    'desc' => 'Thảnh thơi chờ đợi thôi. Đơn hàng của bạn sẽ được giao tận nhà. Nhớ cầm theo CMND/CCCD mà bạn đăng ký khi mua hàng để nhận hàng từ bên giao hàng.'
                ],
                '4' => [
                    'title' => 'Thanh toán sau 14 ngày',
                    'desc' => 'Bạn chỉ cần thanh toán 14 ngày sau khí có xác nhận đơn hàng. Bạn có thể thực hiện thao tác bằng cách nhấp chuột vào đường chúng tôi gửi qua email.'
                ],
            ],
        ],
    ],
    'advantage'         => [
        'title' => 'Ưu điểm của phương thức thanh toán bằng WowMelo:',
        'rows' => [
            '1' => [
                'cols' => [
                    '1' => '',
                    '2' => '<h3><span class="red">COD</span></h3>',
                    '3' => '<h3><span class="green">Thẻ tín dụng</span></h3>',
                    '4' => '<h3>Trả sau <span class="blue">wowmelo</span></h3>',
                ]
            ],
            '2' => [
                'cols' => [
                    '1' => 'Thông tin cần điền tại bước check-out (ngoài thông tin giao nhận hàng)',
                    '2' => 'Không',
                    '3' => 'Luôn luôn cung cấp thông tin thẻ tín dụng (số thẻ, ngày hết hạn, CVV)',
                    '4' => 'Chỉ cần số CMND,CCCD cho lần sử dụng đầu tiên',
                ]
            ],
            '3' => [
                'cols' => [
                    '1' => 'Thời hạn thanh toán',
                    '2' => 'Khi nhân viên giao hàng tới',
                    '3' => 'Tới 15 ngày sau khi có sao kể giao dịch',
                    '4' => 'Luôn luôn là 14 ngày sau ngày đặt mua hàng',
                ]
            ],
            '4' => [
                'cols' => [
                    '1' => 'Phí phạt quá hạn',
                    '2' => 'Huỷ đơn hàng',
                    '3' => 'Lãi suất 30%/năm',
                    '4' => 'Phạt cố định tối đa 50.000VND',
                ]
            ],
            '5' => [
                'cols' => [
                    '1' => 'Bảo mật thông tin',
                    '2' => 'Không',
                    '3' => 'Không, nếu trang web bán hàng không mã hoá dữ liệu thông tin khách hàng',
                    '4' => '100% bảo mật thông tin khách hàng',
                ]
            ],
            '6' => [
                'cols' => [
                    '1' => 'Ảnh hưởng tới đánh giá tín dụng cá nhân',
                    '2' => 'Không',
                    '3' => 'Có (tín dụng xấu sẽ được thông tin báo lại cho cơ quan có thẩm quyền)',
                    '4' => 'Không',
                ]
            ],
            '7' => [
                'cols' => [
                    '1' => 'Kiểm soát tài chính',
                    '2' => 'Không vì dễ dàng tiêu quá hạn mức bản thân có thể chi trả',
                    '3' => 'Không, dễ dàng tiêu quá hạn mức bản thân có thể chi trả ',
                    '4' => 'Có, hạn mức được thiết kế dựa trên thông tin lịch sử mua hàng cá nhân',
                ]
            ],
        ]
    ],
    'collaborate'       => [
        'title' => 'Lợi ích của đối tác',
        'content' => <<<'TEXT'
<ul>
<li>Đối tác không hề chịu bất kì rủi ro nào vì WowMelo sẽ thanh toán lại cho đối tác trong 1 - 2 ngày làm việc.</li>
<li>Giúp Đối tác tăng doanh số và xây dựng lòng tin trong khách hàng.</li>
<li>Tạo sự khác biệt với đối thủ và xây dựng được lòng trung thành của khách hàng.</li>
</ul>
TEXT
        ,
    ],
    'privacy'           => [
        'title' => 'Chính sách bảo mật thông tin',
        'effect_form' => '(có hiệu lực từ ngày 21/12/2018)',
        'content' => <<<'TEXT'
<p>Công ty TNHH Thương mại WowMelo (“WowMelo”,”chúng tôi”) tông trọng sự bảo mật thông tin của Khách hàng. Chính sách bảo mật thông tin này được áp dụng tại trang web <a href="www.wowmelo.com">www.wowmelo.com</a> và cho bất cứ thông tin nào được hiển thị trên trang web này. Chúng tôi sẽ miêu tả chi tiết trong chính sách này quy cách chúng tôi thu thập và sử dụng thông tin, cũng như việc cung cấp thông tin về Khách hàng truy cập website của WowMelo. Chúng tôi xin được nhấn mạnh rằng chính sách này chỉ được áp dụng với các thông tin liên quan tới website của WowMelo.</p>
<p>Bằng việc truy cập vào trang web này, Quý khách đồng ý cho phép thông tin cá nhân được sử dụng theo các mục đích được đề cập và mô tả trong chính sách này. Điều khoản sử dụng trang web <a href="www.wowmelo.com">www.wowmelo.com</a> được kết hợp và tham khảo với Chính sách bảo mật này, và được cùng hiển thị trên trang web này.</p>
<h3>Những thông tin được thu thập và lí do</h3>
Chúng tôi thu thập thông tin về Quý khách trực tiếp từ Quý khách, và tự động thu thập qua việc truy cập và sử dụng trang web của Quý khách.
<h4>Thông tin chúng tôi thu thập trực tiếp từ Quý khách</h4>
Tại thời điểm hiện tại, Quý khách được phép truy cập tất cả các phần của trang web mà không cần đăng ký trước, tuy nhiên việc này có thể thay đổi bất cứ lúc nào và chính sách này sẽ được cập nhật để phản ánh việc thay đổi nếu có. Quý khách có thể cung cấp một số thông tin cá nhân cho chúng tôi để biết thêm về các sản phẩm và dịch cụ của WowMelo – ví dụ như tên đầy đủ, địa chỉ, email và số điện thoại. Chúng tôi có thể thu thập thông tin liên lạc cá nhân và các ý kiến của Quý khách về trang web qua các phương tiện như biểu mẫu, khảo sát, cuộc thi, ý kiến người dùng hoặc các phương tiện khác.
<h4>Thông tin chúng tôi thu thập tự động</h4>
Chúng tôi có thể tự động thu thập các thông tin phi cá nhân để nhận dạng qua việc Quý khách truy cập vvaf sử dụng trang web của chúng tôi bằng cookies, web beacons, tập tin log hoặc các công nghệ khác như tên miền, loại hình trình duyệt web và hệ điều hành máy tính, các trang web mà Quý khách truy cập, các đường link Quý khác nhấp vào và hoạt động khi sử dụng Dịch vụ, địa chỉ IP, thời gian truy cập trang web và URL giới thiệu, hoặc trang web dẫn bạn truy cập tới trang web của chúng tôi, ID điện thoại di động, vị trí địa lý và thông tin ngôn ngữ, tên và thông tin model của thiết bị sử dụng. Chúng toi có thể kết hợp các thông tin này với các thông tin khác mà chúng tôi thu thập được về Quý khách hoặc do Quý khách cung cấp cho chúng tôi, bao gồm tên đăng nhập, tên đầy để và một số thông tin cá nhân khác. Vui lòng tham khảo phần “Cookies và các công nghệ khác” để biết thêm thông tin chi tiết.
<h3>Quy cách sử dụng thông tin</h3>
<p>Chúng tôi có thể chia sẻ thông tin của Quý khách như sau:</p>
<ul>
    <li>Với nhà cung cấp dịch vụ: chúng tôi có thể sử dụng các nhà cung cấp dịch vụ khác để hỗ trợ trang web và có thể sẽ cung cấp thông tin chúng tôi thu thập được về Quý khách cho bên thứ 3, nhà cung cấp dịch vụ hoặc nhà thầu dịch vụ khách với mục đích để thực hiện các chức năng tay cho chúng tôi, tuy nhiên họ không được phép sử dụng các thông tin này cho các mục đích riêng.</li>
    <li>Các thông tin nhận diện chung và phi cá nhân: chúng tôi có thể chia sẻ các thông tin chung/kết hợp hoặc phi cá nhân về người sử dụng với các bên thứ 3 với các mục đích liên quan tới marketing, quảng cáo, nghiên cứu hoặc các mục đích tương tự.</li>
</ul>
<h3>Sử dụng Cookies và các công nghệ theo dõi khác</h3>
<p>Chúng tôi và các bên Cung cấp dịch vụ thứ 3 có thể sử dụng Cookies và các công nghệ theo dõi khác để thu thập các thông tin về việc Quý khách sử dụng trang web của WowMelo và trang web liên quan khác. Chúng tôi có thể kết hợp các thông tin này với các thông tin cá nhân khác mà chúng tôi thu thập được từ Quý khách (và các bên Cung cấp dịch vụ thứ 3 cũng có thể thu thập thông tin hộ chúng tôi).</p>
<p>Tại thời điểm hiện tại, hệ thống của thúng tôi không nhận diện các trình duyệt có yêu cầu không cho phép thu thập thông tin. Quý khách có thể vô hiệu hóa một số công nghệ theo dõi như được miêu tả bên dưới:</p>
<h4>Cookies</h4>
<p>Cookies là dải mã kết hợp số và chữ mà chúng tôi thu thập từ ổ cứng máy tính của Quý khách qua trình duyệt web với mục đích lưu trữ thông tin. Một số cookies giúp chúng tôi thiết kế trang web theo hướng để Quý khách dễ dàng sử dụng, và một số khác thì giúp Quý khách đăng nhập nhanh hơn, hoặc để giúp chúng tôi kiểm soát các hoạt động của Quý khách trên trang web. Đa số các trình duyệt web đều tự động chấp nhận cookies, tuy nhiên Quý khách có thể điều chỉnh cài đặt để chặn cookies nếu Quý khách muốn. Phần “Help” (Trợ giúp) hiển thị trên thanh công cụ của trình duyệt web sẽ hướng dẫn Quý khách cách kiểm soát Cookies. Nếu Quý khách truy cập trang web của chúng tôi với tính năng Cookies bị vô hiệu hóa sẽ chỉ truy cập được một số khu vực nhất định của trang web, nhưng một số tính năng có thể sẽ không hoạt động.</p>
<h4>Hình ảnh rõ nét (Clear GIFs)</h4>
<p>Clear GIFs là các hình ảnh siêu nhỏ với mã độc nhất, có tính năng tương tự với Cookies. Tuy nhiên Cookies thường được lưu trữ ở ổ cứng của máy tính, thì clear GIFs được ẩn trên trang web. Chúng tôi có thể sử dụng clear GIFs (có tên khác là web beacons, web bugs or pixel tags) trên trang web của chúng tôi cùng với những tính năng khác để kiểm soát các hoạt động của Khách truy cập trang web, quản lý nội dung và tổng hợp số liệu thống kê về việc truy cập trang web.</p>
<h4>Bên phân tích thứ 3</h4>
<p>Chúng tôi có thể sử dụng các thiết bị và ứng dụng tự động để xét duyệt việc sử dụng trang web của chúng tôi. Chúng tôi sử dụng các công cụ này để cải tiến trang web, hiệu quả hoạt động và trải nghiệm khách hàng. Các bên phân tích thứ 3 này có thể sử dụng Cookies và các công nghệ theo dõi khác để hoàn thành nghĩa vụ cung cấp dịch vụ. Chúng tôi sẽ không chia sẻ thông tin cá nhân của Quý khách với các bên phân tích thứ 3 này.</p>
<h3>Đường link của bên thứ 3</h3>
<p>Trang web của chúng tôi có thể chứa các trang web của bên thứ ba, bao gồm đường link của các trang mạng xã hội. Chính sách này không được áp dụng cho việc truy cập và sử dụng các trang web này, thay vì vậy, chính sách bảo mật thông tin của riêng những trang web này sẽ được áp dụng. Chúng tôi sẽ không chịu trách nhiệm cho việc sử dụng thông tin của các trang web này.</p>
<h3>Bảo mật thông tin cá nhân</h3>
<p>Chúng tôi sử dụng các hình thức bảo mật để bảo vệ thông tin chúng tôi thu thập được từ việc mất, sử dụng sai mục đích, truy cập vượt quyền hạn, tiết lộ, chỉnh sửa và phá hoại thông tin. Mặc dù chúng tôi cố gắng bảo mật thông tin cá nhân của Quý khách, vui lòng lưu ý rằng không có một hình thức bảo mật thông tin có thể đảm bảo bảo mật 100%. Quý khách cần đảm bảo rằng không có ai được truy cập trái phép mật khẩu, điện thoại, máy tính bằng cách đăng xuất khỏi máy tình dùng chung, chọn sử dụng mật khẩu an toàn mà không ai có thể đoán một cách dễ dàng  và không cung cấp thông tin đăng nhập cũng như mật khẩu của Quý khách cho một ai khác. Chúng tôi sẽ không chịu trách nhiệm cho việc mất mát, trộm cắp cũng như việc xâm phận mật khẩu hoặc bất cứ hoạt động nào của tài khoản cá nhân của Quý khách qua việc sử dụng mật khẩu trái phép.</p>
<h3>Tôi có những quyền hạn gì liên quan tới việc sử dụng thông tin cá nhân của mình?</h3>
<p>Chúng tôi có thể gửi các email liên quan tới thông tin và chương trình khuyến mại cho Khách hàng của mình. Quý khách có thể loại bỏ các email này theo hướng dẫn trong email. Quý khách lưu ý rằng việc loại bỏ này có thể mất 10 ngày làm việc để xử lý. Nếu Quý khách lựa chọn không nhận email về các đề xuất cũng như các thông tin mà chúng tôi cho rằng phù hợp với Quý khách, chúng tôi vẫn có thể gửi thư liên quan tới các Dịch vụ mà Quý khách chọn hoặc nhận được từ chúng tôi.</p>
<h3>Trẻ em dưới 13 tuổi</h3>
<p>Dịch vụ của chúng tôi không được thiết kế để trẻ dưới 13 tuổi sử dụng. Nếu chúng tôi phát hiện trường hợp trẻ dưới dưới 13 tuổi cung cấp thông tin cá nhân cho chúng tôi, thì ngay lập tức các thông tin tin đó sẽ được xóa bỏ khỏi hệ thống của chúng tôi.</p>
<h3>Liên hệ chúng tôi</h3>
<p>Nếu Quý khách có bất cứ câu hỏi nào liên quan tới việc bảo mật của Dịch vụ của WowMelo, hoặc nếu Quý khách muốn khiếu nại, vui lòng gửi thư tới info@WowMelo.com.</p>
<h3>Thay đổi liên quan tới Chính sách này</h3>
<p>Chính sách này được áp dụng từ ngày có hiệu lực như nêu trên. Chúng tôi có thể thay đổi chính sách này, vì vậy vui lòng kiểm tra lại thường xuyên. Chúng tôi sẽ thông báo các thay đổi về chính sách này trên trang web của chúng tôi. Nếu việc thay đổi ảnh hưởng tới mô hình hoạt động liên quan tới các thông tin cá nhân mà chúng tôi thu thập từ trước đó từ Quý khách, chúng tôi sẽ cố gắng cung cấp thông báo tới Quý khách bằng cách nêu rõ các thay đổi trên trang web của chúng tôi.</p>

TEXT
        ,
    ],
    'terms'             => [
        'title' => 'Điều khoản Sử dụng',
        'desc' => 'CÁC ĐIỀU KHOẢN SỬ DỤNG TRANG WEB (“ĐIỀU KHOẢN”) LÀ HỢP ĐỒNG HỢP PHÁP GIỮA QUÝ KHÁCH VÀ WOWMELO TRADING LTD (“WOWMELO”). ÁC ĐIỀU KHOẢN DƯỚI ĐÂY GIẢI THÍCH RÕ QUYỀN HẠN SỬ DỤNG TRANG WEB WWW.WOWMELO.COM/ (“TRANG WEB”. BẰNG VIỆC SỬ DỤNG TRANG WEB NÀY, QUÝ KHÁCH ĐỒNG Ý VỚI TẤT CẢ ĐIỀU KHOẢN NÀY; NẾU QUÝ KHÁCH KHÔNG ĐỒNG Ý VỚI BẤT CỨ ĐIỀU KHOẢN NÀO,VUI LÒNG KHÔNG TRUY CẬP HOẶC SỬ DỤNG TRANG WEB NÀY, HOẶC BẤT CỨ THÔNG TIN NÀO ĐƯỢC HIỂN THỊ TRÊN TRANG WEB NÀY.',
        'content' => <<<'TEXT'
<h3>Thay đổi</h3>
<p>WowMelo có thể thay đổi bất cứ nội dung nào trên trang web bất cứ lúc nào. WowMelo có thể thay đổi, cập nhật, thêm hoặc loại bỏ bất cứ phần nào của Điều khoản này bằng việc thông báo về thay đổi trên trang web. Việc sử dụng trang web này sau khi WowMelo thay đổi Điều khoản Sử dụng đồng nghĩa với việc Quý khách đồng ý với các điều khoản được cập nhật; nếu Quý khách không đồng ý với bất cứ điều khoản mới được cập nhật nào, vui lòng dừng việc sử dụng trang web.</p>
<h3>Sử dụng chung</h3>
<p>Việc sử dụng trang web này đồng nghĩa với việc Quý khách thể hiện, công nhận và đồng ý rằng Quý khách trên 18 tuổi. Nếu Quý khách chưa đủ 18 tuổi, Quý khách có thể không được sử dụng trang web này bất cứ lúc nào hoặc bất cứ bằng cách nào hoặc cung cấp thông tin cho WowMelo hoặc trang web này.</p>
<p>Các nội dung được cung cấp trên trang web này được đăng ký bản bản quyền và thương hiệu bởi WowMelo hoặc các bên thứ 3 cung cấp giấy phép và nhà cung cấp nội dung (gọi chung là “bộ tài liệu”). Bộ tài liệu sẽ bao gồm cả logo, sản phẩm đồ họa, video, hình ảnh, phần mềm và các nội dung khác.</p>
<p>Tùy theo Điều khoản & điều kiện của Điều khoản sử dụng này, và tùy theo việc tuân thủ của Quý khahcs, WowMelo cho phép Quý khách quyền sử dụng có hạn, không độc quyền, không chuyển đổi và được hiển thị Bộ tài liệu cũng như sử dụng trang web này với mục đích cá nhân. Ngoài các quyền lợi vừa nêu trên, Quý khách không có các quyền nào khác liên quan tới việc sử dụng trang web này và Bộ tài liệu. Quý khách không được phép chỉnh sửa, thay đổi, sao chép, tạo bất cứ nội dụng sáng tạo nào dựa trên hoặc khai thác trang web này và Bộ tài liệu dưới bất cứ hình thức nào.</p>
<p>Nếu Quý khách vi phạm bất cứ điều khoản nào, thì quyền sử dụng cho phép phía trên sẽ được chấm dứt ngay lập tức và Quý khách sẽ được yêu cầu xóa hủy bất cứ Bộ tài liệu nào được tải về hoặc in ra từ WowMelo.</p>
<h3>Sử dụng trang web</h3>
<p>Quý khách có thể xem trang web mà không cần phải đăng ký với WowMelo.</p>
<h3>Đường link tới bên thứ 3</h3>
<p>Trang web này có thể được liên kết tới các trang web khác mà không được kiểm soát hoặc điều hành bởi WowMelo (gọi chung là “Bên thứ 3”). Một số khu vực của trang web cho phép Quý khách tương tác hoặc/và thực hiện giao dịch với các Bên thứ 3, và, nếu áp dụng được, cho phép Quý khách thay đổi cài đặt bảo mật  trong tài khoản của Quý khách với Bên thứ 3 để cho phép chia sẻ các hoạt động của Quý khách trên trang web này với tài khoản trên trang web của Bên thứ 3 và, trong một số trường hợp, Quý khách sẽ được chuyển tới trang web của Bên thứ 3 qua đường link cho dù có thể trên giao diện thì Quý khách vẫn ở trên trang web của chúng tôi. Trong bất cứ trường hợp nào, Quý khách phải công nhận và đồng ý rằng việc Quý khách sử dụng trang web của Bên thứ 3 sẽ được quản lý bởi Bên thứ 3 theo chính sách, điều khoản điều kiện và/hoặc hướng dẫn sử dụng Khách hàng của Bên thứ 3. Quý khách phải đồng ý tuân thủ theo chính sách, điều khoản điều kiện và/hoặc hướng dẫn sử dụng Khách hàng của Bên thứ 3 kể từ đây. WowMelo sẽ cung cấp đường link tới trang web của Bên thứ 3 để Quý khách có thể tiện lợi truy cập, bao gồm, không giới hạn tới, sự trung thực, chính xác, chất lượng hoặc sự hoàn thiện về mặt nội dung, dịch vụ, đường link hiển thị và/hoặc các hoạt động khác được thực hiện trên hoặc qua trang web của Bên thứ 3. QUÝ KHÁCH ĐỒNG Ý VỚI VIỆC WOWMELO SẼ KHÔNG, TRONG BẤT CỨ TRƯỜNG HỢP NÀO, CHỊU TRÁCH NHIỆM TRỰC TIẾP HOẶC GIÁN TIẾP CHO BẤT CỨ SẢN PHẨM, DỊCH CỤ, THÔNG TIN, NGUỒN THÔNG TIN VÀ/HOẶC NỘI DUNG CÓ TRÊN HOẶC QUA TRANG WEB CỦA BÊN THỨ 3 VÀ/HOẶC HOẠT ĐỘNG HOẶC TRUYỀN THÔNG CỦA BÊN THỨ 3, HOẶC CHHO BẤT CỨ MỘT TỔN THẤT NÀO LIÊN QUAN TỚI VÀ/HOẶC MẤT MÁT HOẶC HƯ TỔN DO VIỆC QUÝ KHÁCH SỬ DỤNG BẤT CỨ NỘI DUNG HOẶC HOẠT ĐỘNG KINH DOANH NÀO CỦA BÊN THỨ 3. Tất cả các thông tin về Bên thứ 3 liên quan tới sản phẩm, dịch vụ, in ấn, tổ chức của Bên thứ 3 đều không nằm trong hoặc được diễn giải là được WowMelo ủng hộ và khuyên dùng.</p>
<h3>Hình thức truyền thông điện tử</h3>
<p>Bằng việc sử dụng trang web này và việc cung cấp thông tin cá nhân, Quý khách đồng ý nhận các hình thức truyền thông điện tử, bao gồm thông báo qua kênh điện tử, từ WowMelo. Tất cả các hình thức truyền thông điện tử có thể bao gồm thông báo về cước phí, thông tin về giao dịch và các thông tin khác liên quan tới trang web này được thể hiện trên hoặc qua trang web này. Các hình thức truyền thông điện tử này là một phần của mối quan hệ giữa Quý khách và WowMelo. Quý khách đồng ý rằng các thông báo, hợp đồng/văn bản chấp thuận, văn bản cung cấp thông tin hoặc các hình thức truyền thông khác mà chúng tôi có thể gửi qua kênh điện tử là phù hợp theo các yêu cầu về mặt pháp lý, bao gồm hình thức truyền thông qua văn bản.</p>
<h3>Chính sách bảo mật của trang web</h3>
<p>Vui lòng kiểm tra Chính sách bảo mật của trang web của WowMelo trên trang web này. Chính sách này sẽ giải thích cụ thể cách chúng tôi sử dụng các thông tin mà Quý khách cung cấp qua trang web này.</p>
<h3>Các hoạt động không được phép</h3>
<p>Khi sử dụng trang web này, Quý khách đồng ý KHÔNG làm các việc sau:</p>
<ul>
    <li>Truyền tải các quảng cáo, tài liệu quảng cáo, thư rác, spam, thư dây chuyền, thông tin bán hàng đa cấp hoặc các thông tin tương tự mà không được phép hoặc quyền.</li>
    <li>Sử dụng robot hoặc các phương tiện tự động khác để truy cập trang web.</li>
    <li>Thực hiện các hành động gây quá tải cho hệ thống hạ tầng.</li>
    <li>Đăng tải các thông tin trái ngược với hình ảnh công cộng, tài sản trí tuệ hoặc danh tiếng của WowMelo.</li>
</ul>
<p>Danh sách trên chỉ nêu ra các ví dụ và không phải là danh sách đầy đủ, hoàn thiện. WowMelo được phép chấm dứt quyền truy cập tài khoản của Quý khách và khả năng đăng tải thông tin trên trang web này với bất cứ lí do nào mà không cần thông báo hoặc cho bất cứ hành động nào của Quý khách mà chúng tôi cho là không phù hợp hoặc gây ảnh hưởng tới trang web này hoặc các Khách hàng sử dụng khác của WowMelo. WowMelo có thể báo cho các cơ quan pháp lý có thẩm quyền về những hành vi bất hợp pháp, hoặc bất cứ báo cáo nào WowMelo nhận được về các hành vi bất hợp pháp. Khi được các cơ quan luật pháp yêu cầu hoặc tùy theo lựa chọn của WowMelo, chúng tôi có thể hợp tác với các cơ quan luật pháp để điều tra các trường hợp nghi là có hành vi bất hợp pháp trên trang web này hoặc trên Internet nói chung.</p>
<p>Quý khách đồng ý rằng WowMelo cùng ban giám đốc, ban điều hành, nhân viên, đại diện, nhà cung cấp giấy phép và đối tác kinh doanh của WowMelo sẽ không phải chịu trách nhiệm bất cứ bồi thường, hư hỏng, trách nhiệm/nghĩa vụ và phí tổn (bao gồm phí luật sư và phí bào chữa) liên quan tới việc hoặc trong trường hợp Quý khách trốn trách trách nhiệm/nghĩa vụ hoặc trong trường hợp Bên thứ 3 yêu cầu vì Quý khách vi phạm luật/quy định ban hành áp dụng, hoặc vi phạm bản quyền, thương hiệu hoặc các quyền lợi của các Bên thứ 3 khác.</p>
<h3>Quyền sở hữu</h3>
<p>WowMelo là thương hiệu đăng ký bản quyền tại Việt Nam. Tất cả các thương hiệu, tên hoặc logo trên trang web này là tài sản sở hữ của chủ hợp pháp liên quan.</p>
<p>Trừ khi được đính chính khác với Điều khoản sử dụng này, tất cả các thông tin hiển thị trên trang web này, bao gồm tài liệu, thiết kế trang web, nội dung, đồ họa, logo, hình ảnh, biểu tượng (icon)và các tài liệu liên quan là sản phẩm sở hữu của riêng WowMelo. Tất cả cả quyền lợi khác không nêu trong đây cũng được đảm bảo cho riêng WowMelo. Trừ các trường hợp có yêu cầu khác hoặc bị hạn chế bởi pháp luật, tất cả các trường hợp làm lại, tái sử dụng, phát tán, sửa đổi, điều chuyển, in ấn bất cứ sản phẩm bản quyền nào đều bị nghiêm cấm nếu không xin phép chủ sỡ hữu bản quyền trước.</p>
<h3>Ghi chú về tính đảm bảo chính xác</h3>
<p>Chúng tôi không chịu trách nhiệm cho các rủi ro liên quan tới việc Quý khách sử dụng trang web này. Bộ tài liệu có thể có một số thông tin chưa chính xác hoặc lỗi chính tả hoặc một số lỗi khác. WowMelo sẽ không đảm bảo sự chính xác về thời gian của những tài liệu trên trang web này. WowMelo sẽ không chịu trách nhiệm cho bất cứ lỗi hoặc thiếu sót liên quan tới Bộ tài liệu, cho dù có phải do WowMelo, các bên cấp phép, nhà cung cấp hoặc các bên khác cung cấp hay không.</p>
<p>WOWMELO, CHO BẢN THÂN VÀ CÁC BÊN CẤP PHÉP, SẼ KHÔNG THỂ HIỆN, NGẦM Ý HOẶC ĐẠI DIỆN VỀ MẶT LUẬT PHÁP, ĐẢM BẢO HOẶC BẢO LÃNH CHO BẤT CỨ VIỆC GÌ LIÊN QUAN TỚI TRANG WEB NÀY, HAY BẤT CỨ TÀI LIỆU NÀO LIÊN QUAN TỚI CHẤT LƯỢNG, SỰ PHÙ HỢP, TÍNH TRUNG THỰC, ĐỘ CHÍNH XÁC HOẶC SỰ HOÀN THIỆN CỦA BẤT CỨ THÔNG TIN NÀO HOẶC TÀI LIỆU NÀO ĐƯỢC LƯU TRỮ HOẶC THỂ HIỆN TRÊN TRANG WEB NÀY, BAO GỒM KHÔNG HẠN CHẾ TỚI BỘ TÀI LIỆU. TRỪ KHI ĐƯỢC NÊU RÕ CỤ THỂ, TỚI MỨC TỐI ĐA ĐƯỢC CHO PHÉP BỞI PHÁP LUẠT ÁP DỤNG, TRANG WEB NÀY VÀ TẤT CẢ CÁC TÀI LIỆU ĐƯỢC LƯU TRỮ HOẶC THỂ HIỆN TRÊN TRANG WEB ĐƯỢC CUNG CẤP CHO QUÝ KHÁCH THEO HÌNH THỨC “NHƯ VẬY”, “NHƯ CÓ SẴN” VÀ “THEO THỰC TRẠNG”SẼ KHÔNG CÓ NGẦM Ý ĐẢM BẢO VIỆC ĐẢM BẢO TÍNH KHẢ THI CỦA DỊCH VỤ THƯƠNG MẠI, TÍNH PHÙ HỢP CHO MỘT MỤC ĐÍCH NHẤT ĐỊNH HOẶC VIỆC KHÔNG VI PHẠM CÁC QUYỀN LỢI CỦA BÊN THỨ 3. WOWMELO SẼ KHÔNG ĐẢM BẢO RẰNG MÁY TÍNH CỦA QUÝ KHÁCH SẼ KHÔNG DÍNH VIRUS, PHẦN MỀM THEO DÕI HOẶC CÁC MÃ ĐỘC.</p>
<h3>Hạn chế về trách nhiệm</h3>
<p>KHÔNG MỘT PHẦN/BỘ PHẬN NÀO CỦA WOWMELO SẼ CHỊU TRÁCH NHIỆM VỀ CÁC HƯ TỔN GÂY RA BỞI VIỆC QUÝ KHÁCH HIỂN THỊ, SAO CHÉP HOẶC TẢI BẤT CỨ TÀI LIỆU NÀO LÊN HOẠC TỪ TRANG WEB NÀY. KHÔNG MỘT PHẦN/BỘ PHẬN NÀO CỦA WOWMELO TRONG BẤT CỨ TRƯỜNG HỢP NÀO SẼ CHỊU TRÁCH NHIỆM CHO CÁC HƯ TỔN GIÁN TIẾP, ĐẶC BIỆT, HOẶC LÀ KẾT QUẢ CỦA VIỆC NÀO ĐÓ (BAO GỒM MẤT DỮ LIỆU, DOANH THU, LỢI NHUẬN HOẶC CÁC LỢI THẾ VỀ MẶT KINH TẾ) CHO DÙ CÁC TRƯỜNG HỢP XẢY RA NHƯ THẾ NÀO, KỂ CẢ KHI MỘT PHẦN/BỘ PHẬN CỦA WOWMELO BIẾT HOẶC NÊN BIẾT VỀ VIỆC HƯ TỔN CÓ THỂ XẢY RA.</p>
<h3>Luật pháp Việt Nam và Sử dụng Dịch vụ ngoài lãnh thổ Việt Nam</h3>
<p>WomMelo kiểm soát và vận hành trang web này từ văn phòng chính tại Việt Nam và Bộ tài liệu có thể không phù hợp hoặc không sử dụng được tại các nước khác. Nếu Quý khách chọn sử dụng trang web này ngoài lãnh thổ Việt Nam, Quý khách sẽ chịu hoàn toàn trách nhiêm cho việc tuân thủ theo luật pháp địa phương hiện hành.</p>
<h3>Phản hồi và phản ánh</h3>
<p>WowMelo sẽ khuyến cáo Quý khách nếu chúng tôi nhận thấy Quý khách không tuân thủ với các Điều khoản này và sẽ đề xuất phương án giải quyết. Tuy nhiên, một số vi phạm nhất định, tùy theo WowMelo quyết định, có thể khiến quyền truy cập trang web này của Quý khách bị ngừng mà không cần có thông báo trước. Bất cứ tranh chấp hoặc không đồng thuận với Điều khoản hoặc trang web này, hoặc mối quan hệ về kinh doanh giữa Quý khách và WowMelo sẽ được xử tại toà án Việt Nam.</p>
<h3>Thỏa thuận chung </h3>
<p>Nếu bất cứ Điều khoản nào trong đây không đồng nhất với pháp luật hiện hàng, thì các điều khoản đó sẽ được coi như là phản ánh ý định của các bên, và không một điều khoản nào khác phải điều chỉnh theo. Nếu WowMelo không thực hiện theo đúng bất cứ Điều khoản nào trong đây, thì việc đó sẽ không được tính là lí do để miễn hoặc loại bỏ các điều khoản đó. Các Điều khoản này là thỏa thuận giữa riêng Quý khách và WowMelo và thay thế bất cứ các thỏa thuận/đàm phán/thảo luận trước đó hoặc tại thời điểm hiện tại. Quyền sở hữu, lưu ý về tính đảm bảo, diễn tả được thực hện bởi Quý khách, bồi thường, giới hạn về mặt trách nhiệm và quy định chung sẽ vẫn được áp dụng ngay cả khi thỏa thuận này chấm dứt.</p>

TEXT
        ,
    ],
    'policy'            => [
        'title' => 'CAM KẾT VỚI KHÁCH HÀNG',
        'content' => <<<'TEXT'
<h3>Giới Thiệu về chính sách của WowMelo</h3>
<p>WowMelo cam kết sẽ bảo đảm bạn không gặp bất cứ vấn đề nào khi mua hàng. Mục đích của chính sách này chính là giúp bạn cảm thấy thoải mái và an toàn khi mua sắm tại các cửa hàng là đối tác của chúng tôi. Bạn có thể hoàn toàn yên tâm vì tất cả dữ liệu cá nhân được bảo vệ tốt theo Chính sách bảo mật của WowMelo.</p>
<p>Chính sách này có giá trị với tất cả các giao dịch mà bạn thực hiện với tư cách là người dùng của WowMelo.</p>
<h3>Bạn sẽ không cần phải trả tiền nếu Quyền người mua hàng của bạn bị vi phạm</h3>
<p>Cụ thể, bạn sẽ không phải trả tiền nếu:</p>
<ul>
    <li>Bạn không nhận được sản phẩm mà mình đã đặt mua</li>
    <li>Hàng mà bạn nhận được bị lỗi hoặc không phải là hàng mà bạn đã đặt mua trước đó</li>
</ul>
<p>Trong trường hợp thời hạn thanh toán sắp đến, xin vui lòng đảm bảo các nội dung sau:</p>
<table class="table">
    <tr>
        <th>Đối với trường hợp hàng chưa được giao</th>
        <th>Đối với trường hợp hàng bị lỗi hoặc không phải hàng bạn đã đặt mua</th>
    </tr>
    <tr>
        <td>
            <ul>
                <li>Kiểm tra lại một lần nữa để chắc chắn rằng bạn không bỏ qua thông tin thông báo về việc giao nhận hàng.</li>
                <li>Liên hệ với bên bán hàng để kiểm tra xem đơn hàng của bạn đang ở đâu.</li>
                <li>Nếu bạn vẫn chưa nhận được hàng trong khi hạn thanh toán đang đến gần thì hãy liên hệ với bộ phận Chăm sóc khách hàng của WowMelo. Chúng tôi sẽ không yêu cầu bạn phải thanh toán trong thời gian chúng tôi xem xét các vấn đề xảy ra với đơn hàng của bạn. Nếu kết quả là đơn hàng của bạn vẫn chưa được giao, chúng tôi sẽ hoãn ngày thanh toán ( trừ một số trường hợp bên dưới*). </li>
            </ul>
        </td>
        <td>
            <ul>
                <li>Liên hệ với bộ phận Chăm sóc khách hàng của WowMelo để chúng tôi có thể điều chỉnh thời hạn thanh toán của bạn.</li>
                <li>Liên hệ với cửa hàng mà bạn đã mua để có thể giải quyết vấn đề. Trong trường hợp vấn đề được giải quyết và WowMelo nhận được thông báo chính thức từ bên Cửa hàng, chúng tôi sẽ cập nhật thông tin hoá đơn và vấn đề trả hàng.</li>
                <li>Nếu bạn không thể giải quyết vấn đề với cửa hàng trước hạn thanh toán xin vui lòng liên hệ với bộ phận Chăm sóc khách hàng của WowMelo. Bạn cần cung cấp cho chúng tôi: Tên thương hiệu mà bạn đã mua, ngày mua, mã đơn hàng, mô tả về vấn đề mà bạn đang gặp với hàng mà mình nhận được và thời gian mà bạn liên hệ với cửa hàng để giải quyết vấn đề. WowMelo sẽ xem xét các vấn đề mà bạn đang gặp và đưa ra phương án xử lí. Nếu các vấn đề bạn phản ánh không hợp lệ, bạn vẫn phải thanh toán cho chúng tôi như thoả thuận ban đầu.</li>
            </ul>
        </td>
    </tr>
    <tr>
        <th>Nên làm gì nếu bạn đã thanh toán đơn hàng rồi?</th>
        <th>Nên làm gì nếu bạn đã thanh toán đơn hàng rồi?</th>
    </tr>
    <tr>
        <td>
            <p>Nếu bạn đã thanh toán rồi nhưng vẫn chưa nhận được hàng, xin vui lòng làm theo các bước sau:</p>
            <ul>
                <li>Kiểm tra lại một lần nữa để chắc chắn rằng bạn không bỏ qua thông báo giao hàng.</li>
                <li>Liên hệ với cửa hàng để kiểm tra xem đơn hàng của bạn đang ở đâu?</li>
                <li>Nếu vấn đề vẫn chưa được giải quyết xin vui lòng liên hệ với bộ phận Chăm sóc khách hàng của WowMelo. Chúng tôi sẽ sẽ xem xét, nếu hàng của bạn vẫn chưa được giao chúng tôi sẽ hoàn trả lại số tiền mà bạn đã thanh toán cho đơn hàng  trừ một số trường hợp bên dưới*).</li>
            </ul>
        </td>
        <td>
            <p>Nếu bạn đã thanh toán nhưng sau đó mới phát hiện hàng bị lỗi hoặc hàng nhận được không phải hàng bạn đặt mua, xin vui lòng làm theo các bước sau:</p>
            <ul>
                <li>Liên hệ với cửa hàng để có thể giải quyết các vấn đề trên. Cửa hàng có trách nhiệm phải đổi hàng hoặc hoàn tiền lại cho bạn trong trường hợp này.</li>
                <li>Nếu bạn không thể liên hệ với được cửa hàng xin vui lòng liên hệ với bộ phận Chăm sóc khách hàng của WowMelo. Chúng tôi sẽ xem xét vấn đề của bạn. Nếu hàng hoá của bạn nhận được thật sự có vấn đề như trên chúng tôi sẽ hoàn trả lại đúng số tiền mà bạn đã thanh toán cho đơn hàng.</li>
            </ul>
        </td>
    </tr>
    <tr>
        <th colspan="2"><span class="red">*Ghi chú các trường hợp ngoại lệ:</span></th>
    </tr>
    <tr>
        <td colspan="2">
            <p>Bạn phải tạo điều kiện cho đơn hàng được giao trước khi có thể được phép yêu cầu hoàn trả hoặc lùi hạn thanh toán trong trường hợp đơn hàng không được giao. Nếu bạn không cố gắng tạo điều kiện để nhận hàng hoặc để hàng được giao một cách hợp lý sau khi nhận được thông báo giao hàng, bạn sẽ phải chịu phí hoàn trả đơn hàng cho cửa hàng (bao gồm phí giao hàng mà cửa hàng phải trả cho Bên giao hàng).</p>
            <p>Vui lòng lưu ý rằng nếu bạn muốn sử dụng quyền lợi hủy đơn hàng, thì bạn cần đảm bảo hơn việc hủy đơn hàng được cửa hàng mà bạn đặt mua hàng cho phép. Nhìn chung, bạn sẽ không được hủy đơn hàng nếu hàng đã được xuất kho. Tất cả các yêu cầu hủy đơn hàng cho ngày xuất kho sẽ được tính là yêu cầu hoàn trả đơn hàng, và vì vậy Chính sách hoàn trả của cửa hàng mà bạn đặt mua hàng sẽ được áp dụng. Việc này đồng nghĩa với việc bạn phải tạo điều kiện và cố gắng nhận đơn hàng, nếu không bạn sẽ phải chịu phí hoàn trả đơn hàng (bao gồm phí giao hàng mà cửa hàng phải trả cho Bên giao hàng) cho cửa hàng và chúng tôi sẽ cập nhật khoản phí này vào hóa đơn của bạn. Nếu bạn không hoàn trả phí hoàn trả đơn hàng mà chúng tôi yêu cầu trong trường hợp này, bạn có thể không được phép tiếp tục sử dụng Dịch vụ của WowMelo trong tương lai.</p>
        </td>
    </tr>
</table>
<h3>Bạn muốn huỷ đơn hàng của mình?</h3>
<p>Bạn hoàn toàn có thể huỷ đơn hàng của mình nhưng bạn cần đảm bảo rằng mình không vi phạm chính sách huỷ đơn hàng của cửa hàng mà bạn đã mua.</p>
<p><b>Đơn hàng chưa được gửi đi.</b></p>
<p>Nếu bạn vẫn chưa thanh toán và muốn huỷ đơn hàng xin vui lòng thông báo cho chung tôi qua email: info@wowmelo.com và thông báo cho cửa hàng nơi mà bạn đã mua. Sau khi cửa hàng chấp nhận yêu cầu huỷ bỏ đơn hàng của bạn, chúng tôi sẽ cập nhật thông tin huỷ vào đơn hàng của bạn. Vui lòng đảm bảo bao gồm thông tin đặt hàng của bạn trong email cho chúng tôi (tên người bán, ngày mua, số đơn đặt hàng, mô tả về hàng hóa bị hủy và ngày khi bạn liên hệ với người bán về lần hủy đầu tiên). </p>
<p>Nếu bạn đã thanh toán cho chúng tôi, sau đó cửa hàng chấp nhận yêu cầu huỷ đơn hàng của bạn. Trong trường hợp này chúng tôi sẽ hoàn trả tiền lại cùng phương thức thanh toán bạn đã chọn cho giao dịch đó. </p>
<table class="table">
    <tr>
        <th>Thẻ credit/debit hoặc thẻ ATM nội địa</th>
        <td>Trả lại trực tiếp vào thẻ của bạn mà không phải trả thêm phí</td>
    </tr>
    <tr>
        <th>Chuyển khoản ngân hàng</th>
        <td>Trả lại trực tiếp vào ngân hàng mà bạn đã chọn trước đó (vui lòng lưu ý thông tin này trong email yêu cầu hủy của bạn). Bạn sẽ chịu trách nhiệm về phí chuyển khoản ngân hàng.</td>
    </tr>
    <tr>
        <th>Tiền mặt</th>
        <td>
            <p>Để nhận hoàn trả bằng tiền mặt, vui lòng đến trực tiếp văn phòng đại diện của WowMelo tại <b>Tầng 22, TNR Tower, 180-192 Nguyễn Công Trứ, Phường Nguyễn Thái Bình, Quận 1, TP.HCM</b></p>
            <p>hoặc</p>
            <p>Bạn có thể nhận hoàn trả thông qua chuyển khoản vào ngân hàng mà bạn đã chọn trước đó (vui lòng lưu ý thông tin này trong email yêu cầu hủy của bạn). Bạn sẽ chịu trách nhiệm về phí chuyển khoản ngân hàng.</p>
        </td>
    </tr>
</table>
<p><b>Đơn hàng đã được gửi đi</b></p>
<p>Nói chung bạn không thể huỷ nếu đơn hàng đã được gửi đi. Bất kỳ khiếu nại hủy nào sau ngày gửi sẽ được coi là trả lại, do đó bạn phải tuân thủ Chính sách hoàn trả của cửa hàng bạn đã mua. Điều này cũng có nghĩa là bạn sẽ cần xem xét kỹ lưỡng trước khi yêu cầu trả lại, nếu không bạn thường phải chịu trách nhiệm về chi phí trả lại hàng cho cửa hàng và điều này sẽ được cập nhật trong hóa đơn của bạn. Việc không trả chi phí hoàn trả trong tình huống này sẽ ảnh hưởng đến khả năng sử dụng Wowmelo trong tương lai của bạn trong tương lai.</p>
<h3>Yêu cầu/nghĩa vụ đối với cửa hàng hoặc các Tổ chức tài chính</h3>
<p><b>Với cửa hàng</b></p>
<p>Chính sách này nêu rõ quy cách WowMelo xử lý các đơn hàng không được giao hoặc có hàng sai. Các tranh chấp hoặc khoản nợ thanh toán từ bạn theo chính sách này thì cửa hàng vẫn có thể yêu cầu bạn thanh toán mà không cần sự tham gia của WowMelo. Nếu bạn và cửa hàng có thể thỏa thuận phương án giải quyết, hoặc nếu tranh chấp giữa hai bên được giải quyết bằng quyết định của bên khác (ví dụ, theo quyết định của tòa án) và lợi thế nghiêng về phía cửa hàng, thì cửa hàng vẫn có thể nhờ WowMelo hoặc qua hình thức khác để thu số tiền cần được thanh toán từ bạn.</p>
<p><b>Với Tổ chức tài chính</b></p>
<p>Nếu bạn sử dụng thẻ debit/credit/ATM nội địa hoặc hình thức chuyển khoản ngân hàng để đặt mua hàng qua WowMelo, tuy nhiên hàng nhận được có vấn đề, hoặc nếu thẻ debit/credit/ATM nội địa hoặc thẻ ngân hàng của bạn bị sử dụng bất hợp pháp, bạn có thể thông báo với (i) WowMelo theo chính sách này hoặc (ii) tổ chức tài chính phát hành thẻ hoặc nhận tiền gửi của bạn, tuy nhiên bạn chỉ được chọn một trong hai hình thức thông báo.</p>
<p>Nếu bạn thông báo và yêu cầu WowMelo hoàn trả, cùng lúc chúng tôi nhận được yêu cầu hoàn trả từ tổ chức tài chính, thì ngay lập tức chúng tôi sẽ hủy yêu cầu hoàn trả của bạn tại WowMelo, và hoàn trả số tiền tranh chấp cho tổ chức tài chính. Nếu yêu cầu hoàn trả của bạn với WowMelo bị từ chối hoặc hủy, hoặc bạn không nhận được số tiền hoàn trả từ WowMelo, bạn vẫn có thể yêu cầu Tổ chức tài  chính liên quan hoàn trả số tiền này dựa trên chính sách của các Tổ chức tài chính này. Nếu bạn đã thanh toán cho WowMelo tại thời điểm yêu cầu hoàn trả, chúng tôi sẽ hoàn trả số tiền bạn thanh toán cho đơn hàng của mình, bao gồm thuế và phí giao hàng cho bạn.</p>

TEXT
        ,
    ],
    'about'             => [
        'title' => 'Về Chúng Tôi',
        'desc' => 'WowMelo sẽ giúp bạn gia tăng hứng khởi và niềm tin khi mua sắm trực tuyến bằng việc giảm đi những công đoạn phức tạp trong việc thanh toán. Khi thanh toán bằng Wowmelo bạn sẽ được nhận hàng ngay lập tức nhưng có thể trả sau đến 14 ngày mà không phải trả thêm bất kì chi phí phát sinh nào.',
        'content' => <<<'TEXT'
<h3>1. Về WowMelo</h3>
<p>Fram^ là một công ty Thụy Điển trong lĩnh vực Phát triển Công nghệ Thông tin & Nôi Khởi nghiệp (Venture Building) hoạt động tại khu vực Đông Nam Á. Fram ^ được thành lập và xây dựng bởi những con người đầy đam mê và tài năng đến từ khắp nơi trên thế giới.</p> 
<p>WowMelo là một trong những thành viên của được phát triển bởi Fram^. Chúng tôi ra đời với sứ mệnh xóa bỏ rào cản và tạo dựng niềm tin cho việc mua sắm trực tuyến ở Việt Nam – một thị trường đang bùng nổ phát triển mạnh mẽ. Với phương thức thanh toán đơn giản của WowMelo, bạn được thử sản phẩm tại nhà trước khi quyết định chi trả, mà không phải trả thêm bất cứ chi phí phát sinh. </p>
<h3>2. Trải nghiệm mua sắm tuyệt vời khi có WowMelo:</h3>
<p>Phương thức thanh toán đơn giản, tiện lợi cho mọi trường hợp vì bạn không cần phải trải qua nhiều công đoạn như hình thức thanh toán trực tuyến truyền thống.</p>
<p>Bạn sẽ được thử và cảm nhận sản phẩm trước khi thanh toán.</p>
<p>Ban có thể chọn thời điểm thanh toán trong vòng 14 ngày sau ngày đặt hàng mà không phát sinh bất kì chi phí nào.</p>
<h3>3. Giá trị cốt lõi của WowMelo:</h3>
<p>Đập tan nghi ngờ về chất lượng sản phẩm của việc thanh toán trước khi trải nghiệm - một trong những vấn đề nan giải của việc mua sắm trực tuyến tại Việt Nam.</p>
<p>Giúp bạn có thể mua sắm trực tuyến một cách dễ dàng vì bước thanh toán với Wowmelo vô cùng đơn giản và nhanh gọn.</p>
<p>Đảm bảo toàn bộ giá trị đơn hàng, và bảo mật an toàn thông tin cho bạn.</p>
<h3>4. Ý nghĩa của “WowMelo":</h3>
<p>Shopping is WOW: Bạn sẽ phải thốt lên Wow! Sứ mệnh của WowMelo giúp bạn đơn giản hóa việc mua sắm bằng việc loại bỏ quy trình thanh toán mất thời gian truyền thống. Bạn có thể hoàn thiện đơn hàng với vài thao tác đơn giản.</p>
<p>Shopping is MELO: Melo là cách viết tắt của “mellow – ngọt ngào”. WowMelo cho phép bạn  mua sắm thoải mái tại các cửa hàng yêu thích và thanh toán hóa đơn sau đó tới tận 14 ngày mà không cần phải trả thêm bất cứ lãi suất hay phụ phí nào.</p>

TEXT
        ,
    ],
    'merchant_support'  => [
        'title' => 'Câu hỏi thường gặp – Đối tác Bán hàng',
        'content' => <<<'TEXT'
<h3>1. Dịch vụ Wowmelo</h3>
<p><b>Đơn vị tiền tệ nào được chấp nhận bởi Wowmelo?</b></p>
<p>Hiện tại Wowmelo chỉ chấp nhận đơn vị tiền tệ Việt Nam Đồng.</p>
<h3>2. Thông tin thanh toán</h3>
<p><b>Nếu Khách hàng lựa chọn sử dụng dịch vụ Hỗ trợ mua sắm của Wowmelo, khi nào tôi sẽ nhận được khoản thanh toán cho đơn hàng?</b></p>
<p>Đối tác sẽ nhận được khoản thanh toán theo lịch được thống nhất khi ký Hợp đồng Hợp tác Thương Mại giữa hai bên. Thường tiền sẽ được chuyển sau khoảng hai (02) ngày làm việc ngay sau khi WowMelo nhận được thông báo xác nhận đơn hàng từ phía đối tác. WowMelo sẽ luôn thanh toán cho các đơn hàng được WowMelo cho phép sử dụng dịch vụ Hỗ trợ mua sắm. Việc này sẽ không liên quan tới cách mà Khách hàng chọn để thanh toán cho WowMelo.</p>
<p><b>Khi nào thì Khách hàng được hoàn tiền lại cho các yêu cầu trả hàng được chấp nhận?</b></p>
<p>Phụ thuộc vào việc Đối tác đã nhận được tiền từ WowMelo chưa và việc Wowmelo đã nhận được tiền từ Khách hàng chưa:</p>
<p><u>Đối tác đã nhận thanh toán</u></p>
<ul>
    <li>WowMelo đã nhận tiền từ Khách hàng: Đối tác hoàn trả cho Khách hàng</li>
    <li>WowMelo chưa nhận tiền từ khách hàng: Đối tác hoàn trả cho WowMelo</li>
</ul>
<p><u>Đối tác chưa nhận thanh toán</u></p>
<ul>
    <li>WowMelo đã nhận tiền từ Khách hàng: WowMelo hoàn trả cho Khách hàng</li>
    <li>WowMelo chưa nhận tiền từ khách hàng: Không cần hoàn trả; hóa đơn sẽ được cập nhật tự động</li>
</ul>
<h3>3. Hợp đồng /Thoả thuận với Wowmelo</h3>
<p><b>WowMelo chịu trách nhiệm cho rủi ro liên quan tới gian lận như thế nào?</b></p>
<p>Đối tác Bán hàng của WowMelo được bảo vệ khỏi những rủi ro liên quan tới gian lận chỉ trong trường hợp Đối tác Bán hàng tuân thủ theo Chính sách giao hàng của WowMelo.</p>
<p><b>Những mặt hàng nào không được phép bán theo Thỏa thuận với WowMelo?</b></p>
<p>Thông tin chi tiết trong bản Hướng dẫn Đạo đức trong Kinh doanh của WowMelo.</p>
<h3>4. Xử lý đơn hàng</h3>
<p><b>Chúng tôi xác nhận đơn hàng như thế nào trong Cổng thông tin Đối tác Bán hàng (“Merchant Portal”)</b></p>
<p><i>Đơn hàng chưa xác nhận</i> là đơn hàng được tạo tại bước Hoàn thiện đơn hàng (“Check-out”), nhưng chưa được hệ thống máy chủ xác nhận. Nếu Đối tác có Đơn hàng chưa xác nhận, Đối tác nên đưa vấn đề này cho nhân viên kỹ thuật của mình để xem xét trường hợp có lỗi API tiềm ẩn. Đối tác nên xử lý các đơn hàng này của Cổng thông tin Đối tác bán hàng.</p>
<p>Xác nhận đơn hàng đồng nghĩa với việc hàng hóa đã sẵn sàng có để chuyển đi cho Khách hàng. Thao tác xác nhận đơn hàng sẽ kích hoạt quy trình Mua lại đơn hàng nếu Khách hàng chọn sử dụng dịch vụ của WowMelo. Việc xác nhận đơn hàng cũng sẽ kích hoạt quy trình WowMelo thanh toán cho Đối tác Bán hàng.</p>
<p><b>Chúng tôi thêm hàng hóa, phí và mức chiết khấu cho Đơn hàng như thế nào?</b></p>
<p>Đối tác có thể chỉnh sửa chi tiết Đơn hàng trong Cổng thông tin Đối tác bán hàng. Đối tác có thể xóa, thêm, thay đổi tên hàng hóa, thông tin tham khảo, số lượng, giá hoặc mức chiết khấu tương ứng.</p>
<p>Đối tác không được phép tăng giá trị đơn hàng vượt quá giá trị được Khách hàng xác nhận tại thời điểm mua hàng. Nếu giá trị đơn hàng lớn hơn giá trị được Khách hàng xác nhận, tổng giá trị đơn hàng sẽ được bôi đỏ và Đối tác sẽ không thể lựa chọn nút “Lưu chỉnh sửa”.</p>
<p><u>Đơn hàng đã được xuất kho/giao vận:</u></p>
<p>Đối tác không được quyền chỉnh sửa tăng giá trị của đơn hàng đã được xuất kho, tuy nhiên Đối tác vẫn được phép thêm chiết khấu hoặc loại bỏ phí thu sau khi đơn hàng được xuất kho. Đối tác chỉ cần đăng nhập vào Cổng thông tin Đối tác, tìm đơn hàng cần được điều chỉnh và chọn thanh công cụ “Hoàn trả”: Đối tác sẽ được quyền yêu cầu hoàn trả cho Khách hàng số tiền không quá giá trị đơn hàng tương ứng.</p>
<p><b>Xóa đơn hàng khỏi Cổng thông tin Đối tác bán hàng như thế nào?</b></p>
<p>Nếu hàng hóa chưa được xuất kho, Đối tác vui lòng lựa chọn thanh “Xóa” để cập nhật hóa đơn và tài khoản cho Khách hàng. Nếu Khách hàng đã thanh toán, WowMelo sẽ xử lý yêu cầu hoàn trả tự động. Các đơn hàng bị xóa sẽ không được hiển thị trên Bản báo cáo tra soát chuyển tiền giữa 2 bên, nếu WowMelo chưa chuyển khoản tiền tương ứng cho Đơn hàng đó tới Đối tác.</p>
<p><b>Lùi hạn thanh toán trên hóa đơn như thế nào?</b></p>
<p>Nếu Khách hàng có thắc mắc liên quan tới hạn thanh toán, xin vui lòng hướng dẫn Khách hàng liên hệ với Dịch vụ Chăm sóc Khách hàng của WowMelo. Nếu Đối tác có thắc mắc liên quan tới việc lùi hạn thanh toán, xin vui lòng liên hệ tới Bộ phận Hỗ trợ Đối tác.</p>
<p><b>Gửi lại hóa đơn đã được cập nhật cho Khách hàng như thế nào?</b></p>
<p>Đối tác chỉ cần lựa chọn thanh “Gửi lại hóa đơn” trên trang Đơn hàng. Hóa đơn mới sẽ được gửi tới email của Khách hàng.</p>
<h3>5. Hoàn trả</h3>
<p><b>Chúng tôi được đợi nhận lại hàng hoàn trả - chúng tôi nên xử lý hóa đơn như thế nào?</b></p>
<p>Vui lòng hướng dẫn Khách hàng liên hệ Bộ phận Chăm sóc Khách hàng của WowMelo trong trường hợp hàng hoàn trả chưa được Đối tác nhận lại, mà đã quá hạn thanh toán.</p>
<p><b>Nếu Khách hàng có khiếu nại về Hóa đơn thì chúng tôi xử lý như thế nào?</b></p>
<p>Nếu Khách hàng có khiếu nại về Hóa đơn, vui lòng hướng dẫn Khách hàng liên hệ Bộ phận Chăm sóc Khách hàng của WowMelo để được hỗ trợ thêm.</p>
<p><b>Điều chỉnh đơn hàng và hoàn trả một phần cho Đơn hàng trong Cổng thông tin Đối tác bán hàng?</b></p>
<ul>
    <li>
        <p><u>Đối với các Đơn hàng chưa được xuất kho</u></p>
        <p>Vui lòng chọn “Điều chỉnh Đơn hàng”, Đối tác sẽ được quyền thay đổi Đơn hàng.</p>
    </li>
    <li>
        <p><u>Đối với các Đơn hàng đã xuất kho</u></p>
        <p>Vui lòng chọn Đơn hàng tương ứng và chọn “Hoàn trả” trong phần “Đơn hàng”. Đối tác được quyền lựa chọn số tiền cần được hoàn trả cho Khách hàng. Khi số tiền hoàn trả được nhập nhau, vui lòng nhấp chọn “Hoàn trả”.</p>
    </li>
</ul>
<p><b>Gửi lại Hóa đơn đã được cập nhật cho Khách hàng như thế nào?</b></p>
<p>Trong trường hợp thắc mắc/khiếu nại đã được giải quyết và Khách hàng đã đồng ý thanh toán cho Hóa đơn mới, vui lòng hướng dẫn Khách hàng liên hệ với Bộ phận Chăm sóc Khách hàng của WowMelo.</p>
<h3>6. Hồ sơ Kế toán</h3>
<p><b>Chúng tôi  nhận phần thanh toán cho các Đơn hàng sử dụng WowMelo khi nào?</b></p>
<p>Tất cả các đơn hàng được Kích hoạt sẽ được thanh toán gộp theo hạn thanh toán được thỏa thuận trong Hợp đồng Thỏa thuận Hợp tác Thương Mại giữa WowMelo và Đối tác.</p>
<p>Nếu Đối tác có thắc mắc liên quan tới hạn thanh toán, vui lòng liên hệ với Bộ phận Hỗ trợ Đối tác.</p>
<p><b>Chúng tôi nhận Bản báo cáo tra soát chuyển tiền như thế nào?</b></p>
<p>Cổng thông tin Đối tác bán hàng sẽ mặc định chiết xuất bản bản cáo sơ lược cho các khoản tiền ra-vào hàng tháng, Đối tác có thể chọn chiết xuất các bản báo cáo cho khoảng thời gian dài hơn. Đối tác có thể làm thao tác này bằng cách chọn “Lọc theo ngày” trong phần “Báo cáo tra soát chuyển tiền”.</p>
<p><b>Bản Báo cáo tra soát chuyển tiền có nội dung gì?</b></p>
<p>Khi Đối tác lựa chọn “Bản cáo tra soát chuyển tiền” tại đầu trang, Quý khách sẽ thấy danh sách thông tin về các khoản tiền thu chi, bao gồm ngày thu chi, thời gian bán hàng, nội dung giao dịch, doanh thu tổng, phí và tình trạng hoàn trả và số tiền được thu chi.</p>
<ul>
    <li>
        <p><u>Tải file CVS cho Bản báo cáo tra soát chuyển tiền</u></p>
        <p>Đối tác có thể tải file CVS bao gồm thông tin tổng tiền và các giao dịch liên quan tới mỗi lần thu chi.</p>
    </li>
    <li>
        <p><u>Tải file PDF cho Bản báo cáo tra soát chuyển tiền</u></p>
        <p>Đối tác có thể tải file PDF bao gồm thông tin tổng tiền của mỗi lần thu chi.</p>
    </li>
</ul>
<p><b>Rà soát giao dịch cho mỗi đợt chuyển tiền như thế nào?</b></p>
<p>Đối tác có thể xem danh sách các giao dịch của mỗi đợt chuyển tiền bằng cách nhấp chuột vào mã lệnh chuyển tiền.</p>
<p><u>Đường link dẫn tới Đơn hàng</u></p>
<p>Mỗi giao dịch sẽ được dẫn bằng đường link tới Đơn hàng tương ứng. Bằng cách nhấp chuột vào số Đơn hàng, Đối tác có thể thấy ngay lập tức thông tin Đơn hàng chi tiết và thông tin của lệnh chuyển tiền tương ứng.</p>
<h3>7. Quản lý Người dùng</h3>
<p><b>Người dùng đã đăng ký là ai?</b></p>
<p>Người dùng đã đăng ký là người có thể đăng nhập vào Hệ thống xử lý đơn hàng, Cổng thông tin Đối tác Bán hàng. Các người dùng khác nhau sẽ có các quyền/cấp độ được truy cập khác nhau, và thông tin địa chỉ email sẽ là thông tin sử dụng để nhận diện Người dùng. Khi liên hệ với Bộ phận Hỗ trợ Đối tác, Đối tác sẽ cần cung cấp  thông tin nhận diện Người dùng đã đăng ký để đảm bảo rằng thông tin được yêu cầu cung cấp sẽ chỉ được đưa cho người có thẩm quyền.</p>
<p><b>Thêm Người dùng như thế nào?</b></p>
<p>Đối tác có thể thêm Người dùng mới để cho phép quyền truy cập vào Cổng thông tin Đối tác bán hàng qua Nhân viên Quản trị chỉ định bằng cách sử dụng nút “Mời Người dùng” trong phần “Người dùng”.</p>
<p>Đối tác cũng có thể hủy hoặc điều chỉnh quyền truy cập của các Người dùng hiện tại nếu tài khoản của Đối tác có chức năng/quyền hành cho phép điều đó.</p>
<p><b>Các tài khoản Người dùng khác nhau có quyền truy cập khác nhau như thế nào?</b></p>
<p>Người dùng Quản trị (Admin) sẽ có cấp độ truy cập khác. Người dùng Quản trị (Admin) sẽ tự động nhận quyền truy cập tất cả các ứng dụng và phiên bản cập nhật trong Cổng thông tin Đối tác bán hàng. Các Người dùng khác sẽ cần được ủy quyền để truy cập các phiên bản cập nhật.</p>
<p>Người dùng Quản trị (Admin) sẽ có quyền truy cập như sau:</p>
<ul>
    <li>Tất cả các ứng dụng và chức năng trong Cổng thông tin Đối tác bán hàng</li>
    <li>Tất cả các phiên bản cập nhật mới nhất của các ứng dụng và chức năng, tuy nhiên các Người dùng khác sẽ cần Người dùng Quản trị cung cấp lại quyền truy cập</li>
</ul>
<p>Đối tác cần có ít nhất một (01) Người dùng Quản trị (Admin), nhưng được phép có nhiều Người dùng Quản trị (Admin).</p>
<p><u>Điều chỉnh quyền truy cập</u></p>
<p>Người dùng thường, khác Người dùng Quản trị, sẽ cần sự cho phép để sử dụng mỗi ứng dụng riêng, như:</p>
<ul>
    <li>Lịch sử</li>
    <li>Đơn hàng</li>
    <li>Cài đặt</li>
    <li>Khoản thanh toán</li>
    <li>Người dùng</li>
</ul>
<p><u>Quyền truy cập trong ứng dụng “Đơn hàng”</u></p>
<p>Trong phần ứng dụng “Đơn hàng”, Người dùng Quản trị (Admin) sẽ được cho phép quyền truy cập “Giới hạn” hoặc “Đầy đủ” cho Người dùng thông thường.</p>
<p>Quyền truy cập “Giới hạn” cho phép Người dùng những việc sau:</p>
<ul>
    <li>Xem đơn hàng</li>
    <li>Gửi lại Hóa đơn</li>
    <li>Điều chỉnh thông tin email của Khách hàng</li>
</ul>
<p>Quyền truy cập “Giới hạn” KHÔNG cho phép Người dùng những việc sau:</p>
<ul>
    <li>Sử dụng các tính năng quản lý đơn hàng khác, ví dụ đánh dấu hàng đã xuất kho, hủy, hoàn trả hoặc nhả Đơn hàng.</li>
</ul>
<p>Quyền truy cập “Đầy đủ” cho phép Người dùng xem và sử dụng tất cả các tính năng được nêu trên.</p>
<p><b>Chúng tôi kích hoạt người dùng trên Cổng thông tin Đối tác bán hàng như thế nào?</b></p>
<p>Khi Người dùng Quản trị thêm Người dùng mới vào Cổng thông tin Đối tác bán hàng, email tự động với đường link kích hoạt sẽ được gửi tới địa chỉ email của Người dùng mới. Người dùng mới cần nhấp vào đường link trong vòng bảy ngày. Trên trang Kích hoạt, Người dùng chỉ cần điền thông tin mật khẩu mới trước khi chọn “Chọn mật khẩu”. Tài khoản của Người dùng mới sẽ được Kích hoạt và Người dùng mới có thể đăng nhập vào Cổng thông tin Đối tác bán hàng của WowMelo.</p>
<p><b>Chúng tôi cài đặt lại mật khẩu như thế nào?</b></p>
<p>Đối tác chỉ cần lựa chọn “Quên mật khẩu” trên trang đăng nhập Cổng thông tin Đối tác bán hàng và điền thông tin địa chỉ email của Người dùng. Email hướng cài đặt lại mật khẩu sẽ được gửi tới địa chỉ email lựa chọn.</p>
<h3>8. Thông tin Đối tác bán hàng</h3>
<p><b>Thêm hình ảnh logo trên Hóa đơn như thế nào?</b></p>
<p>Trong phần “Cài đặt”, Người dùng (nếu được cấp quyền) sẽ có thể lựa chọn “Cài đặt Cửa hàng”. Kéo xuống tới phần “Logo” and nhấp chọn “Đăng tải logo” để đăng tải hình ảnh logo theo yêu cầu.</p>
<p><b>Chuyển nhượng Thỏa thuận Hợp tác Thương mại với WowMelo cho công ty khác như thế nào?</b></p>
<p>Vui lòng liên hệ Bộ phận Hỗ trợ Đối tác để được hướng dẫn trong trường hợp Đối tác muốn chuyển nhượng Thỏa thuận Hợp tác với WowMelo. Chúng tôi sẽ gửi tới Đối tác bản Thỏa thuận Chuyển nhượng để Đối tác điền thông tin và chuyển lại.</p>
<p><b>Cập nhật thông tin liên hệ của chúng tôi như thế nào?</b></p>
<p>Vui Vui lòng liên hệ Bộ phận Hỗ trợ Đối tác để được hướng dẫn cập nhật thông tin liên hệ.</p>
<p><b>Cập nhật thông tin liên hệ trên Hóa đơn gửi tới cho Khách hàng như thế nào?</b></p>
<p>Nếu được quyền truy cập, Người dùng hoặc Người dùng Quản trị (Admin) có thể cập nhật thông tin địa chỉ trong phần “Cài đặt”. Vui lòng lựa chọn “Cài đặt Cửa hàng” và kéo tới phần “Thông tin Cửa hàng”. Trong phần này, Đối tác có thể cài đặt thông tin liên hệ của mình.</p>

TEXT
        ,
    ],
    'customer_service'  => [
        'title'         => 'Làm thế nào để có thể liên hệ với bộ phận chăm sóc khách hàng của WowMelo?',
        'hotline'       => 'Hotline',
        'phone'         => '09090118669',
        'email'         => 'Email',
        'email_info'    => 'info@wowmelo.com'
    ],
];
