<?php

return [
    'title' =>'Cảm ơn bạn đã liên hệ WowMelo ',
    'text' =>'Nội dung tin nhắn của bạn đã được chuyển đến bộ phận hỗ trợ. 
    Chúng tôi sẽ liên hệ lại với bạn trong vòng 3 giờ.',
    'subject' => "Cảm ơn bạn đã liên hệ WowMelo.",
    'hello' => 'Xin chào',
    'regards' => 'Trân trọng',

];
