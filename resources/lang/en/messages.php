<?php

return [
    'banner'            => [
        'customer_button'       => 'Contact',
        'customer_desc'         => 'WowMelo provides an exclusive shopping sevice for our merchants\' most loyal customers. We create a frictionless shopping experience by removing the long tedious payment process: you will not need to pull out your bank card, log into your bank account to transfer money, nor will you need to sit at home and wait for the delivery service to come in order to pay cash.<br/><br/>Our Shopping Assistance service allows you to place order anywhere and anytime - on the bus, while having your breakfast or before your bedtime - with just a few clicks. We will make sure that your items will be delivered to your door, and you only need to pay us whenever you can find moment of free hand within 14 days of the order placement date.',
        'customer_title'        => 'What is WowMelo Shopping Assistance service?',
        'merchant'              => 'For Merchant',
        'merchant_benfit'       => 'Explore Merchant\'s benefits',
        'merchant_button'       => 'Go to Merchant Portal',
        'merchant_login'        => 'Get in touch with Sales team',
        'partner_button'        => 'Contact',
        'partner_desc'          => 'WowMelo is the first quick credit loan and payment solution that boosts conversion, average cart size, and customer loyalty by enabling customers to PAY LATER their purchase’s on your e-commerce.',
        'partner_title'         => 'Give loans even without credit cards.',
        'shopper'               => 'BUY NOW PAY LATER',
        'shopper_benfit'        => 'At your favourite stores… Just select WowMelo at checkout.',
        'shopper_login'         => 'Get fast credit for shopping even without a credit card, with the option to pay 14 or 30 days later.',
        'shopper_button'        => 'Go to Shopper Portal',
        'customer_service'      => 'Wowmelo<br/>Buy now - Pay later 14 days',
        'customer_service_desc' => 'How can WowMelo help you?',
    ],
    'common'            => [
        'see_more'  => 'See More',
    ],
    'contact'           => [
        'desc'          => 'Please submit your information, we will contact soon.',
        'email'         => 'Email address',
        'hotline'       => '090 9011 869',
        'message'       => 'Message',
        'firstname'     => 'First name',
        'lastname'      => 'Last name',
        'phone'         => 'Phone number',
        'website'       => 'Website URL',
        'average_order' => [
            'text'  => 'Average order value',
            'value' => [
                '1' => '< 200$',
                '2' => '200$ - 500$',
                '3' => '500$ - 1000$',
                '4' => '1000$ - 2000$',
                '5' => '> 2000$',
            ]
        ],
        'annual_sales'  => [
            'text'  => 'Annual sales through website',
            'value' => [
                '1' => '< 0.5M',
                '2' => '0.5M - 2M',
                '3' => '2M - 10M',
                '4' => '10M - 50M',
                '5' => '> 50M',
            ]
        ],
        'submit'        => 'Submit',
        'title'         => 'Contact Form',
    ],
    'faq'               => [
        '1'     => [
            '1'     => [
                'ans'   => 'You don’t have to register. Just sit back and relax – WowMelo service will appear in the Check-out page for those who have been approved to use the Pay Later service. If you were not qualified, you can register and share login details from other stores so we can review your purchase history.',
                'ques'  => 'How do I register to use the WowMelo service?',
            ],
            '2'     => [
                'ans'   => <<<'TEXT'
<p>To increase your eligibility for WowMelo, please help us eliminate some top decline reasons by:</p>
<ul>
<li>Ensuring to pay on time whenever you are using a WowMelo service;</li>
<li>Attempting to order with a lower basket value, if possible;</li>
<li>Increasing purchase frequency with our partnered Merchants, remember to log in so that we can identify you;</li>
<li>Supplying consistent personal information across all of our partnered Merchants so that we can identify you.</li>
</ul>
TEXT
                ,
                'ques'  => 'How can I increase my chances of being offered WowMelo Shopping Assistant service?',
            ],
            '3'     => [
                'ans'   => 'With our Shopping Assistant service, you can pay for your order up to 14 days after the order confirmation  date (depending on the your chosen Merchant this period might be shorter). We will send you an email with payment instructions as soon as your order is confirmed. To complete the payment, just click the link in the email we sent to you and follow the onscreen instructions. Alternatively, you can log into your personal account on www.wowmelo.com to complete the payment.',
                'ques'  => 'How does Shopping Assistant service work?',
            ],
            '4'     => [
                'ans'   => 'Once approved to use WowMelo, all you need to provide is the National ID number to complete the order. We will automatically create a WowMelo account for you using the email address that you use to log into the Merchant’s website, and all order-related information will be sent to that email address. Hence, kindly make sure that you supply the correct email address when shopping on our Merchant’s website.',
                'ques'  => 'What information do I need to provide to use WowMelo service?',
            ],
            '5'     => [
                'ans'   => <<<'TEXT'
<ul>
<li>You can register on our website and then provide us your login details to your favorite online shopping destination through secure contact form at the bottom of this page.</li>
<li>We will review you purchase history within 24 hours. Please change your password once you receive a notification that review is complete.</li>
<li>You will receive an email notification once review is complete. Enjoy “pay later” service!</li>
</ul>
TEXT
                ,
                'ques'  => 'I’ve never purchased from partner website. Can I still get the service?',
            ],
            'title' => 'About Pay Later Service',
        ],
        '2'     => [
            '1'     => [
                'ans'   => 'Your statement will be sent via email once the Merchant confirms your order. This statement will detail what you need to do, the payment due date and how you should pay. Please note, you do not need to pay before the due date.',
                'ques'  => 'When will I receive my statement and how is my statement sent?',
            ],
            '2'     => [
                'ans'   => <<<'TEXT'
<p>You can pay for your purchases whenever you want, but ensure to do so before the due date. Simply click onto the link we sent to you via email in order to make a secure payment.</p>
<p>Alternatively, you can also pay by logging into your WowMelo account that we created automatically upon your first time using our service.</p>
<p>Note that tt is important to pay on time. Any payment made after the 14th-day mark or any otherwise agreed upon dates will affect your eligibility to use WowMelo service in the future.</p>
TEXT
                ,
                'ques'  => 'How do I make a payment?',
            ],
            '3'     => [
                'ans'   => <<<'TEXT'
<p>You can pay your statement with credit/debit cards, domestic ATM cards, bank transfer or DHL Cash-on-Delivery.</p>
<ul>
<li>Direct/credit /domestic ATM card payment information is processed securely by Alepay from Nganluong.vn. All transactions take place via secure connections, protected with the latest industry standard security protocols.</li>
<li>With DHL Cash-On-Delivery service, you will be receiving a payment reminder letter from DHL directly. Upon the letter receipt, you should pay the DHL deliverer directly. In case of failed delivery of the reminder letter or failed payment upon the first reminder letter, DHL will only attempt for another 2 deliveries, after which if payment collection is still unsuccessful, a notification of failed collection will be sent back to WowMelo. In this circumstance, you will be required to choose an alternative method to complete the payment. </li><ul>
TEXT
                ,
                'ques'  => 'What are my payment options with WowMelo?',
            ],
            '4'     => [
                'ans'   => <<<'TEXT'
<p>Payment is typically up to 14 days after the order is confirmed. To help you pay on time, we’ll alert you via email.</p>
<p>You can also log in and view your orders at any time.</p>
TEXT
                ,
                'ques'  => 'I have forgotten when my payment due date is?',
            ],
            '5'     => [
                'ans'   => 'You don’t have to pay for goods you haven’t received. Contact the Merchant for an update on your order and also inform us about the delay. WowMelo will try our best to support you during the issue resolution time (check our Shopper Protection Policy for more details).',
                'ques'  => 'I have received an invoice, but I don’t have my stuff.',
            ],
            '6'     => [
                'ans'   => 'Normally, we would send this email as soon as your order is confirmed. However, you can also log into your account with your email address to find the payment details for your statement.',
                'ques'  => 'I haven’t received the email showing my statement number/payment information?',
            ],
            '7'     => [
                'ans'   => 'Once we have received your payment we will send a payment confirmation email to the address you used when ordering. If you do not receive this confirmation, please log into your WowMelo account to see your payment status.',
                'ques'  => 'Have you received my payment?',
            ],
            '8'     => [
                'ans'   => '<b>Please contact our Customer Service team to have this changed.</b>',
                'ques'  => 'My email address is not correct. How can I change it?',
            ],
            'title' => 'Billing & Payment',
        ],
        '3'     => [
            '1'     => [
                'ans'   => <<<'TEXT'
Ans<p>If you return some or all of your order, WowMelo will issue you with a new statement as soon as the Merchant accepts your return. Please note that your Merchant’s Return policy is applicable in this case.</p>
<p>If there is a delay in the process of registering a return on the Merchant side and there is an upcoming payment due date, please inform our Customer Service in order to have your payment due date postponed.</p>
<p><i>Note: If you have recently paid for a WowMelo order, please allow up to 72 hours (not including National holidays, Saturday & Sunday) for this to be received and processed.</i></p>
TEXT
                ,
                'ques'  => 'What if I make a return?',
            ],
            '2'     => [
                'ans'   => 'If you would like to cancel your order, we ask you to contact the Merchant with whom you made your purchase with.',
                'ques'  => 'How should I do if I want to cancel my order?',
            ],
            '3'     => [
                'ans'   => 'Please contact the Merchant directly to get this information.',
                'ques'  => 'Has the Merchant receive my returned items?',
            ],
            '4'     => [
                'ans'   => 'The Merchant will provide you with the necessary details regarding the return of your goods.',
                'ques'  => 'Where do I send the goods I would like to return?',
            ],
            '5'     => [
                'ans'   => 'The return period can vary depending on the Merchant with whom you have been shopping. Please contact or consult the website of the Merchant for further information regarding their return policy.',
                'ques'  => 'What is the return period for my order?',
            ],
            '6'     => [
                'ans'   => 'If you have already paid for your order to WowMelo, the Merchant with whom you have placed an order will return you the appropriate amount via the agreed methods between you and the Merchant.',
                'ques'  => 'How will I be refunded?',
            ],
            'title' => 'Returns',
        ],
        '4'     => [
            '1'     => [
                'ans'   => 'Yes, you can enter a separate delivery address at the checkout. We recommend that it you are new to our service and haven’t used the WowMelo service before, you should have your orders delivered to your billing address. This maximises the chances of your purchase being accepted.',
                'ques'  => 'Can I send my stuff to an address other than my billing address?',
            ],
            '2'     => [
                'ans'   => <<<'TEXT'
<p>You should never pay the delivery man. Your chosen Merchant may use a different logistics provider (other than our appointed cash handler DHL) who may not be authorised to receive the payment on behalf of WowMelo.</p>
<p>If you pay choose to pay by cash, please register to pay by cash by clicking on the link that we sent in your order confirmation email.  Then pay only when you receive the reminder letter from DHL (our appointed cash handler).</p>
TEXT
                ,
                'ques'  => 'Can I pay by cash directly to the deliverer upon the receipt of my order?',
            ],
            'title' => 'Delivery',
        ],
        'desc'  => 'Desc',
        'title' => 'FAQs',
    ],
    'footer'            => [
        'about'             => 'About',
        'about_us'          => 'About Us',
        'careers'           => 'Careers',
        'connect'           => 'Connect',
        'customer_service'  => 'Customer Service',
        'developers'        => 'Developers',
        'email_address'     => 'Email Address',
        'fags'              => 'FAQs',
        'for_merchant'      => 'For Merchants',
        'login'             => 'Login',
        'merchant'          => 'Merchant',
        'merchant_support'  => 'Merchant Support',
        'platforms'         => 'Platforms and Plugins',
        'policy'            => 'Shopper Protection Policy',
        'press'             => 'Press',
        'privacy'           => 'Privacy Policy',
        'shopper'           => 'Shopper',
        'subscribe'         => 'Subscribe',
        'subscribe_news'    => 'Subscribe',
        'terms'             => 'Terms and conditions',
        'update'            => 'Receive updates',
        'copyright'         => 'Copyright © 2018 WowMelo.',
        'company_info'      => 'WowMelo Trading Limited. Business Registration No: 0315447070<br/>
        Address: 180, Nguyen Cong Tru, Nguyen Thai Binh Ward, District 1, Ho Chi Minh City, Viet Nam'
    ],
    'home'              => [
        'desc'  => [
            'melo'      => '',
            'partner'   => '(many other brands coming soon…)',
            'wow'       => 'Just a few simple steps required',
        ],
        'melo'  => [
            'all_desc'      => 'Need to check all of your up-to-date  transactions? Simply log into your WowMelo account that we created automatically upon your first time using our service.',
            'all_title'     => 'All in one place',
            'free_desc'     => 'Our Shopping Assistance service is absolutely free – no interest, no hidden fees',
            'free_title'    => 'It’s free',
            'problem_desc'  => 'Received your items and not happy? No worries, we got you covered with our protection policy, our team will assist you. Get in touch',
            'problem_title' => 'No problems',
            'shopper' => 'WowMelo for your online store',
            'findout' => 'Find out more'
        ],
        'title' => [
            'melo'      => 'With WowMelo, Shopping is sweet',
            'partner'   => 'Shopping directory',
            'wow'       => 'With WowMelo, Shopping is easy',
        ],
        'wow'   => [
            'browse_desc'   => 'Browse our partnered merchant’s online shops for your desired items and add them to shopping carts as usual. Don\'t forget to log in!',
            'browse_title'  => 'Browse & choose',
            'item_desc'     => 'Sit back and relax. We will get your items to your door. Make sure that you have the National ID card that you entered upon check-out with you when receiving the items from the delivery service.',
            'item_title'    => 'Get your items',
            'pay_desc'      => 'You only need to choose how to pay WowMelo 14 days after the order confirmation date. You can do so by clicking onto the link we sent into your email.',
            'pay_title'     => 'Pay 14 days later',
            'select_desc'   => 'Choose WowMelo option at the payment page. This will be  available automatically to our partnered merchants’ most loyal registered customers.',
            'select_title'  => 'Select WowMelo',
        ],
    ],
    'lang'              => [
        'english'       => 'English',
        'vietnamese'    => 'Vietnamese',
    ],
    'login'             => 'Login',
    'register'          => 'Register',
    'menu'              => [
        'customer'      => 'WowMelo Service',
        'easy'          => 'Customer service',
        'for_merchant'  => 'For Merchants',
        'for_shopper'   => 'For Shopper',
        'shop'          => 'Easy shopping',
    ],
    'partner'           => [
        '1'     => [
            'desc'  => 'Check-out will be easier and faster for shoppers. Satisfied shoppers are more likely to return and buy more.',
            'title' => 'More seamless check-out',
        ],
        '2'     => [
            'desc'  => 'WowMelo will always pay you regardless of how shoppers choose to  pay us at the end. All you need to do is to focus on your operations.',
            'title' => 'Peace of mind',
        ],
        '3'     => [
            'desc'  => 'Be a dinosaur no more! Join the new technological wave in retail and become a smart retailer who brings the firsthand experience of what futuristic shopping is like.',
            'title' => 'Part of innovativon initiatives',
        ],
        'desc'  => 'Let\'s findout',
        'title' => 'What can WowMelo deliver',
    ],
    'mission'           => [
        'desc' => <<<'TEXT'
<ul>
    <li>Your customers pay later.You get paid next business day.</li>
    <li>Give your customers more purchasing power thanks to the freedom to pay later their online purchases. The credit assessment is instant.</li>
    <li>Our mission is to eliminate barriers and to build trust in online shopping in Vietnam </li>
</ul>
TEXT
        ,
        'title' => 'Increase sales and customers',
        'button' => 'Try now'
    ],
    'different'         => [
        'title' => 'What is different among WowMelo payment method and others?',
        '1' => [
            'title' => '<span class="red">COD</span> payment method',
            'step' => [
                '1' => [
                    'title' => 'Step 1',
                    'desc' => 'Check-out and choose COD payment method'
                ],
                '2' => [
                    'title' => 'Step 2',
                    'desc' => 'Shipper pick up your order and deliver it to you'
                ],
                '3' => [
                    'title' => 'Step 3',
                    'desc' => 'Receive and check your order'
                ],
                '4' => [
                    'title' => 'Step 4',
                    'desc' => 'Pay immediately after receiving your order'
                ],
            ],
        ],
        '2' => [
            'title' => '<span class="green">Credit card</span> payment method',
            'step' => [
                '1' => [
                    'title' => 'Step 1',
                    'desc' => 'Fill out Credit card information'
                ],
                '2' => [
                    'title' => 'Step 2',
                    'desc' => 'Fill out buyer information'
                ],
                '3' => [
                    'title' => 'Step 3',
                    'desc' => 'Confirm information and finish transaction'
                ],
                '4' => [
                    'title' => 'Step 4',
                    'desc' => 'Done!'
                ],
            ],
        ],
        '3' => [
            'title' => '<span class="blue">WowMelo</span> payment method',
            'step' => [
                '1' => [
                    'title' => 'Browse & choose',
                    'desc' => 'Browse our partnered merchant’s online shops for your desired items and add them to shopping carts as usual. Don\'t forget to log in!'
                ],
                '2' => [
                    'title' => 'Select WowMelo',
                    'desc' => '
                    Choose WowMelo option at the payment page. This will be  available automatically to our partnered merchants’ most loyal registered customers.'
                ],
                '3' => [
                    'title' => 'Get your items',
                    'desc' => 'Sit back and relax.
                    We will get your items to your door. Make sure that you have the National ID card that you entered upon check-out with you when receiving the items from the delivery service.'
                ],
                '4' => [
                    'title' => 'Pay 14 days later',
                    'desc' => '
                    You only need to choose how to pay WowMelo 14 days after the order confirmation date. You can do so by clicking onto the link we sent into your email.'
                ],
            ],
        ],
    ],
    'advantage'         => [
        'title' => 'Advantages of WowMelo payment method:',
        'rows' => [
            '1' => [
                'cols' => [
                    '1' => '',
                    '2' => '<h3><span class="red">COD</span> payment</h3>',
                    '3' => '<h3><span class="green">Credit card</span> payment</h3>',
                    '4' => '<h3><span class="blue">wowmelo</span> payment</h3>',
                ]
            ],
            '2' => [
                'cols' => [
                    '1' => 'Information to fill in the check-out step (in addition to delivery information)',
                    '2' => 'No',
                    '3' => 'Always provide credit card information (card number, expiration date, CVV)',
                    '4' => 'Only need ID number, CCCD for the first time.',
                ]
            ],
            '3' => [
                'cols' => [
                    '1' => 'Payment Deadline',
                    '2' => 'When the delivery staff arrives.',
                    '3' => 'Up to 15 days after the transaction finished.',
                    '4' => 'Always 14 days after the order date.',
                ]
            ],
            '4' => [
                'cols' => [
                    '1' => 'Overdue fees',
                    '2' => 'Orders will be canceled',
                    '3' => 'Interest rate of 30% per year',
                    '4' => 'Maximum 50.000 VND',
                ]
            ],
            '5' => [
                'cols' => [
                    '1' => 'Information Security',
                    '2' => 'No',
                    '3' => 'No, if the website does not encrypt customer information data',
                    '4' => 'Customer information is safe 100%',
                ]
            ],
            '6' => [
                'cols' => [
                    '1' => 'Financial control',
                    '2' => 'No',
                    '3' => 'Yes (bad credit will be reported back to the competent authorities)',
                    '4' => 'No',
                ]
            ],
            '7' => [
                'cols' => [
                    '1' => 'Financial control',
                    '2' => 'No, because it is easy to over-limit yourself to pay',
                    '3' => 'No, it is easy to over-limit yourself to pay',
                    '4' => 'Yes, the limit is designed based on personal purchase history information',
                ]
            ],
        ]
    ],
    'collaborate'       => [
        'title' => 'MERCHANT BENEFITS ',
        'content' => <<<'TEXT'
<ul>
    <li>No risk - Merchant get paid next business day</li>
    <li>Boost sales and increase trust with consumers</li>
    <li>Differentiate from competitors and build loyalty</li>
</ul>
TEXT
        ,
    ],
    'privacy'           => [
        'title' => 'Privacy Policy',
        'effect_form' => 'Effective as of 21 December 2018',
        'content' => <<<'TEXT'
<p>Wowmelo Trading Limited Company (“Wowmelo,” “we” or “us”) values your privacy. This Website Privacy Policy (the “policy”) applies to www.wowmelo.com and any information associated with this Site. In this policy, we describe how we collect, use, and disclose information that we obtain about visitors to our Site. Again, please note that this policy is limited to data associated with this Site.</p>
<p>By visiting the Site, you agree that your personal information will be handled as described in this policy. The www.wowmelo.com Website Terms of Use are incorporated by reference into this policy, and available on this Site.</p>
<h3>What Information Do We Collect About You and Why?</h3>
<p>We may collect information about you directly from you, as well as automatically through your use of our Site.</p>
<h4>Information We Collect Directly From You</h4> 
<p>At this time, you can browse all portions of our Site without registering, however, this may change at any time and this policy will be updated to reflect the change. You may submit certain personal information to us in order to learn more about Wowmelo products and services, including contact details such as your name, address, email address, and phone number. We may also collect contact details and opinions you provide on our Site via forms, surveys, contests, user comments and other mechanisms.</p>
<h4>Information We Collect Automatically</h4> 
<p>We may automatically collect the following non-personally identifying information about your use of our Site through cookies, web beacons, log files, and other technologies including: your domain name; your browser type and operating system; web pages you view, links you click and your activities in the Services; your IP address; time stamp and the length of time you visit our Site and the referring URL, or the webpage that led you to our Site; mobile device ID; location and language information; device name and model. We may combine this information with other information that we have collected about you or that you have provided to us, including, where applicable, your user name, name, and other personal information. Please see the section “Cookies and Other Tracking Mechanisms” below for more information.</p>
<h3>How We Use Your Information</h3>
<p>We use your information, for the following purposes:</p>
<ul>
<li>To provide more information about our products and services to you, to respond to your inquiries, and for other client service purposes.</li>
<li>To tailor the content and information that we may send or display to you, to offer location customization, and personalized help and instructions, and to otherwise personalize your experiences while using the Site.</li>
<li>For marketing and promotional purposes. For example, we may send you news and newsletters, special offers, and promotions, or to otherwise contact you about products or information we think may interest you.</li>
<li>To better understand how users access and use our Site, both on an aggregated and individualized basis, in order make improvements and respond to user desires and preferences, and for other research and analytical purposes, through the use of automated devices and applications.</li>
</ul>
<h3>How We Share Your Information</h3>
<p>We may share your information, as follows:</p>
<ul>
<li>Service Providers We may use service providers to assist us with the Site and may disclose the information we collect from you to third party vendors, service providers, contractors or agents who perform functions solely on our behalf, and they may not use the information for their own purposes.</li>
<li>Aggregate and Non-Personally Identifying Information We may share aggregate or non-personally identifying information about users with third parties for marketing, advertising, research or similar purposes.</li>
</ul>
<h3>Our Use of Cookies and Other Tracking Mechanisms</h3>
<p>We and our third party service providers use cookies and other tracking mechanisms to track information about your use of our Site and to track users across unaffiliated sites. We may combine this information with other personal information we collect from you (and our third party service providers may do so on our behalf).</p>
<p>Currently, our systems do not recognize browser “do-not-track” requests. You may, however, disable certain tracking as discussed in this section (e.g., by disabling cookies).</p>
<ul>
<li>Cookies Cookies are alphanumeric identifiers that we transfer to your computer’s hard drive through your web browser for record-keeping purposes. Some cookies allow us to make it easier for you to navigate our Site, while others are used to enable a faster log-in process, or to allow us to track your activities at our Site. Most web browsers automatically accept cookies, but if you prefer, you can edit your browser options to block them in the future. The Help portion of the toolbar on most browsers will tell you how to do control cookies. Visitors to our Site who disable cookies will be able to browse certain areas of the Site, but some features may not function.</li>
<li>Clear GIFs Clear GIFs are tiny graphics with a unique identifier, similar in function to cookies. In contrast to cookies, which are stored on your computer’s hard drive, clear GIFs are embedded invisibly on web pages. We may use clear GIFs (a.k.a. web beacons, web bugs or pixel tags), in connection with our Site to, among other things, track the activities of Site visitors, help us manage content, and compile statistics about Site usage.</li>
<li>Third Party Analytics We may use automated devices and applications to evaluate usage of our Site. We use these tools to help us improve our Site, performance and user experiences. These entities may use cookies and other tracking technologies to perform their services. We do not share your personal information with these third parties.</li>
</ul>
<h3>Third-Party Links</h3>
<p>Our Site may contain links to third-party websites, including social media sites. Any access to and use of such linked websites is not governed by this policy, but instead is governed by the privacy policies of those third party websites. We are not responsible for the information practices of such third party websites.</p>
<h3>Security of My Personal Information</h3>
<p>We have implemented safeguards to protect the information we collect from loss, misuse, and unauthorized access, disclosure, alteration, and destruction. Please be aware that despite our efforts, no data security measures can guarantee 100% security. You should take steps to protect against unauthorized access to your password, phone, and computer by, among other things, signing off after using a shared computer, choosing a robust password that nobody else knows or can easily guess, and keeping your log-in and password private. We are not responsible for any lost, stolen, or compromised passwords or for any activity on your account via unauthorized password activity.</p>
<h3>What Choices Do I Have Regarding Use of My Personal Information?</h3>
<p>We may send periodic promotional or informational emails to our clients. You may opt-out of such communications by following the opt-out instructions contained in the e-mail. Please note that it may take up to 10 business days for us to process opt-out requests. If you opt-out of receiving emails about recommendations or other information we think may interest you, we may still send you e-mails about any Services you have requested or received from us.</p>
<h3>Children Under 13</h3>
<p>Our products and services are not designed for children under 13. If we discover that a child under 13 has provided us with personal information, we will delete such information from our systems.</p>
<h3>Contact Us</h3>
<p>If you have questions about the privacy aspects of our Services or would like to make a complaint, please contact us at info@wowmelo.com.</p>
<h3>Changes to this Policy</h3>
<p>This policy is current as of the Effective Date set forth above. We may change this policy from time to time, so please be sure to check back periodically. We will post any changes to this policy on our Site. If we make any changes to this policy that materially affect our practices with regard to the personal information we have previously collected from you, we will aim to provide you with notice in advance of such change by highlighting the change on our Site.</p>

TEXT
        ,
    ],
    'terms'             => [
        'title' => 'TERMS OF USE',
        'desc' => 'THESE WEBSITE TERMS OF USE (THE “TERMS”) ARE A LEGAL CONTRACT BETWEEN YOU AND WOWMELO TRADING LTD. (“WOWMELO”). THESE TERMS EXPLAIN HOW YOU ARE PERMITTED TO USE THE WEBSITE LOCATED AT THE URL: WWW.WOWMELO.COM/ (THE “SITE”). BY USING THIS SITE, YOU ARE AGREEING TO ALL THESE TERMS; IF YOU DO NOT AGREE WITH ANY OF THESE TERMS, DO NOT ACCESS OR OTHERWISE USE THIS SITE, OR ANY INFORMATION CONTAINED ON THIS SITE.',
        'content' => <<<'TEXT'
<h3>Changes</h3>
<p>Wowmelo may make changes to the content available on the Site at any time. Wowmelo can change, update, add or remove provisions of these Terms, at any time, by posting the updated Terms on this Site. By using this Site after Wowmelo has updated these Terms, you are agreeing to all the updated Terms; if you do not agree with any of the updated Terms, you must stop using the Site.</p>
<h3>General Use</h3>
<p>By using this Site, you represent, acknowledge and agree that you are at least 18 years of age. If you are not at least 18 years old, you may not use the Site at any time or in any manner or submit any information to the Wowmelo or the Site.</p>
<p>Wowmelo provides content through the Site that is copyrighted and/or trademarked work of Wowmelo or Wowmelo’s third-party licensors and suppliers (collectively, the “Materials”). Materials may include logos, graphics, video, images, software and other content.</p>
<p>Subject to the terms and conditions of these Terms, and your compliance with these Terms, Wowmelo hereby grants you a limited, personal, non-exclusive and non-transferable license to use and to display the Materials and to use this Site solely for your personal use. Except for the foregoing license, you have no other rights in the Site or any Materials and you may not modify, edit, copy, reproduce, create derivative works of, reverse engineer, alter, enhance or in any way exploit any of the Site or Materials in any manner.</p>
<p>If you breach any of these Terms, the above license will terminate automatically and you must immediately destroy any downloaded or printed Materials.</p>
<h3>Using the Site</h3>
<p>You can simply view the Site and you do not need to register with Wowmelo to do so.</p>
<h3>Links to Third-Party Sites</h3>
<p>This Site may be linked to other web sites that are not sites controlled or operated by a Wowmelo Entity (collectively, “Third-Party Sites”). Certain areas of the Site may allow you to interact and/or conduct transactions with such Third-Party Sites, and, if applicable, allow you to configure your privacy settings in your Third-Party Site account to permit your activities on this Site to be shared with your contacts in your Third-Party Site account and, in certain situations, you may be transferred to a Third-Party Site through a link but it may appear that you are still on this Site. In any case, you acknowledge and agree that the Third-Party Sites may have different privacy policies and terms and conditions and/or user guides and business practices than Wowmelo, and you further acknowledge and agree that your use of such Third-Party Sites is governed by the respective Third-Party Site privacy policy and terms and conditions and/or user guides. You hereby agree to comply with any and all terms and conditions, users guides and privacy policies of any of Third-Party Sites. Wowmelo is providing links to the Third-Party Sites to you as a convenience, and Wowmelo does not verify, make any representations or take responsibility for such Third-Party Sites, including, without limitation, the truthfulness, accuracy, quality or completeness of the content, services, links displayed and/or any other activities conducted on or through such Third-Party Sites. YOU AGREE THAT WOWMELO WILL NOT, UNDER ANY CIRCUMSTANCES, BE RESPONSIBLE OR LIABLE, DIRECTLY OR INDIRECTLY, FOR ANY GOODS, SERVICES, INFORMATION, RESOURCES AND/OR CONTENT AVAILABLE ON OR THROUGH ANY THIRD-PARTY SITES AND/OR THIRD-PARTY DEALINGS OR COMMUNICATIONS, OR FOR ANY HARM RELATED THERETO, OR FOR ANY DAMAGES OR LOSS CAUSED OR ALLEGED TO BE CAUSED BY OR IN CONNECTION WITH YOUR USE OR RELIANCE ON THE CONTENT OR BUSINESS PRACTICES OF ANY THIRD-PARTY. Any reference on the Site to any product, service, publication, institution, organization of any third-party entity or individual does not constitute or imply Wowmelo’s endorsement or recommendation.</p>
<h3>Electronic Communications</h3>
<p>By using the Site, and provide contact information, you consent to receiving electronic communications, including electronic notices, from Wowmelo. These electronic communications may include notices about applicable fees and charges, transactional information and other information concerning or related to the Site provided on or through the Site. These electronic communications are part of your relationship with Wowmelo. You agree that any notices, agreements, disclosures or other communications that we send you electronically will satisfy any legal communication requirements, including that such communications be in writing.</p>
<h3>Website Privacy Policy</h3>
<p>Please review the Wowmelo Website Privacy Policy, which is available at on this Site and which explains how we use information that you submit to Wowmelo via this Site.</p>
<h3>Unauthorized Activities</h3>
<p>When using this Site, you agree not to:</p>
<ul>
<li>Disseminate any unsolicited or unauthorized advertising, promotional materials, ‘junk mail’, ‘spam’, ‘chain letters’, ‘pyramid schemes’ or any other form of such solicitation.</li>
<li>Use any robot, spider, scraper or other automated means to access the Site.</li>
<li>Take any action that imposes an unreasonable or disproportionately large load on our infrastructure.</li>
<li>Post anything contrary to our public image, goodwill or reputation.</li>
</ul>
<p>This list of prohibitions provides examples and is not complete or exclusive. Wowmelo reserves the right to terminate access to your account and your ability to post to this Site with or without cause and with or without notice, for any reason or no reason, or for any action that Wowmelo determines is inappropriate or disruptive to this Site or to any other user of this Site. Wowmelo may report to law enforcement authorities any actions that may be illegal, and any reports it receives of such conduct. When legally required or at Wowmelo’s discretion, Wowmelo will cooperate with law enforcement agencies in any investigation of alleged illegal activity on this Site or on the Internet.</p>
<p>You agree to indemnify and hold Wowmelo Entities and their respective officers, directors, employees, agents, licensors and business partners harmless from and against any and all costs, damages, liabilities and expenses (including attorneys’ fees and costs of defense) Wowmelo or any other indemnified party suffers in relation to, arising from or for the purpose of avoiding any claim or demand from a third party that your use of this Site violates any applicable law or regulation, or the copyrights, trademark rights or other rights of any third party.</p>
<h3>Proprietary Rights</h3>
<p>Wowmelo is our trademark in Vietnam. Other trademarks, names and logos on this Site are the property of their respective owners.</p>
<p>Unless otherwise specified in these Terms, all information and screens appearing on this Site, including documents, site design, text, graphics, logos, images and icons, as well as the arrangement thereof, are the sole property of Wowmelo. All rights not expressly granted herein are reserved. Except as otherwise required or limited by applicable law, any reproduction, distribution, modification, retransmission, or publication of any copyrighted material is strictly prohibited without the express written consent of the copyright owner or license.</p>
<h3>Disclaimer of Warranties</h3>
<p>Your use of this Site is at your own risk. The Materials have not been verified or authenticated in whole or in part by Wowmelo, and they may include inaccuracies or typographical or other errors. Wowmelo does not warrant the accuracy of timeliness of the Materials contained on this Site. Wowmelo has no liability for any errors or omissions in the Materials, whether provided by Wowmelo, our licensors or suppliers or other users.</p>
<p>WOWMELO, FOR ITSELF AND ITS LICENSORS, MAKES NO EXPRESS, IMPLIED OR STATUTORY REPRESENTATIONS, WARRANTIES OR GUARANTEES IN CONNECTION WITH THIS SITE, OR ANY MATERIALS RELATING TO THE QUALITY, SUITABILITY, TRUTH, ACCURACY OR COMPLETENESS OF ANY INFORMATION OR MATERIAL CONTAINED OR PRESENTED ON THIS SITE, INCLUDING WITHOUT LIMITATION THE MATERIALS. UNLESS OTHERWISE EXPLICITLY STATED, TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, THIS SITE AND MATERIALS CONTAINED OR PRESENTED ON THIS SITE ARE PROVIDED TO YOU ON AN “AS IS,” “AS AVAILABLE” AND “WHERE-IS” BASIS WITH NO WARRANTY OF IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT OF THIRD-PARTY RIGHTS. WOWMELO DOES NOT PROVIDE ANY WARRANTIES AGAINST VIRUSES, SPYWARE OR MALWARE THAT MAY BE INSTALLED ON YOUR COMPUTER.</p>
<h3>Limitation of Liability</h3>
<p>NO WOWMELO ENTITY SHALL BE LIABLE TO YOU FOR ANY DAMAGES RESULTING FROM YOUR DISPLAYING, COPYING OR DOWNLOADING ANY MATERIALS TO OR FROM THIS SITE. IN NO EVENT SHALL any Wowmelo entity BE LIABLE TO YOU FOR ANY INDIRECT, EXTRAORDINARY, EXEMPLARY, PUNITIVE, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES (INCLUDING LOSS OF DATA, REVENUE, PROFITS, USE OR OTHER ECONOMIC ADVANTAGE) HOWEVER ARISING, EVEN IF the Wowmelo entity KNOWS or should have known THERE IS A POSSIBILITY OF SUCH DAMAGE.</p>
<h3>Local Laws and Service Use outside of Vietnam</h3>
<p>Wowmelo controls and operates this Site from its headquarters in Vietnam and the Materials may not be appropriate or available for use in other locations. If you use this Site outside Vietnam, you are responsible for following applicable local laws.</p>
<h3>Feedback</h3>
<p>If you send or transmit any communications, comments, questions, suggestions or related materials to Wowmelo, whether through the Site or by letter, email, telephone or otherwise (collectively, “Feedback”) suggesting or recommending changes to the Site, or Materials, including, without limitation, new features or functionality relating thereto, all such Feedback is, and will be treated as, non-confidential and non-proprietary. You hereby assign all right, title and interest in, and Wowmelo is free to use, without any attribution or compensation to you, any ideas, know-how, concepts, techniques or other intellectual property and proprietary rights contained in the Feedback, whether or not patentable, for any purpose whatsoever, including but not limited to, developing, manufacturing, having manufactured, licensing, marketing and selling, directly or indirectly, products using such Feedback. You understand and agree that Wowmelo is not obligated to use, display, reproduce or distribute any such ideas, know-how, concepts or techniques contained in the Feedback, and you have no right to compel such use, display, reproduction or distribution.</p>
<h3>Governing Law</h3>
<p>Wowmelo will advise you if we feel you are not complying with these Terms and to recommend any necessary corrective action. However, certain violations of these Terms, as determined by Wowmelo, may result in immediate termination of your access to this Site without prior notice to you. Any disputes relating to these Terms or this Site, or the business relationship between you and Wowmelo, will be heard in the courts of Vietnam.</p>
<h3>General Agreement</h3>
<p>If any of these Terms is found to be inconsistent with applicable law, then such term shall be interpreted to reflect the intentions of the parties, and no other terms will be modified. WowMelo’s failure to enforce any of these Terms is not a waiver of such term. These Terms are the entire agreement between you and Wowmelo and supersede all prior or contemporaneous negotiations, discussions or agreements between you and Wowmelo about this Site. The proprietary rights, disclaimer of warranties, representations made by you, indemnities, limitations of liability and general provisions shall survive any termination of these Terms.</p>

TEXT
        ,
    ],
    'policy'            => [
        'title' => 'Shopper Protection Policy',
        'content' => <<<'TEXT'
<h3>Introduction</h3>
<p>WowMelo commits to ensure that assist you should you encounter any problems with your purchases. The aim of this Policy is to make you feel comfortable and safe while shopping at our partnered merchants. Rest assured that all of your personal data are well protected under our Privacy Policy.</p>
<p>WowMelo’s Shopper Protection covers all purchases that you make as a consumer when you use WowMelo.</p>
<h3>You will not need to pay if your Shopper Rights are violated</h3>
<p>Specifically, we won’t ask you to pay if:</p>
<ul>
<li>You don’t receive the goods you bought</li>
<li>The goods received are defective or not what you purchased.</li>
</ul>
<p>In case that the payment due date approaches, please make sure of the followings:</p>
<table class="table">
<tr>
<th>Non-delivery of goods</th>
<th>Defective goods or not what you purchased</th>
</tr>
<tr>
<td>
<ul>
<li>Double-check that you haven’t missed a delivery notice</li>
<li>Contact the merchant to find out where the goods are</li>
<li>If you still haven’t received your items when the new due date approaches, contact Wowmelo’s Customer Service. We won’t ask you to pay while we are looking into the matter. If our investigations show that the goods have not been delivered, we will postpone the payment date – unless one of the exceptions applies (please see below).</li>
</ul>
</td>
<td>
<ul>
<li>Contact Wowmelo’s Customer Service so that we can adjust your payment due date</li>
<li>Contact the merchant to try and resolve the issue. In the case that the issue is resolved and upon official notification from the merchant, we will update your invoice to reflect the approved return.</li>
<li>If you haven’t reached an understanding with the merchant before the new due date, contact Wowmelo’s Customer Service. Please have your order information with you during the call, including the merchant name, date of purchase, order number, description of the disputed good, and date when you first contacted the seller about the dispute. We will only ask you to pay if we conclude, based on an extensive investigation, that your claims are invalid.</li>
</ul>
</td>
</tr>
<tr>
<th>What should you do if you already paid?</th>
<th>What should you do if you already paid?</th>
</tr>
<tr>
<td>
<p>If you already paid for goods that you don’t receive, please do the following:</p>
<ul>
<li>Double-check that you haven’t missed a delivery notice</li>
<li>Contact the merchant to find out where the goods are</li>
<li>If the problem remains unsolved, contact Wowmelo’s Customer Service. We won’t ask you to pay while we’re looking into the matter. If your investigations show that the goods will not be delivered, we will refund your payment – unless one of the exceptions applies (please see below).</li>
</ul>
</td>
<td>
<p>If you are already paid for the goods before you notice a defect, please do the following:</p>
<ul>
<li>Contact the merchant and try to resolve the issue. The Merchant is responsible to exchange the goods or pay you back in this case.</li>
<li>If you cannot reach an understanding with the merchant, contact Wowmelo’s Customer Service. If investigations show that your goods are defective, we will refund the respective amount you paid for your purchase.</li>
</ul>
</td>
</tr>
<tr>
<th colspan="2"><span class="red">Exceptions</span></th>
</tr>
<tr>
<td colspan="2">
<p>You must make it possible for the goods to be delivered to you in order to be entitled to a refund or to suspend your due date in situations where your goods have not been delivered. If you don’t make reasonable efforts to pick up or receive the goods after you’ve received a coming delivery / unsuccessful delivery notice, you are generally liable for the cost of returning the goods to the merchant (including the fees that the merchant has to pay to the delivery service).</p>
<p>Please also keep in mind that if you want to use your cancellation right, you have to make sure that it is within what the merchants allows for. In general, you will not be able to cancel an order once it has been dispatched. Any cancellation claim after the dispatch date will be considered as a return, and therefore you will have to adhere to the merchant’s Return Policy. This also means that you will need to make reasonable efforts to try and receive the ordered goods, otherwise you are generally liable for the cost of returning the goods to the merchant (including the fees that the merchant has to pay to the delivery service) and this will be reflected in your invoice. Failure to pay the return cost in this situation will affect your future ability to use Wowmelo service in the future.</p>
</td>
</tr>
</table>
<h3>Do you want to cancel your purchase?</h3>
<p>If you want to use your cancellation right, you have to make sure that you can do so under the Merchant’s Cancellation Policy.</p>
<p><b>Order is not yet dispatched</b></p>
<p>If you have not paid us yet and want to exercise your cancellation right, just inform us via writing at ino@wowmelo.com and also the merchant about such decision. Once the merchant approves such cancellation, we will update the invoice accordingly. Please make sure to include your order information in your email to us (the merchant name, date of purchase, order number, description of the cancelled goods, and date when you first contacted the merchant about the cancellation).</p>
<p>If you have already paid us, then, once the merchant approves such cancellation, then we will return the appropriate amount via the same on the payment method you chose for that transaction.</p>
<table class="table">
<tr>
<th>Via credit/debit or domestic ATM cards</th>
<td>Return directly to your card at no extra fees</td>
</tr>
<tr>
<th>Via bank transfer</th>
<td>Return directly to your chosen bank account (please note this information on your cancellation request email) – please note that you will be liable for the bank transfer fees</td>
</tr>
<tr>
<th>Via cash</th>
<td>
<p>Collect cash return at our representative office at <b>Floor 22, TNR Tower, 180-192 Nguyen Cong Tru, Nguyen Thai Binh Ward, District 1, Ho Chi Minh City</b></p>
<p>or</p>
<p>Return directly to your chosen bank account (please note this information on your cancellation request email) – please note that you will be charged for the bank transfer fees.</p>
</td>
</tr>
</table>
<p><b>Order has been dispatched</b></p>
<p>In general, you will not be able to cancel an order once it has been dispatched. Any cancellation claim after the dispatch date will be considered as a return, and therefore you will have to adhere to the merchant’s Return Policy. This also means that you will need to make reasonable efforts to try and receive the ordered goods before placing a return request, otherwise you are generally liable for the cost of returning the goods to the merchant and this will be reflected in your invoice. Failure to pay the return cost in this situation will affect your future ability to use Wowmelo in the future.</p>
<h3>Claims with your merchant or with financial institutions</h3>
<p><b>With your merchant</b></p>
<p>This Policy outlines WowMelo’s own procedures for undelivered or materially different products as described above. Merchants subject to any dispute or claim under this Policy may still bring a payment claim against you without the involvement of Wowmelo. Should you and the merchant reach an agreement concerning the payment claim, or should your dispute with the merchant be adjudicated in a different forum (for example, through arbitration or a court), and the dispute is found in favor of the merchant, the merchant may reinstate the payment claim through Wowmelo or other means.</p>
<p><b>With your financial institutions</b></p>
<p>If you used your credit or debit card or domestic ATM card or bank deposit to purchase the disputed good through Wowmelo, or your credit or debit card or bank account information was used for an unauthorized purchase, you can submit a claim with (i) Wowmelo under this Policy or (ii) dispute the charge through your card issuing bank or depositary institution, but not both. If you submit a claim with us, and then dispute the charge with your issuing bank or depositary institution, and we receive a chargeback or other payment reversal, we will cancel your claim. If your claim with us is cancelled or denied, or you otherwise do not obtain a refund from us, you can still dispute the purchase with your issuing bank or depositary institution according to your issuing bank’s or depositary institution’s policies.</p>
<p>If you have already paid when filing a claim, and we accept your claim, we will refund you back the amount you paid for your purchase, including any taxes and shipping charges paid by you.</p>

TEXT
        ,
    ],
    'about'             => [
        'title' => 'About Us',
        'desc' => 'WowMelo is able to help you increase your interest and trust in online  shopping by reducing complicated payment process. When you pay by WowMelo you will receive your order immediately but you can pay later 14 days without interest.',
        'content' => <<<'TEXT'
<h3>1. Introduction about WowMelo</h3>
<p>Fram^ is a Scandinavian - Vietnamese IT Development and Venture Building company in Asean. Fram^ was  built by was founded and built by passionate company builders and top talented engineers. We are an explosive-growth, well-funded group on a mission to create, build and digitize companies around the world.</p> 
<p>WowMelo is a member of Fram^. We were born with the mission of eliminating barriers and building trust for online shopping in Vietnam - a booming market. With WowMelo, you can try the product at home before deciding to pay, without any extra costs.</p>
<h3>2. Wonderful shopping experience with WowMelo:</h3>
<p>WowMelo 's payment method is simple and convenient for all cases because you do not need to go through many stages like traditional online payment methods.</p>
<p>You are able to try product before paying.</p>
<p>You can pay whenever you want within 14 days after the order date without incurring any cost.</p>
<h3>3. WowMelo’s core value:</h3>
<p>Delete doubts about the product quality of pre-experience payment which is one of the problems of online shopping in Vietnam.</p>
<p>Paying by WowMelo helps you shop online easily because of simple and quick process.</p>
<p>Ensure the entire value of your order, and information security for shoppers.</p>
<h3>4. “WowMelo"’s meaning:</h3>
<p>Shopping is WOW: You are going to say Wow! WowMelo's mission helps shopper simplify shopping by eliminating the traditional time-consuming payment process. You can complete the order with a few simple steps.</p>
<p>Shopping is MELO: Melo is an abbreviation of "mellow - sweet". WowMelo allows you to shop comfortably at your favorite stores and pay later until 14 days without paying any extra interest or extra fees.</p>

TEXT
        ,
    ],
    'merchant_support'  => [
        'title' => 'FREQUENTLY ASKED QUESTIONS – FOR MERCHANT',
        'content' => <<<'TEXT'
<h3>1. Wowmelo’s service</h3>
<p><b>Which currency is accepted in WowMelo?</b></p>
<p>Wowmelo currently only accepts Vietnamese Dong.</p>
<h3>2. Payment information</h3>
<p><b>If Customer choose paying by WowMelo, when Merchant receive payment for order?</b></p>
<p>Merchant will receive payment following to schedule which was agreed by both parties when signing the Commercial contract. Usually, money will be transferred after two (02) working days immediately when WowMelo receives the order confirmation from the Merchant. 
WowMelo always pays for all orders that allow using WowMelo shopping support service. This will not relate to the way Customers choose to pay by WowMelo.
</p>
<p><b>When do Customer receive their return money in case the requests are accepted?</b></p>
<p>Depends on whether Merchant has received money from WowMelo and whether Wowmelo has received money from the Customer:</p>
<p><u>In case Merchant received payment:</u></p>
<ul>
<li>WowMelo has received the money from the Customer: Merchant refunds to the Customer.</li>
<li>WowMelo has not received money from Customers yet: Merchant refunds to WowMelo.</li>
</ul>
<p><u>In case Merchant has not received payment:</u></p>
<ul>
<li>WowMelo has received money from Customer: WowMelo refunds to Customer/li>
<li>WowMelo has not received money from customers: No refund required, the bill will be updated automatically.</li>
</ul>
<h3>3. Contract / Agreement with Wowmelo:</h3>
<p><b>How is WowMelo responsible for fraud related risks?</b></p>
<p>WowMelo's Merchants are protected from fraud related risks only in case of Merchants complying with WowMelo's Delivery Policy.</p>
<p><b>Which commodities are not allowed to be sold under the Agreement with WowMelo?</b></p>
<p>Details in WowMelo's Ethics Guide in Business.</p>
<h3>4. Order processing:</h3>
<p><b>How does WowMelo confirm order in Merchant Portal?</b></p>
<p><i>Order has not been confirmed as an order was created in “check-out" step</i> but not yet confirmed by the server system. If Merchant has unconfirm order, it is better to check with technical department to consider a potential API error. Merchant should handle the order in Merchant Portal.</p>
<p>Order confirmation means the order is ready to deliver to Customer. It will activate the Purchase Order process if Customer chooses WowMelo's service. Order confirmation will also trigger the WowMelo’s payment process for the Merchant.</p>
<p><b>How Merchant add more commodities, fees and discounts to orders?</b></p>
<p>Merchant can edit order detail in Merchant portal. Merchant can delete, add or change name, information, quantity, price or respective discount rates.</p>
<p>Merchant is not allowed to increase the price of the order beyond the price confirmed by the Customer at the time of purchase. If the order value is greater than the value confirmed by the Customer, the total value of the order will be highlighted in red and the Merchant will not be able to select the "Save edit" button.</p>
<p><u>The order has been shipped:</u></p>
<p>Merchant is not allowed to increase the value of ex-warehoused order, but Merchant is still allowed to add discounts or remove fees after the order is released. Merchant logs in to Merchant Portal then find the order that needs to be adjusted and select the "Refund" toolbar. Merchant will be entitled to request a refund of the amount not exceeding the corresponding order value.</p>
<p><b>How to remove an order from the Merchant Portal?</b></p>
<p>If the order has not been delivered, please select the "Delete" button to update the invoice and account for the Customer. If the customer has paid, WowMelo will process the automatic refund request. Deleted orders will not be displayed on the Money Transfer Control Report between the two parties, if WowMelo has not transferred the corresponding amount of the order to Merchant.</p>
<p><b>How to delay the deadline of payment on invoice? </b></p>
<p>If Customer has any questions related to payment's deadline, please guild Customer to contact with WowMelo Customer Service. If Merchant has questions about the delay of payment, please contact Merchant Support.</p>
<p><b>How to resend the updated invoice to Customer?</b></p>
<p>Merchant selects “send back invoice” button in Order page. A New invoice will be sent to Merchant's email.</p> 
<h3>5. Return:</h3>
<p><b>Merchant is waiting to receive the returned goods - how should Merchant handle the invoice?</b></p>
<p>Please guild Customer contact to WowMelo Customer Service in case the returned product has not been received by the Merchant, but has expired.</p>
<p><b>How should Merchant handle if Customer complaint?</b></p>
<p>Please guide Customer to contact WowMelo Customer Service for further assistance.</p> 
<p><b>Adjust orders and partially refund for Orders in Merchant Portal?</b></p>
<ul>
<li>
    <p><u>In case the order has not been released</u></p>
    <p>Please select "Adjust Order" button, Merchant will be able to change the order.</p>
</li>
<li>
    <p><u>In case the order has been released</u></p>
    <p>Please select the corresponding order and select "Refund" in the "Orders" section. Merchant is entitled to choose the amount to be returned to the Customer. When the refund amount is entered, please click "Refund".</p>
</li>
</ul>
<p><b>How to return an updated Invoice to Customer?</b></p>
<p>In case the complaint has been resolved and the Customer has agreed to pay for the new Invoice, please instruct Customer to contact WowMelo Customer Service.</p>
<h3>6. Accounting profile:</h3>
<p><b>When do Merchants receive payment for the orders which are paid by WowMelo?</b></p>
<p>All Activated orders will be paid in accordance with the payment terms agreed in the Trade Cooperation Agreement between WowMelo and Merchant.</p> 
<p>If Merchant has questions about deadline of payment, please contact to Merchant Support.</p>
<p><b>How do Merchant receive the Money Transfer Control Report?</b></p>
<p>Merchant Portal will automatically export a summary of monthly cash flows, and Merchant can choose to export longer periods reports. Merchant can do this by selecting "Filter by date" in the "Money transfer control report" section.</p>
<p>When Merchant chooses the "Money Transfer Control Report" at the top of the page, Merchant will see a list of information about the payments, including the date of payment, time of sale, transaction content, business collect the total, fees and repayment status and amount paid.</p>
<ul>
<li>
    <p><u>Download the CSV file for the Money Transfer Control Report</u></p>
    <p>Merchant can download CSV file including the all information and transactions related to related to each collection and expenditure.</p>
</li>
<li>
    <p><u>Download the PDF file for the Money Transfer Control Report</u></p>
    <p>Merchant can download PDF file including the all information and transactions related to related to each</p>
</li>
</ul>
<p><b>How to review transactions for each transfer?</b></p>
<p>Merchant can view the list of transactions of each transfer by clicking on the money order code</p>
<p><u>Links to Orders:</u></p>
<p>Each transaction will be linked to the corresponding Order. By clicking on the Order number, Merchant can immediately see the detailed Order information and the corresponding transfer order information.</p>
<h3>7. User management:</h3>
<p><b>Who is the registered user?</b></p>
<p>Registered users are those who can log in to the Order Processing System, Merchant Portal. Different users will have different access rights and email address information will be the information used to identify the user. When contacting Merchant Support, the Merchant will need to provide registered User Identification information to ensure that the information requested is provided only to authorized personnel.</p> 
<p><b>How to add new user?</b></p>
<p>Partners can add new Users to allow access to the Sales Partner Portal via the designated Administrator by using the "Invite Users" button in the "User" section.</p>
<p>Merchant may also cancel or adjust the access of existing Users if their account have the functionality to allow it.</p>
<p><b>How do different user accounts have different access rights?</b></p>
<p>Administrative users (Admin) will have different access levels. Administrator (Admin) users will automatically receive access to all applications and updated versions in the Sales Partner Portal. Other Users will need to be authorized to access updated versions.</p>
<p>Administrative users (Admin) have different access levels. Administrator (Admin) users will automatically receive access to all applications and updated versions in the Merchant Portal. Other Users will need to be authorized to access updated versions.</p>
<p>Admin have the following access rights:</p>
<ul>
<li>All applications and functions in the Merchant Portal</li>
<li>All updated versions of the application and functionality, however other Users will need Admin Users to provide access again. </li>
</ul>
<p>Merchant need to have at least one (01) Administrative User (Admin), but are allowed to have multiple Administrative Users (Admin).</p>
<p><u>Adjustment of access rights:</u></p>
<p>Normal user need permission to use each individual application, such as:</p>
<ul>
<li>History</li>
<li>Order</li>
<li>Setting</li>
<li>Payment</li>
<li>User</li>
</ul>
<p><u>Access in the "Orders" function:</u></p>
<p>In the "Orders" function section, the Admin will be allowed to access "Limited" or "Full" access for ordinary Users.</p>
<p>"Limited" access allows Users to:</p>
<ul>
<li>Review the order</li>
<li>Send back bill</li>
<li>Edit Customer's email</li>
</ul>
<p>"Limited" access rights DO NOT allow Users to do the following:</p>
<ul>
<li>Use other order management features, for example marking goods that have been shipped, canceled, returned or released.</li>
</ul>
<p>"Full" access allows Users to view and use all of the features mentioned above.</p>
<p><b>How do Merchant activate users on the Merchant Portal?</b></p>
<p>When the Admin adds a new User to the Merchant Portal, automatic email with the activation link will be sent to the new User's email address. New users need to click on the link within seven days. On the Activation page, Users only need to enter new password information before selecting "Choose password". New User's account will be activated and new users can login to WowMelo's Merchant Portal.</p>
<p><b>How can Merchant change password?</b></p>
<p>Merchant can select "Forgot password" on the Merchant Portal login page and fill in the user's email address information. Email for password reset will be sent to the selected email address.</p>
<h3>8. Merchant information</h3>
<p><b>How to add Merchant's logo in sales invoice?</b></p>
<p>In the "Settings" section, Users who are allowed will be able to select "Store Settings". Scrolling down to the "Logo" section and clicking "Upload logo" to upload the logo image as required.</p>
<p><b>How to transfer a Trade Cooperation Agreement with WowMelo to another company?</b></p>
<p>Please contact Merchant Support for guidance in case the Merchant wants to transfer the Cooperation Agreement with WowMelo. We will send to the Merchant a Transfer Agreement to fill in and return.</p>
<p><b>How to update Merchant's contact information?</b></p>
<p>Please contact to Merchant Support to be instructed </p>
<p><b>How to update contact information on Invoice sent to Customer?</b></p>
<p>If Merchant have access, the Admin can update the address information in the "Settings" section. Please select "Store Settings" and go to the "Store Information" section. In this section, Merchant can set up their contact information.</p>

TEXT
        ,
    ],
    'customer_service'  => [
        'title'         => 'How can I contact with Wowmelo\'s customer service?',
        'hotline'       => 'Hotline',
        'phone'         => '09090118669',
        'email'         => 'Email',
        'email_info'    => 'info@wowmelo.com'
    ],
];
