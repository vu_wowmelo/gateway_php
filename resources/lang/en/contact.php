<?php

return [
    'title' =>'Thank you for your interest in WowMelo!',
    'text' =>'We received your request.
Our team will get in touch with you within 3 working hours',
    'subject' => "Thank you for your interest in WowMelo!",
    'content' =>'We received your request.
Our team will get in touch with you within 3 working hours',
    'hello' => 'Hello',
    'regards' => 'Best Regards,',


];
