<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatedTableOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->index()->nullable();
            $table->string('order_code');
            $table->float('total',12,2);
            $table->float('discount_total',12,2);
            $table->float('discount_percent',4,2);
            $table->float('tax',4,2);
            $table->tinyInteger('status')->default(1);
            $table->string('billing_address');
            $table->string('billing_address_note');
            $table->string('billing_contact_name');
            $table->string('billing_contact_phone');
            $table->string('billing_ward');
            $table->string('billing_district');
            $table->string('billing_city');
            $table->string('billing_state');
            $table->string('billing_zipcode');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
